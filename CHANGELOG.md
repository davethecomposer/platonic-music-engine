# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project DOES NOT adhere to [Semantic Versioning](http://semver.org/spec/v2.0.0.html),
instead, it uses its own fancy versioning approach.

## [Unreleased]

## [Heraclitus] - 2019-03-28

### Added
- Added "Suprematism" painting to the malevich style algorithm
- We now have a way to arbitrarily pass any command.X value to the SA.
- New SA for runes
- New SA for I Ching
- New SA for tarot cards
- New SA based on Alphonse Allais's *Album primo-avrelisque*.
- SA based on Kazimir Malevich's *Suprematist* paintings: Black Square, Red Square, Black Cross, Black Circle, and White on White.
- Added SA based on Piet Mondrian's *Composition* paintings.
- Option to print out scores specific to an SA. For eg, mesostic, one note, and DnD character sheets. It uses the -sc/--score command line options.
- Added DnD Character Sheet generation
- We can print tempo word without the bpm, bpm without tempo word, or both.
- "cantos" option to mesostics. This is in the style of Cage's *Writing Through the Cantos* (of Ezra Pound).
- "merce" option to mesostics for random typographic features. Based on Cage's *62 Mesostics re: Merce Cunningham*
- Added function to figure out intervals from pitch numbers. Can be expanded. 
- Generate text mesostics and print out text files, lyric files (that can be used elsewhere in the PME to add lyrics to Lilypond output), and pdfs.
- Added lyrics for Lilypond. 
- Added version name to header variable: header.version = "x"
- We now calibrate the audio frequencies to produce the proper a4. This happened in older versions but was very convoluted. We now have a function toward the end that handles it simply. But we only do it for divisions of the octave greater than 2 as smaller divisions probably want to keep the base pitches.
- Added Harmonic Series tuning. For any given n/Harmonic we generate all the ratios of 1-n eliminating duplicates and ratios greater than 2.
- Added header.middle_of_instrument calculation. It is the pitch number that is the middle pitch of your instrument starting from the root pitch of your tuning. This gets used in several Style Algorithms.
- Began Changelog.

### Changed
- Moved all texts to a new directory to make it easier for other SAs to access.
- Lilypond can either get its number of notes from the number of notes in pitch_table or you can pass it. The latter is just a quick fix. We used to set this to the global command.number_of_notes but that created its own problems. Did the same for Csound.
- Created new directory "style_algorithms" and moved all the style algorithm files in there and created subdirs for each SA.
- Moved row_row_row to PME_Notation as it can be used in a general sense. Improved notation to be all contained in one table and move the processing to its own file like everything else.
- It used to be that taking a random number from 1-x with the "round" option set to "no" would generate a number from 1 to x+1 with the decimal values. This does not seem like the expected behavior. Fixes so that it generates all the numbers between 1 and x.
- Fixed long-standing and unknown bug dealing with non-C tunings and rotation values. This worked two years ago but something happened since then.
- Added line spacing algorithm for the merce style of mesostics.
- New method of printing out mesostics from egreg on TeX Stack Exchange. It allows for proportional fonts and other typographic manipulations.
- Changed command options (eg, command line stuff) to a global variable. Also removed all resulting global variables from function calls in top-level SAs. This cleaned things up a ton but is in violation of my long-held style. Things change.
- Lilypond_creation now looks up pitches and octaves in one big table that has all octaves instead of trying to calculate things. This fixes bugs and the code is much smaller and easier to follow.
- Changed .tun and .scl to .ptun and .pscl
- Moved Platonic Score Style Algorithm into its own file in scripts directory. This matches how the rest work.
- Expanded calculated scale degrees to two octaves. Mainly for creating EDOs bigger than an octave.
- Removed header.tuning_octave. calculated_scale_degrees.P8 should be able to replace it in all instances.

### Removed
- header.tuning_octave (replaced with calculated_scale_degrees.P8)
