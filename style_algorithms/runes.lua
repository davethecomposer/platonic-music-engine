-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Runes"
header.style = "" 
header.style_algorithm_author = "David Bellows"

command.user_input = "Heraclitus" 
command.generate_score = "no" -- "yes" "no"
command.generate_log = "yes" -- "yes" "no"

command.question = "?"
command.number_of_notes = ""

command.stone_color = "" -- "black" "gray" "grey" "blue" "red" "yellow" "orange" "purple" "green" "white" "brown"
command.line_color = "" -- "black" "blue" "red" "yellow"
local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()

-------------------------------------------------------------------

require("dir_runes/generate_runes") 

generate_runes_latex()
