-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "PME Notation"
header.style = "" 
header.style_algorithm_author = "David Bellows"

-------------------------------------------
local notes = {"Row, Row, Row Your Boat", "Possibly Eliphalet Oram Lyte", "c4@P1:4./ff", "M-2", "P1:4", "M2:8", "M3:4.", "M3:4", "M2:8", "M3:4", "P4:8", "P5:2.", "P8:8", "P8", "P8", "P5", "P5", "P5", "M3", "M3", "M3", "P1", "P1", "P1", "P5:4", "P4:8", "M3:4", "M2:8", "P1:2."}
local original_composer = table.remove(notes, 1)
local original_title = table.remove(notes, 1)
command.number_of_notes = #notes 

command.user_input = "Heraclitus"

command.generate_log = "yes"

command.generate_lilypond = "no"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" ; local enforce_nine = "no"
command.wave = "no" ; command.flac = "no" ; command.ogg = "no" ; local ogg_tag = "yes"

command.instrument = "Piano"

command.tempo_word = "allegro"

command.system = "western iso"

command.scale = "c-Panchromatic"

command.scale_recipe = "all"
command.scale_fill = 0
command.octave_recipe = "all" 

command.dynamics_recipe = "pp,mf:2,fff"

command.duration_recipe = "16th,8th:2,quarter,quarter"

command.tuning_name = "c-Western Standard"
-- command.tuning_name = "c-" .. math.pi .. "/EDO"
----------------------------------------

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"

local pitch_table = {} ; local frequency_table = {} ; local volume_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {}

local midi_ins_number = 0

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()

header.reference_pitch,header.reference_pitch_name = reference_pitch()

local tempo_number = tempo_value() 

audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

calculated_scale_degrees = generate_relative_scale_degrees()

analyze_intervals()

local scale_octave = get_scale_octave(calculated_scale_degrees) 
midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

----------------------------------------------

require("dir_pme_notation/pme_notation")

local pitch_table, duration_table, volume_table = generate_pme_music(notes)

local a4_calibration = calibrate_to_a4()

for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440)
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
end	     

local start_of_table = 1
ins_num_table[1] = 1

-- create specific Orchestra instrument
local orchestra_instrument = csound_instrument ; local instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
local csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
local csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)

local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

csound_score_ins = finish_csound_score_ins(csound_score_ins)

local csound_orchestra = csound_orchestra1 

local csound_score = csound_score_function1 .. csound_score_function1 .. "\n" .. csound_score_ins

local orchestra_finish = create_finish_orchestra(tempo_number)

local csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
local csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)

if command.generate_lilypond == "yes" then -- Generate Lilypond score
   require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
   local adapter = style_algorithm_author ; local pme = "Platonic Music Engine" 

   local time = "6/8" ; local voices = 1 ; local lyrics = {} ; local use_ragged_right = "no" ; local no_indent = "no"
   local rest_or_space = "s" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"

   local header = lilypond2header(pme, original_composer, original_title, additional_info)

   local lily_note_table_1,lily_note_table_3,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

   local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_3)
   
   local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(complete_lilypond_score)
end
