-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

function generate_album_primo_avrilesque()
   require("dir_album_primo_avrilesque/album_tables")
   local number = command.number_of_notes

   -----------------------------------------
   -- Generate Lilypond pdf
   -----------------------------------------
   if command.generate_score == "yes" then
      command.generate_lilypond = "yes"
   end
   
   command.lilypond_tempo_indication = "word" -- "bpm" "word" "both"
   command.generate_feldman = "no"
   command.generate_pattern_15 = "no"
   command.generate_pattern_35 = "no" 
   -- command.wave = "no" ; command.flac = "no" ; command.ogg = "no" ; local ogg_tag = "yes"

   command.instrument = "Piano"
   command.tempo_word = "largo"
   command.system = "western iso"
   command.tuning_name = "c-Western Standard"
   command.scale = "c-Panchromatic"
   command.scale_recipe = "all"
   command.scale_fill = 0
   command.octave_recipe = "all"
   command.dynamics_recipe = "rest"
   command.duration_recipe = "whole"

   local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"

   local pitch_table = {} ; local frequency_table = {} ; local volume_table = {} ; local amplitude_table = {}
   local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
   local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {} 

   local midi_ins_number = 0

   header.reference_pitch,header.reference_pitch_name = reference_pitch()

   local tempo_number = tempo_value() 

   -- Generate audio frequencies
   audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

   -- Calculate degrees for tuning
   calculated_scale_degrees = generate_relative_scale_degrees()

   -- Analyze your tuning against an ideal Just Intonation
   analyze_intervals()

   local scale_octave = get_scale_octave(calculated_scale_degrees) 
   midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

   local base_table = {}
   base_table = preparse_pitches(scale_octave)
   pitch_table = quantize("pitch_table", base_table)

   local base_table = {}
   base_table = preparse_velocities()
   volume_table = quantize("volume_table", base_table)

   local base_table = {}
   base_table = preparse_durations()
   duration_table = quantize("duration_table", base_table)

   local a4_calibration = calibrate_to_a4()

   for counter = 1, command.number_of_notes do 
      frequency_table[counter] = audio_freq_table[pitch_table[counter]]
      csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration 
      pan_table[counter] = .5
      ins_num_table[counter] = 1
      midi_ins_num_table[counter] = midi_ins_number
   end
   start_table = generate_start_table(duration_table)

   -- Create Csound
   -- Csound preamble
   local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

   local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

   if sound_font ~= "" then
      csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/" .. sound_font .. "\" \nsfpassign   0, isf\n "
   end

   -- End Csound preamble

   local start_of_table = 1
   ins_num_table[1] = 1
   
   -- create specific Orchestra instrument
   local orchestra_instrument = csound_instrument ; local instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument

   local csound_orchestra1 = create_csound_orchestra(orchestra_instrument) 

   local csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)

   local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

   -- local csoundscore = csound_score_ins -- for API playback

   csound_score_ins = finish_csound_score_ins(csound_score_ins)

   local csound_orchestra = csound_orchestra1 

   local csound_score = csound_score_function1 .. "\n" .. csound_score_ins
   
   local orchestra_finish = create_finish_orchestra(tempo_number)

   local csound_file = csound_preamble .. csound_orchestra .. orchestra_finish .. csound_score

   local csound_file_name = create_csound_file(csound_file)

   -- Create Audio
   create_csound_audio_file(csound_file_name, ogg_tag)

   -- Lilypond
   if command.generate_lilypond == "yes" then 
      require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
      local pme = "Platonic Music Engine" ; local original_composer = ""
      local original_title = "Marche Funebre" ; local voices = 1 ; local lyrics = {}
      
      local time = "4/4" ; local no_indent = "no"
      local rest_or_space = "r" ; local use_ragged_right = "yes" ; local avoid_collisions = "no"
      
      header.note_scheme = "simple"
      -- header.note_scheme = "complex"

      local header = lilypond2header(pme, original_composer, original_title, additional_info)

      local lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

      local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_2)
      
      local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
      
      local complete_lilypond_score = header..lilypond_score..footer

      lilypond_file_name = generate_lilypond_pdf(complete_lilypond_score) 
      lilypond_file_name = string.gsub(lilypond_file_name, ".ly", ".pdf")
   end      


      ----------------------------------------
      --Generate Artwork
      ----------------------------------------
      local titles = {}
      local color ; local print_color ; local subject ; local verb ; local object ; local ornament_color
      local check_for_a_vowel ; local a_or_an
      local score = ""  
      
      local tmp = io.open("texts/list_of_nouns/list_of_nouns.txt", "r")
      local nouns = tmp:read("*a")
      tmp:close()

      local tmp = io.open("texts/list_of_ing_verbs/list_of_ing_verbs.txt", "r")
      local verbs = tmp:read("*a")
      tmp:close()
      
      local vowels = {"a", "e", "i", "o", "u"}
      
      local noun_table = nouns:split("\n")
      local verb_table = verbs:split("\n")
      
      local random_noun_subject = platonic_generator("random_noun_subject", 1, #noun_table, number, "yes")
      local random_verb = platonic_generator("random_verb", 1, #verb_table, number, "yes")
      local random_noun_object = platonic_generator("random_noun_object", 1, #noun_table, number, "yes")
      local random_color = platonic_generator("random_color", 1, #color_table, number, "yes")
      local random_ornament_color = platonic_generator("random_ornament_color", 1, #color_table, number, "yes")
      local random_ornament_style = platonic_generator("random_ornament_style", 1, #ornament_style_table, number, "yes")
      
      for counter = 1, number do
	 color = color_table[random_color[counter]]
	 print_color = string.gsub(color,"%d","")
	 print_color = string.gsub(print_color, "%u", " %1")
	 print_color = string.gsub(print_color, "%s+", " ")
	 print_color = string.lower(print_color)
	 subject = noun_table[random_noun_subject[counter]]
	 verb = verb_table[random_verb[counter]]
	 verb = string.lower(verb)
	 object = noun_table[random_noun_object[counter]]
	 ornament_color = color_table[random_ornament_color[counter]]
	 ornament_style = ornament_style_table[random_ornament_style[counter]].style_number
	 ornament_dim = ornament_style_table[random_ornament_style[counter]].dimensions

	 check_for_vowel = string.sub(print_color, 2, 2) 
	 a_or_an = "a"
	 
	 for sub = 1, #vowels do
	    if check_for_vowel == vowels[sub] then a_or_an = "an" break
	    end
	 end

	 tmp_title = a_or_an .. "" .. print_color .. " " .. subject .. " " .. verb .. " " .. a_or_an .. "" .. print_color .. " " .. object
	 tmp_title = string.upper(tmp_title)
	 
	 if #tmp_title > 60 then 
	    tmp_title = "\\footnotesize{}" .. tmp_title
	 else
	    tmp_title = "\\small{}" .. tmp_title
	 end
	 titles[counter] = tmp_title

	 score = score .. "\n\\newpage\n\\begin{vplace}\n\\begin{center}"
	 score = score .. "\n\\begin{tikzpicture}[color=" .. ornament_color .. ", every node/.style={inner sep=0pt}]"
	 score = score .. "\n\\node[minimum size=" .. ornament_dim .. "cm](vecbox){};"
	 score = score .. "\n\\node[anchor=north west] at (vecbox.north west) {\\pgfornament[width=3cm]{" .. ornament_style .. "}};"
	 score = score .. "\n\\node[anchor=north east] at (vecbox.north east) {\\pgfornament[width=3cm,symmetry=v]{" .. ornament_style .. "}};"
	 score = score .. "\n\\node[anchor=south west] at (vecbox.south west) {\\pgfornament[width=3cm,symmetry=h]{" .. ornament_style .. "}};"
	 score = score .. "\n\\node[anchor=south east] at (vecbox.south east) {\\pgfornament[width=3cm,symmetry=c]{" .. ornament_style .. "}};"
	 score = score .. "\n\\draw[line width=.2cm](-5.5,-5.5)rectangle(5.5,5.5);"
	 score = score .. "\n\\draw[line width=.05cm](-5.3,-5.3)rectangle(5.3,5.3);"
	 score = score .. "\n\\draw[fill=" .. color .. ",draw=white](-5,-4)rectangle(5,5);"
	 score = score .. "\n\\node[align=center,text width = 9cm,text=black] at (0,-4.7) {" .. titles[counter] .. "};"
	 score = score .. "\n\\end{tikzpicture}"
	 score = score .. "\n\\end{center}"
	 score = score .. "\n\\end{vplace}\n"  

      end
      write_log("album.lua", "titles", titles)
      
      local dedication = header.dedication
      local title = "Art"
      local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
      local font = "newcent" ; local poet = "inspired by Alphonse Allais" ; local algorithm = header.algorithm
      local temperament_name = "" ; local instrument = "" ; local meter = ""
      local preamble = latex_preamble(font)
      
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, temperament_name, pretty_system)

      local preface = "\n\\setlength{\\parindent}{5cm}\n\\begin{vplace}[.2]\n\\begin{minipage}[c]{0.5\\textwidth}\n\\setlength{\\parindent}{1pc}\nOn 1 April 1897 Alphonse Allais published a monograph whose title translated into English reads: \\textit{April Foolish Album.}\n\n"

      preface = preface .. "The first part consisted of a series of monochromatic paintings. The preface to this section read, in part: \n\n{\\itshape{}The impression that I felt at the sight of this exciting masterpiece does not conform to any description. My destiny suddenly appeared to me in letters of fire. - And I, too, am a painter! I cried in French (at the time, I ignored the Italian language and I have still made no headway in it.) \n\nAnd when I say artist, let me make myself clear: I did not mean to speak of artists as they are most commonly understood, ridiculous craftsmen who need a thousand different colors to express their tiresome designs. \n\nNo! \n\nThe painter that I idealized was one whose genius was so great that he needed no more than one color for a painting: dare I say, the monochroidal artist.}"

      preface = preface .. "\n\nThe second part consisted of a piece of music about which he wrote: \n\n{\\itshape The author of this March Funebre was inspired, in its composition, of the principle, accepted by everyone, that great pains are silent.\n\nThese great pains being silent, the musicians will only have to count the measures, instead of indulging in the indecent noise which removes any majestic quality to better funerals.}"
      
      preface = preface .. "\n\\end{minipage}\n\\end{vplace}"

      score = preface .. score
      
      local complete_score = preamble .. "\n\\usepackage{pgfornament}\n\\usepackage{pdfpages}\n\\usepackage[none]{hyphenat}" .. "\n" .. latex_title .. score

      complete_score = string.gsub(complete_score, "\\nonzeroparskip", "")
      
      -- local footer = latex_footer()
      local footer = "\n\n\\includepdf{" .. lilypond_file_name .. "}\n\n"
      
      complete_score = complete_score .. footer .. "\n\\end{document}"

      local latex_name = "Album"
      if command.generate_score == "yes" then
	 create_latex(complete_score, latex_name)
      end
      
      return
   end
