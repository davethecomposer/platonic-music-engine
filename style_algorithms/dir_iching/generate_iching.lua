-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

function generate_iching_latex()
   require("dir_iching/generate_iching_tables")
   local binary_result = 0
   local result_table = {}
   local method = command.method
   local question = command.question
   local remainder = 0
   local result_line_table = {}
   local binary_table = {}
   if method == "" then
      local tmp = table.unpack(platonic_generator("method", 1, #method_table, 1, "yes")) 
      method = method_table[tmp]
   end
   write_log("generate_iching", "method", method)
   
   if method == "One coin" then
      
      binary_table = platonic_generator(question, 0, 1, 6, "yes")
      write_log("generate_iching", "binary_table", binary_table)
      for counter = 1,6 do
	 local binary_base = 2^(counter -1)
	 binary_result = binary_result + (binary_base * binary_table[counter])
	 if binary_table[counter] == 0 then
	    result_table[counter] = "---"
	 else
	    result_table[counter] = "- -"
	 end

      end
      binary_result = binary_result +1 -- Our algorithm does 0-63, but we need 1-64
      write_log("generate_iching", "binary_result", binary_result)
      write_log("generate_iching", "result_table", result_table)
   elseif method == "Yarrow stalks" then
      random.seed(hash(header.user_data .. question),hashstream(header.user_data .. question))
      local table_counter = 1
      for meta = 1,6 do 
	 local total_pile = 50
	 total_pile = total_pile -1
	 for counter = 1, 3 do
	    local left_pile = random.random(1, total_pile -1)
	    local right_pile = total_pile - left_pile
	    write_log("generate_iching", "left & right piles", left_pile .. ",", right_pile)
	    
	    remainder = right_pile - (right_pile -1)
	    right_pile = right_pile -1
	    total_pile = total_pile - remainder
	    write_log("generate_iching", "first remainder", remainder)
	    local left_hand_1 = remainder
	    write_log("generate_iching", "left_hand 1, left pile, right pile", left_hand_1 .. ", " .. left_pile .. ", " .. right_pile)
	    
	    while left_pile > 4 do
	       left_pile = left_pile -4 
	    end
	    
	    left_hand_2 = left_pile
	    total_pile = total_pile - left_hand_2	 
	    write_log("genereate_iching", "left hand 2", left_hand_2)
	    
	    while right_pile > 4 do
	       right_pile = right_pile -4 
	    end
	    left_hand_3 = right_pile
	    total_pile = total_pile - left_hand_3	 
	    write_log("genereate_iching", "left hand 3", left_hand_3)
	    
	    left_hand = left_hand_1 + left_hand_2 + left_hand_3     
	    write_log("genereate_iching", "left hand", left_hand)
	    result_line_table[counter] = left_hand
	    binary_table[table_counter] = left_hand
	    table_counter = table_counter + 1
	 end
	 write_log("generate_iching", "result_line_table", result_line_table)
	 local tmp_total = 0
	 for counter = 1, 3 do
	    tmp_total = tmp_total + result_line_table[counter]
	 end 
	 if tmp_total == 25 then result_table[meta] = "-x-"; tmp_binary = 1; end 
	 if tmp_total == 17 then result_table[meta] = "- -"; tmp_binary = 1; end
	 if tmp_total == 21 then result_table[meta] = "---"; tmp_binary = 0; end
	 if tmp_total == 13 then result_table[meta] = "-o-"; tmp_binary = 0; end
	 
	 local binary_base = 2^(meta -1)
	 binary_result = binary_result + (binary_base * tmp_binary)
      end
      binary_result = binary_result + 1
      write_log("generate_iching", "binary_table", binary_table)
      write_log("generate_iching", "binary_result", binary_result)
      write_log("generate_iching", "result_table", result_table)
   elseif method == "Three coins" then
      random.seed(hash(header.user_data .. question),hashstream(header.user_data .. question))
      local table_counter = 1
      for meta = 1,6 do
	 local tmp_total = 0
	 for counter = 1, 3 do
	    local tmp  = random.random(0,1) 
	    tmp_total = tmp_total + tmp
	    result_line_table[counter] = tmp
	    binary_table[table_counter] = tmp
	    table_counter = table_counter +1
	 end
	 if tmp_total == 3 then result_table[meta] = "-x-"; tmp_binary = 1; end 
	 if tmp_total == 1 then result_table[meta] = "- -"; tmp_binary = 1; end
	 if tmp_total == 2 then result_table[meta] = "---"; tmp_binary = 0; end
	 if tmp_total == 0 then result_table[meta] = "-o-"; tmp_binary = 0; end

	 local binary_base = 2^(meta -1)
	 binary_result = binary_result + (binary_base * tmp_binary)
      end
      binary_result = binary_result + 1 
      write_log("generate_iching", "binary_table", binary_table)
      write_log("generate_iching", "binary_result", binary_result)
      write_log("generate_iching", "result_table", result_table)
   end


   result = "\n\n\\begin{center}\n{\\textit{\\Large{}" .. question .. "}}\n\\end{center}\n\n"

   if method == "One coin" then
      result = result .. "\\bigskip\n\\begin{center}"
     
      for counter = 1,6 do
	 local tmp = one_coin_graphics[binary_table[counter]]
	 result = result .. tmp
      end
      result = result .. "\n\\end{center}\n\n"

   elseif method == "Three coins" then
      table_counter = 1
      result = result .. "\\bigskip\n\\begin{center}"
      for counter = 1,6 do
	 for sub = 1,3 do
	    local tmp = three_coins_graphics[binary_table[table_counter]]
	    result = result .. tmp
	    table_counter = table_counter + 1
	 end
	 if counter == 3 then
	    result = result .. "\n\n"
	 else
	    result = result .. "\\qquad "
	 end
      end
      result = result .. "\n\\end{center}\n\n"

   elseif method == "Yarrow stalks" then
      table_counter = 1
      result = result .. "\\bigskip\n\\begin{center}"
      for counter = 1,6 do
	 for sub = 1,3 do
	    local tmp = yarrow_stalks_graphics[binary_table[table_counter]]
	    result = result .. tmp
	    table_counter = table_counter + 1
	 end
	 if counter == 3 then
	    result = result .. "\n\n"
	 else
	    result = result .. "\\qquad "
	 end
      end
      result = result .. "\n\\end{center}\n\n"
      
   end
   
   result = result .. "\\begin{center}\n"
   for counter = 6, 1, -1 do
      local tmp = result_table[counter] 
      tmp = iching_graphics[tmp]  
      result = result .. "\n\\begin{tikzpicture}\n" .. tmp .. "\n\\end{tikzpicture}\n"
   end
   result = result .. "\n\\begin{CJK*}{UTF8}{gbsn}\n\\bigskip\\bigskip\\bigskip"
   result = result .. iching_comments[binary_result]
   result = result .. "\n\\end{CJK*}"
   result = result .. "\n\\end{center}"
   -- local result = "\n\\begin{tikzpicture}[remember picture,overlay]\n" .. result .. "\n\\end{tikzpicture}\n"
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Divination"
      local composer = "David Bellows"
      local arranger = "Platonic Music Engine"
      local font = "newcent"
      local poet = "\\textit{" .. method .. "} method"
      algorithm = header.algorithm
      local meter = "" 
      local piece = "" 
      local instrument = ""
      local symbol = ""
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. "\n\\usepackage{CJKutf8}\n" .. latex_title 

      local body = result 
      local score = score .. body --.. "\n}\\end{tikzpicture}"

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\n\\end{document}" 

      create_latex(score, "I-Ching")

   end
   return
end
