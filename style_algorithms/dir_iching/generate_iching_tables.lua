-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

method_table = {}
method_table = {"One coin", "Yarrow stalks", "Three coins"}

iching_graphics = {}
iching_graphics = {
   ["---"] = "\\draw[fill=black](0,0)rectangle(12,1);\n",
   ["- -"] = "\\draw[fill=black](0,0)rectangle(4,1);\n\\draw[fill=black](8,0)rectangle(12,1);\n",
   ["-x-"] = "\\draw[fill=black](0,0)rectangle(4,1);\n\\draw[line width = .2cm](5.5,0)--(6.5,1);\n\\draw[line width = .2cm](5.5,1)--(6.5,0);\n\\draw[fill=black](8,0)rectangle(12,1);\n",
   ["-o-"] = "\\draw[fill=black](0,0)rectangle(10,1);\n\\draw[line width = .2cm,color=white](6,.5)circle(1em);\n\\draw[fill=black](8,0)rectangle(12,1);\n",
}

one_coin_graphics = {}
one_coin_graphics = {
   [1] = "\\includegraphics[width=2cm]{dir_iching/heads} ",
   [0] = "\\includegraphics[width=2cm]{dir_iching/tails} ",
}

three_coins_graphics = {}
three_coins_graphics = {
   [1] = "\\includegraphics[width=1cm]{dir_iching/heads} ",
   [0] = "\\includegraphics[width=1cm]{dir_iching/tails} ",
}

yarrow_stalks_graphics = {}
yarrow_stalks_graphics = {
   [9] = "\\includegraphics[width=1cm]{dir_iching/nine_stalks} ",
   [8] = "\\includegraphics[width=1cm]{dir_iching/eight_stalks} ",
   [5] = "\\includegraphics[width=1cm]{dir_iching/five_stalks} ",
   [4] = "\\includegraphics[width=1cm]{dir_iching/four_stalks} ",
}

iching_comments = {}
iching_comments = {
   [1] = "{\\Large{}乾 (qián)}\n\n{}Force (the creative, strong action)\n",
   [2] = "{\\Large{}坤 (kūn)}\n\n{}Field (the receptive, acquiescence, the flow)\n",
   [3] = "{\\Large{}屯 (zhūn)}\n\n{}Sprouting (difficulty at the beginning, gathering support, hoarding)\n",
   [4] = "{\\Large{}蒙 (méng)}\n\n{}Enveloping (youthful folly, the young shoot, discovering)\n",
   [5] = "{\\Large{}需 (xū)}\n\n{}Attending (waiting, moistened, arriving)\n",
   [6] = "{\\Large{}讼 (sòng)}\n\n{}Arguing (conflict, lawsuit)\n",
   [7] = "{\\Large{}师 (shī)}\n\n{}Leading (the army, the troops)\n",
   [8] = "{\\Large{}比 (bǐ)}\n\n{}Grouping (holding together, alliance)\n",
   [9] = "{\\Large{}小畜 (xiǎo chù)}\n\n{}Small Accumulating (the taming power of the small, small harvest)\n",
   [10] = "{\\Large{}履 (lǚ)}\n\n{}Treading (treading, continuing)\n",
   [11] = "{\\Large{}泰 (tài)}\n\n{}Pervading (peace, greatness)\n",
   [12] = "{\\Large{}否 (pǐ)}\n\n{}Obstruction (standstill, stagnation, selfish persons\n",
   [13] = "{\\Large{}同人 (tóng rén)}\n\n{}Congregating People (fellowship, gathering)\n",
   [14] = "{\\Large{}大有 (dà yǒu)}\n\n{}Great Possessing (possession in great measure)\n",
   [15] = "{\\Large{}谦 (qiān)}\n\n{}Humbling (modesty)\n",
   [16] = "{\\Large{}豫 (yù)}\n\nProviding-For (enthusiasm, excess)\n",
   [17] = "{\\Large{}随 (suí)}\n\nFollowing\n",
   [18] = "{\\Large{}蛊 (gǔ)}\n\nCorrecting (decay, decaying, branch)\n",
   [19] = "{\\Large{}临 (lín)}\n\nNearing (approach, the forest)\n",
   [20] = "{\\Large{}观 (guān)}\n\n{}Viewing (contemplation, looking up)\n",
   [21] = "{\\Large{}噬嗑 (shì kè)}\n\n{}Gnawing Bite (biting through, biting and chewing)\n",
   [22] = "{\\Large{}贲 (bì)}\n\nAdorning (grace ,luxuriance)\n",
   [23] = "{\\Large{}剥 (bō)}\n\nStripping (splitting apart, flaying)\n",
   [24] = "{\\Large{}复 (fù)}\n\nReturning\n",
   [25] = "{\\Large{}无妄 (wú wàng)}\n\nWithout Embroiling (innocence, the unexpected, pestilence)\n",
   [26] = "{\\Large{}大畜 (dà chù)}\n\nGreat Accumulating (the taming power of the great, great storage, potential energy)\n",
   [27] = "{\\Large{}颐 (yí)}\n\nSwallowing (nourishing, jaws, comfort/security)\n",
   [28] = "{\\Large{}大过 (dà guò)}\n\nGreat Accomplishment (preponderance of the great, great surpassing, critical mass)\n",
   [29] = "{\\Large{}坎 (kǎn)}\n\nGorge (the abyss, repeated entrapment)\n",
   [30] = "{\\Large{}离 (lí)}\n\nRadiance (the clinging, fire, the net)\n",
   [31] = "{\\Large{}咸 (xián)}\n\nConjoining (influence, feelings)\n",
   [32] = "{\\Large{}恒 (héng)}\n\nPersevering (duration, constancy)\n",
   [33] = "{\\Large{}遁 (dùn)}\n\nRetiring (retreat, yielding)\n",
   [34] = "{\\Large{}大壮 (dà zhuàng)}\n\nGreat Invigorating (the power of the great, great maturity)\n",
   [35] = "{\\Large{}晋 (jìn)}\n\nProspering (progress, going forward)\n",
   [36] = "{\\Large{}明夷 (míng yí)}\n\nDarkening of the Light (keeping your thoughts to yourself)\n",
   [37] = "{\\Large{}家人 (jiā rén)}\n\nDwelling People (the family, the clan, family members)\n",
   [38] = "{\\Large{}睽 (kuí)}\n\nPolarising (opposition, perversion)\n",
   [39] = "{\\Large{}蹇 (jiǎn)}\n\nLimping (obstruction, afoot)\n",
   [40] = "{\\Large{}解 (xiè)}\n\nTaking-Apart (deliverance, untangled)\n",
   [41] = "{\\Large{}损 (sǔn)}\n\nDiminishing (decrease)\n",
   [42] = "{\\Large{}益 (yì)}\n\nAugmenting (increase)\n",
   [43] = "{\\Large{}怪 (guài)}\n\nDisplacement (resoluteness, parting, break-through)\n",
   [44] = "{\\Large{}构 (gòu)}\n\nCoupling (coming to meet, meeting)\n",
   [45] = "{\\Large{}萃 (cuì)}\n\nClustering (gathering together, finished)\n",
   [46] = "{\\Large{}升 (shēng)}\n\nAscending (pushing upward)\n",
   [47] = "{\\Large{}困 (kùn)}\n\nConfining (oppression, exhaustion, entangled)\n",
   [48] = "{\\Large{}井 (jǐng)}\n\nReplenishing (the well)\n",
   [49] = "{\\Large{}革 (gé)}\n\nAbolishing the Old (revolution, molting, getting rid of the old)\n",
   [50] = "{\\Large{}鼎 (dǐng)}\n\nEstablishing the New (overturning the cauldron)\n",
   [51] = "{\\Large{}震 (zhèn)}\n\nTaking Action (the arousing, shock, thunder)\n",
   [52] = "{\\Large{}艮 (gèn)}\n\nBound (keeping still, mountain)\n",
   [53] = "{\\Large{}渐 (jiàn)}\n\nInfiltrating (development, gradual progress, advancement)\n",
   [54] = "{\\Large{}归妹 (guī mèi)}\n\nMaturing (getting married)\n",
   [55] = "{\\Large{}丰 (fēng)}\n\nAbounding (abundance, fullness, fulfilling greatness)\n",
   [56] = "{\\Large{}旅 (lǚ)}\n\nSojourning (the wanderer, traveling)\n",
   [57] = "{\\Large{}巽 (xùn)}\n\nGrounded (proceding humbly, the gentle, the penetrating, wind, calculations)\n",
   [58] = "{\\Large{}兑 (duì)}\n\nOpen (joyful, the joyous, lake)\n",
   [59] = "{\\Large{}涣 (huàn)}\n\nDispersing (dispersion, dissolution)\n",
   [60] = "{\\Large{}节 (jié)}\n\nArticulating (limitation, moderation)\n",
   [61] = "{\\Large{}中孚 (zhōng fú)}\n\nCenter Returning (inner truth, innermost sincerity)\n",
   [62] = "{\\Large{}小过 (xiǎo guò)}\n\nSmall Exceeding (preponderance of the small, small surpassing, measured actions)\n",
   [63] = "{\\Large{}既济 (jì jì)}\n\nAlready Fulfilled (already completed, already done)\n",
   [64] = "{\\Large{}未济 (wèi jì)}\n\nNot Yet Fulfilled (before completion, not yet completed)\n",
}
   


   
