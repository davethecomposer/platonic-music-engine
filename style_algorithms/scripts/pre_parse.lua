-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that i3s.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Preparsers for pitch/velocity/duration/tempo recipes

preparse_pitches = function(scale_octave)
   local scale_name = command.scale
   local octave_recipe = command.octave_recipe
   local scale_recipe = command.scale_recipe
   local fill = command.scale_fill
   local pitch_table = {} -- This is the table we're generating
   local scale_table = {}
   local degree_value = 0
   local scale_recipe_table = {}
   local octave_table = {}
   local octave_pattern_table = {}
   scale_octave = math.floor(scale_octave)
   local low = header.lowrange
   local high = header.highrange
   local tuning_octave = calculated_scale_degrees.P8 --header.tuning_octave
   
   -- The following is for 0-EDO these values are 1 when using .001-EDO so it seems reasonable
   if scale_octave == 0 then scale_octave = 1 tuning_octave = 1 end
   
   do -- Create scale_table that contains the pattern for the scale and degrees emphasized
      if not scale_recipe then scale_recipe = "all" end
      if scale_recipe == "all" then fill = 1 end -- Sane assumption regardless of what user enters
      
      local key,mode = bifurcate(scale_name,"-") 
      local rotate_degree = distance_of_C_to_key[key] --distance_of_A_to_key[key]
      local rotate_value = calculated_scale_degrees[rotate_degree] -- 0-EDO odd, .001-EDO reports 0 but 0-EDO reports 1
      rotate_value = rotate_value - header.tuning_rotate
      if rotate_value < 0 then rotate_value = rotate_value + tuning_octave end

      if mode == "Panchromatic" then -- This works for 0-EDO
	 for counter = 0,scale_octave -1 do
	    scale_table[counter] = fill
	 end

      else   	 
	 for counter = 0,scale_octave-1 do scale_table[counter] = 0 end
	 -- local tmp_scale_pattern = (scale_key[mode].pattern)
	 mode = string.gsub(mode," ","_")
	 local tmp = io.open("scripts/scales/" .. mode .. ".pscl")
	 local data = tmp:read("*a")
	 tmp:close()
	 local _,name = bifurcate(data,"name:") 
	 local name,restof = bifurcate(name,"description:")
	 local description,rest = bifurcate(restof,"\nrepeat point:") 
	 local repeat_point,tmp_scale_pattern = bifurcate(rest,"\nscale:") 
	 
	 name = string.gsub(name,"^%s*", "")
	 description = string.gsub(description,"^%s*", "")
	 repeat_point = string.gsub(repeat_point,"^%s*", "")
	 local stuff = string.gsub(tmp_scale_pattern," ","") 
	 stuff = string.gsub(stuff,"^%\n*","") -- remove leading newlines    
	 stuff = string.gsub(stuff,"%\n*$","") -- remove all trailing newlines

	 local scale_pattern_table = stuff:split("\n")
	 
	 for i,value in ipairs(scale_pattern_table) do  
	    local degree_value = calculated_scale_degrees[value] 
	    local tmp_value = (degree_value + rotate_value) 
	    if tmp_value >= scale_octave then tmp_value = tmp_value - scale_octave end -- the fact that we can't use modulo for the same effect is troublesome
	    scale_table[(tmp_value)] = fill 
	 end     
      end
      
      write_log("pre_parse","scale_table",scale_table)

      scale_recipe_table = scale_recipe:split(",")     
      
      for i,value in ipairs(scale_recipe_table) do  
	 local degree,multiplier = bifurcate(value,":") 
	 
	 if degree ~= "all" then 
	    if not multiplier then multiplier = 1 end
	    if tonumber(degree) then 
	       tmp_value = degree_value + rotate_value
	       if tmp_value >= tonumber(math.ceil(scale_octave)) then tmp_value = tmp_value - scale_octave end
	       -- degree_value = (tonumber(degree) + rotate_value) % scale_octave
	       degree_value = tonumber(tmp_value)
	    else
	       degree_value = (calculated_scale_degrees[degree]) 
	       local tmp_value = degree_value + rotate_value 
	       if tmp_value >= tonumber(math.ceil(scale_octave)) then tmp_value = tmp_value - scale_octave end
	       degree_value = tonumber(tmp_value) 
	    end

	    if tonumber(scale_table[degree_value]) ~= tonumber(fill) then
	       scale_table[degree_value] = scale_table[degree_value] + multiplier
	    else
	       scale_table[degree_value] = multiplier
	    end
	    
	 end  
      end
      write_log("pre_parse","scale_table (with scale degrees)",scale_table)
   end -- Create scale_table that contains the pattern for the scale and degrees emphasized  
  
   do -- Create octave_table that contains the pattern for the octave and octaves emphasized
      local tmp_table = {}
      if not octave_recipe then octave_recipe = "all" end
      local octave_recipe_table = octave_recipe:split(",")
      for counter = -1,9 do octave_table[counter] = 0 end
      
      for i,value in ipairs(octave_recipe_table) do
	 local octave,multiplier = bifurcate(value,":")
	 if not multiplier then multiplier = 1 end
	 if octave ~= "all" then
	    local tmp_octave_number = octave_name_list[octave] 
	    octave_table[tmp_octave_number] = multiplier
	 else
	    for counter = -1,9 do octave_table[counter] = 1 end
	 end 
       end      
      write_log("pre_parse","octave_table",octave_table)
   end -- Create octave_table
      
   do -- now put it all together           
      -- local middle_a = 69 --+ calculated_scale_degrees.m3 - tuning_octave 
      -- local octave_low = middle_a - (tuning_octave * 5)  
      -- local octave_high = middle_a + (tuning_octave * 5) 

      local middle_c = c_octave_table.c4 
      local octave_low = c_octave_table["c-1"] --middle_c - (tuning_octave * 5)  
      local octave_high = c_octave_table.c9 --middle_c + (tuning_octave * 5) 
      -- print(middle_a,middle_c)
      local increment = 1
      for counter = -1,9 do
	 for subcounter = 1,tonumber(octave_table[counter]) do
	    for subsubcounter = 0, scale_octave - 1 do 
	       if scale_table[subsubcounter] ~= 0 then
		  for subsubsubcounter = 1,scale_table[subsubcounter] do 
		     local pitch = math.floor(((counter + 1)  * tuning_octave) + subsubcounter + octave_low)
		     if pitch >= low and pitch <= high then
			pitch_table[increment] = pitch 
			increment = increment + 1
		     end -- if pitch >= low
		  end -- for subsubsubcounter = 1, scale_table
	       end -- if scale_table[subsubcounter] ~= 0
	    end -- for subsubcounter = 0, scale_octave - 1
	 end -- for subcounter = 1, octave_table[counter]
      end -- counter = -1, 9
   end -- put it all together      
   
   write_log("pre_parse","pitch_table",pitch_table)
   return pitch_table
	 
end -- pre_parse pitches

-- Dynamic markings to MIDI velocity numbers
preparse_velocities = function()
   local basestring = command.dynamics_recipe
   local newstring = {} ; local tmp_table = {} ; local increment = 1 
   if basestring == "all" then 
      newstring = get_keys_from_table(velocity_dynamic_list)     
   else
      newstring = basestring:split(",") 
   end
   local resultstring = "" 

   for counter = 1,#newstring do
      local vel,number = bifurcate(newstring[counter],":")
      if number == nil then number = 1 end 
      for subcounter = 1,number do
	 local tmp_velocity = velocity_dynamic_list[vel]
	 if not tmp_velocity then tmp_velocity = vel end
	 tmp_table[increment] = tmp_velocity 
	 increment = increment + 1
      end
   end
   return(tmp_table)
end 

-- Preparse Durations 
preparse_durations = function()
   basestring = command.duration_recipe
   local newstring = {} ; local tmp_table = {} ; local increment = 1
   if basestring == "all" then
      newstring = get_keys_from_table(durations2seconds) 
   else
      newstring = basestring:split(",") 
   end
   local resultstring = "" ; local tmp_table = {}
   for counter = 1,#newstring do
      local duration,number = bifurcate(newstring[counter],":")
      if number == nil then number = 1 end
      for subcounter = 1,number do
	 local tmp_duration = durations2seconds[duration] 
	 if not tmp_duration then tmp_duration = duration end 
	 tmp_table[increment] = tmp_duration 
	 increment = increment + 1
      end
   end
   return tmp_table
end

-- Tempo markings to number
tempo_value = function()
   local basestring = command.tempo_word
   if tonumber(basestring) then 
      tempo_number = tonumber(basestring) 
   else tempo_number = tempo_ticks_list[basestring]
   end

   return tempo_number
end

