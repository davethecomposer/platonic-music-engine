-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Concert pitch tuning system
concert_pitch_system_list = {}
concert_pitch_system_list = {
   ["western iso"] = {tuning=440.000, name="Western ISO 16 (A=440)"},
   ["bach organ"] = {tuning=480.000, name="Bach organ (A=480)"},
   ["handel early"] = {tuning=422.500, name="Handel 1740 (A=422.5)"},
   ["handel late"] = {tuning=409.000, name="Handel 1780 (A=409)"},
   ["verdi"] = {tuning=432.000, name="Verdi (A=432)"},
   ["ny and boston"] = {tuning=442.000, name="New York Philharmonic and Boston Symphony Orchestra (A=442)"},
   ["continental"] = {tuning=443.000, name="Continental Europe Orchestras (A=443)"},
   ["baroque"] = {tuning=415.000, name="Modern Baroque (A=415)"},
   ["chorton"] = {tuning=466.000, name="Chorton: Baroque church (A=466)"},
   ["classical"] = {tuning=432.000, name="Modern Classical (A=432)"},
   ["test"] = {tuning=110.000, name="Dave's Test"},
}

platonic_degree = {} -- Just Intonation 5 limit scale 1 symmetric from Wikipedia
platonic_degree = {
   ["P1"] = 1,
   ["m2"] = 1.06666666,
   ["M2"] = 1.125,
   ["m3"] = 1.2,
   ["M3"] = 1.25, 
   ["P4"] = 1.33333333, 
   ["TT"] = 1.41423611, -- Average of 45/32 and 64/45 (aug 4th and dim 5th)
   ["P5"] = 1.5,
   ["m6"] = 1.6, 
   ["M6"] = 1.66666666, 
   ["m7"] = 1.777777778, 
   ["M7"] = 1.875, 
   ["P8"] = 2,
   ["m9"] = 2.133333333,
   ["M9"] = 2.25,
   ["m10"] = 2.4,
   ["M10"] = 2.5,
   ["P11"] = 2.666666666,
   ["TT2"] = 2.8284722,
   ["P12"] = 3,
   ["m13"] = 3.2,
   ["M13"] = 3.333333333,
   ["m14"] = 3.555555555,
   ["M14"] = 3.75,
   ["P15"] = 4,      
}

distance_of_A_to_key = {}
distance_of_A_to_key = {
   ["a"] = "P1",
   ["as"] = "m2",
   ["bf"] = "m2",
   ["b"] = "M2",
   ["bs"] = "m3",
   ["c"] = "m3",
   ["cs"] = "M3",
   ["df"] = "M3",
   ["d"] = "P4",
   ["ds"] = "TT",
   ["ef"] = "TT",
   ["e"] = "P5",
   ["es"] = "m6",
   ["f"] = "m6",
   ["fs"] = "M6",
   ["gf"] = "M6",
   ["g"] = "m7",
   ["gs"] = "M7",
   ["af"] = "M7",
}

distance_of_key_to_A = {}
distance_of_key_to_A = {
   ["a"] = "P8", --"P1" formerly 10/2/16 needs testing
   ["as"] = "M7",
   ["bf"] = "M7",
   ["b"] = "m7",
   ["bs"] = "M6",
   ["c"] = "M6",
   ["cs"] = "m6",
   ["df"] = "m6",
   ["d"] = "P5",
   ["ds"] = "TT",
   ["ef"] = "TT",
   ["e"] = "P4",
   ["es"] = "M3",
   ["f"] = "M3",
   ["fs"] = "m3",
   ["gf"] = "m3",
   ["g"] = "M2",
   ["gs"] = "m2",
   ["af"] = "m2",
}

distance_of_C_to_key = {}
distance_of_C_to_key = {
   ["a"] = "M6",
   ["as"] = "m7",
   ["bf"] = "m7",
   ["b"] = "M7",
   ["bs"] = "P1",
   ["c"] = "P1",
   ["cs"] = "m2",
   ["df"] = "m2",
   ["d"] = "M2",
   ["ds"] = "m3",
   ["ef"] = "m3",
   ["e"] = "M3",
   ["es"] = "P4",
   ["f"] = "P4",
   ["fs"] = "TT",
   ["gf"] = "TT",
   ["g"] = "P5",
   ["gs"] = "m6",
   ["af"] = "m6",
}

distance_of_key_to_C = {}
distance_of_key_to_C = {
   ["a"] = "M6",
   ["as"] = "m7",
   ["bf"] = "m7",
   ["b"] = "M7",
   ["bs"] = "P8",
   ["c"] = "P1",
   ["cs"] = "m2",
   ["df"] = "m2",
   ["d"] = "M2",
   ["ds"] = "m3",
   ["ef"] = "m3",
   ["e"] = "M3",
   ["es"] = "P4",
   ["f"] = "P4",
   ["fs"] = "TT",
   ["gf"] = "TT",
   ["g"] = "P5",
   ["gs"] = "m6",
   ["af"] = "m6",
}

equal_base_frequencies = {}
equal_base_frequencies = {
   ["a"] = 6.875,--440,--220.0, --440.000, 6.875,
   ["as"] = 7.28380877375, --466.1637615181,--233.081880759, --466.1637615181,
   ["bf"] = 7.28380877375, --466.1637615181,--233.081880759, --466.1637615181,
   ["b"] = 7.71692658215, --493.88330125613,--246.941650628, --493.88330125613,
   ["c"] = 8.1757989157, --523.251130602,--261.6255653006, --4.08789945785,
   ["cs"] = 8.6619572181, --554.365261954,--277.1826309769,
   ["df"] = 8.6619572181, --554.365261954,--277.1826309769,
   ["d"] = 9.17702399745, --587.329535834,--293.6647679174,
   ["ds"] = 9.72271824135, --622.253967444,--311.1269837221,
   ["ef"] = 9.72271824135, --622.253967444,--311.1269837221,
   ["e"] = 10.3008611536, --5.1504305768, --659.255113826,--329.6275569129,
   ["f"] = 10.9133822324, --5.4566911162, --698.456462866,--349.2282314330,
   ["fs"] = 11.5623257098, --739.988845424,--369.9944227116,
   ["gf"] = 11.5623257098, --739.988845424,--369.9944227116,
   ["g"] = 12.2498573745, --783.990871964,--391.9954359817,
   ["gs"] = 12.9782717994, --830.60939516,--415.3046975799,
   ["af"] = 12.9782717994, --830.60939516,--415.3046975799,
}

