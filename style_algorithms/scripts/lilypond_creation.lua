-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Lilypond Header
lilypond2header = function(arranger, poet, meter, piece)
   local instrument = command.instrument
   local tuning_name = command.tuning_name
   local algorithm = header.algorithm
   io.write("Generating Lilypond score ...\r") ; io.flush()
   local title = "Music" ; local dedication = header.dedication ; local subtitle = algorithm 
   local subsubtitle = "" ; local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z")
   local composer = header.style_algorithm_author
   local pretty_system = header.reference_pitch_name
   
   local tmp_tuning_name,tmp_name = bifurcate(tuning_name,"-")
   tmp_tuning_name = string.gsub(tmp_tuning_name,"s",'" \\sharp "')

   if tmp_tuning_name ~= "f" and tmp_tuning_name ~= 'f" \\sharp "' then
      tmp_tuning_name = string.gsub(tmp_tuning_name,"f",'" \\flat "')
   end

   tuning_name = tmp_tuning_name .. "-" .. tmp_name .. " tuning"
   piece = "\\markup {\\column {\\line {\"".. tuning_name .."\"}\n\t\t\t\t  \\line { using \""..pretty_system.."\"}}}"
   
   local lilypond_header = 
      "\\version \""..
      header.lilypond_version ..
      "\" \n\n\\header { \n \tdedication = \"(For " .. dedication ..
      ")\" \n \ttitle = \"" .. title ..
      "\" \n \tsubtitle = \"" .. subtitle ..
      "\" \n \tsubsubtitle = \"" .. subsubtitle ..
      "\" \n \tinstrument = \"" .. instrument .. 
      "\" \n \tpoet = \"" .. poet .. 
      "\" \n \tcomposer = \"" .. composer .. 
      "\" \n \tarranger = \\markup { \\with-color #darkblue \\with-url #\"https://www.platonicmusicengine.com\"{" .. arranger .. "}}" ..
      "\n \tmeter = \"" .. meter .. 
      "\" \n \tpiece = " .. piece ..   
      "\n \ttagline = \"\" \n \tcopyright = \\markup {\\column \\center-align {\\line {"..today.."} \n\t\t\t\t\t\t\\line {\"This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.\"}}}\n}" ..
      "\n\n\\include \"scripts/merge-rests.ly\"" ..
      "\n\\include \"scripts/auto-ottava.ly\"" ..
      "\n\\language \"english\"" ..
      "\n\\pointAndClickOff"
   
   -- if header.note_scheme == "simple" then lilypond_header = lilypond_header.."\n\\include \"scripts/enharmonic.ly\"" end

   return lilypond_header
end

-- Generate Lilypond notes from audio frequencies
generate_lilypond_notes_from_frequencies = function(scale_degrees, pitch_table, freq_table, duration_table, velocity_table, rest_or_nil, avoid_collision, number_notes)
   local instrument = command.instrument
   local number_of_notes
   if number_notes == nil then
      number_of_notes = #freq_table
   else
      number_of_notes = number_notes
   end

   local tuning_name = command.tuning_name
   local scale = command.scale
   local lily_note = {} 
   local lilypond_pitches = {}
   local lilypond_notes = {}
   local lilypond_notes_1 = {}
   local lilypond_notes_3 = {}
   local score_1 = {}
   local score_2 = {}
   local score_3 = {}
   local table_to_search = ""
   local lilypond_key = ""
   local active_vel = "" ; local active_vel_1 = "" ; local active_vel_3 = ""
   local middle_c = 261
   local upper_clef, lower_clef = midi_instrument_list[instrument].lilypond_clef,midi_instrument_list[instrument].second_clef
   local tmp_vel_table = get_keys_from_table(lilypond_velocity_list)
   local dur_table = get_keys_from_table(lilypond_duration_list)
   local rest_or_space = rest_or_nil
   local force_up = "no"
   
   local are_we_in_a_chord = "no"
   local chord_symbol = ""
   local chord_duration = nil
   local chord_dynamic
   
   local sharp_or_flat = sharp
   if header.note_scheme == "simple" then
      require("scripts/lilypond_pitches/standard")
      local root,scale_name = bifurcate(scale,"-")
      -- Check for a flat in signature or a minor key that uses flats
      if (string.find(root, "f") and not string.find(root, "fs"))or (string.find(scale_name, "minor") and (root == "d" or root == "g" or root == "c")) then 
	 sharp_or_flat = "flat"
      end
   elseif header.note_scheme == "complex" then
      require("scripts/lilypond_pitches/48edo")
   else require("scripts/lilypond_pitches/standard")
   end
   
   lily_freq = get_keys_from_table(freq2lily)
   
   for counter = 1, number_of_notes do
      local low = 1
      local high = #lily_freq
      local match_value = freq_table[counter] 
      local lily_num = binary_search(match_value, low, high, lily_freq)
      local lily_value = lily_freq[lily_num]
      local lily_pitch = freq2lily[lily_value] 
      if sharp_or_flat == "flat" then
	 local sub = string.match(lily_pitch, "(%a+)") -- (%a+) checks for any letters ignoring all , and '
	 sub = sharp2flat[sub] 
	 lily_pitch = string.gsub(lily_pitch,"(%a+)",sub) 
      end 
      lilypond_pitches[counter] = lily_pitch
      
      local tmp_dur = duration_table[counter]
      prechord = ""
      if tmp_dur == "." then 
	 if are_we_in_a_chord == "no" then
	    are_we_in_a_chord = "yes"
	    -- lilypond_pitches[counter - 1] = "<" .. lilypond_pitches[counter - 1]
	    prechord = "<" 
	    chord_symbol = ""
	 end

	 dur = old_dur 
	 
      else
	 if are_we_in_a_chord == "yes" then
	    are_we_in_a_chord = "no"
	    chord_symbol = ">" 
	 end
	 
	 dur = lilypond_duration_list[duration_table[counter]] 

	 -- The following allows for duration values not in the lilypond_duration_list, like fractional stuff
	 -- it does this with its own linear brute-force search. Could I reuse the binary search? Anyhoo, notice the difference
	 -- between subcounter and counter.
	 if not dur then
	    for subcounter = 1,#dur_table do
	       if tonumber(duration_table[counter]) == dur_table[subcounter] then
		  dur = lilypond_duration_list[dur_table[subcounter]] 
	       elseif tonumber(duration_table[counter]) > dur_table[subcounter] and tonumber(duration_table[counter]) < dur_table[subcounter + 1] then       
		  if math.abs(tonumber(duration_table[counter]) - dur_table[subcounter]) < math.abs(tonumber(duration_table[counter]) - dur_table[subcounter+1]) then 
		     dur = lilypond_duration_list[dur_table[subcounter]] 
		  else
		     dur = lilypond_duration_list[dur_table[subcounter + 1]] 
		  end
	       end
	    end	 	 
	 end
      end
      old_dur = dur
      if are_we_in_a_chord == "yes" then dur = "" end
      local tmp_vel = lilypond_velocity_list[velocity_table[counter]] 

      -- The following allows for velocity values not in the lilypond_velocity_list, like fractional stuff
      -- it does this with its own linear brute-force search. Could I reuse the binary search? Anyhoo, notice the difference
      -- between subcounter and counter.
      if not tmp_vel then
	 for subcounter = 1,#tmp_vel_table do
	    if tonumber(velocity_table[counter]) == tmp_vel_table[subcounter] then
	       tmp_vel = lilypond_velocity_list[tmp_vel_table[subcounter]] 
	    elseif tonumber(velocity_table[counter]) > tmp_vel_table[subcounter] and tonumber(velocity_table[counter]) < tmp_vel_table[subcounter + 1] then       
	       if math.abs(tonumber(velocity_table[counter]) - tmp_vel_table[subcounter]) < math.abs(tonumber(velocity_table[counter]) - tmp_vel_table[subcounter+1]) then 
		  tmp_vel = lilypond_velocity_list[tmp_vel_table[subcounter]] 
	       else
		  tmp_vel = lilypond_velocity_list[tmp_vel_table[subcounter + 1]] 
	       end
	    end
	 end	 	 
      end 
      if tmp_vel == "r" then
	 if avoid_collision == "yes" then
	    lilypond_notes[counter] = lilypond_pitches[counter] .. dur .. "\\rest"
	 else
	    lilypond_notes[counter] = "r" .. dur
	 end
      else
	 lilypond_notes[counter] = prechord .. lilypond_pitches[counter] .. chord_symbol .. dur
      end

      if are_we_in_a_chord == "yes" then tmp_vel = "" end
      
      if lower_clef == "0" then
	 if tmp_vel ~= active_vel then
	    if tmp_vel ~= "r" then
	       lilypond_notes_1[counter] = lilypond_notes[counter] .. tmp_vel
	       active_vel = tmp_vel
	    else
	       lilypond_notes_1[counter] = lilypond_notes[counter]
	    end
	 else
	    lilypond_notes_1[counter] = lilypond_notes[counter]
	 end
      else
	 if freq_table[counter] > middle_c or force_up == "yes" then
	    if tmp_vel ~= active_vel_1 then
	       if tmp_vel ~= "r" then
		  lilypond_notes_1[counter] = lilypond_notes[counter] .. tmp_vel --.. chord_symbol
		  active_vel_1 = tmp_vel
	       else
		  lilypond_notes_1[counter] = lilypond_notes[counter] 
	       end
	       lilypond_notes_3[counter] = rest_or_space--[["r"]] .. dur 
	    else
	       lilypond_notes_1[counter] = lilypond_notes[counter] .. chord_symbol	         
	       lilypond_notes_3[counter] = rest_or_space--[["r"]] .. dur 
	    end
	    
	 else
	    if tmp_vel ~= active_vel_3 then
	       if tmp_vel ~= "r" then
		  lilypond_notes_3[counter] = lilypond_notes[counter] .. tmp_vel
		  active_vel_3 = tmp_vel
	       else
		  lilypond_notes_3[counter] = lilypond_notes[counter]
	       end
	       lilypond_notes_1[counter] = rest_or_space--[["r"]] .. dur .. chord_symbol
	    else
	       lilypond_notes_1[counter] = rest_or_space--[["r"]] .. dur .. chord_symbol
	       lilypond_notes_3[counter] = lilypond_notes[counter] 
	    end
	 end -- upper vs lower clef
      end -- single vs double clefs
   end -- for loop for creating pitches

   local key_lily,mode_lily = table.unpack(scale:split("-"))

   if mode_lily == "major" then
      lilypond_key = key_lily .. " \\" .. "major"
   elseif mode_lily == "minor" or mode_lily == "harmonic minor" then
      lilypond_key = key_lily .. " \\" .. "minor"
   else
      lilypond_key = "c \\major"
   end
   
   write_log("Lilypond creation", "lily_note", lilypond_notes)
   write_log("Lilypond creation", "lily_note_1", lilypond_notes_1)
   write_log("Lilypond creation", "lily_note_3", lilypond_notes_3)

   score_1 = table.concat(lilypond_notes_1," ")
   if down_clef ~= "0" then score_2 = table.concat(lilypond_notes_3," ") end
   return score_1,score_2,lilypond_key

end

-- Generate Lilypond variables
lilypond2variables = function(key, time, tempo_number, score_one, score_two, score_three, score_four, score_five, score_six)
   local instrument = command.instrument
   local tempo_word = command.tempo_word
   local equal = "="
   local upper_clef = midi_instrument_list[instrument].lilypond_clef
   local down_clef = midi_instrument_list[instrument].second_clef
   local score_info
   
   if tonumber(tempo_word) ~= nil then
      tempo_word = ""
      local whole,fraction = math.modf(tempo_number)
      if fraction ~= 0 then
	 tempo_number = whole
	 equal = "=" -- Need to make this into approximate sign. Might be complicated.
      end
   end

   if command.lilypond_tempo_indication == "both" or command.lilypond_tempo_indication == nil then
      score_info = "\n\nscore_info = {\\time " .. time .. " \\tempo \"" .. tempo_word .. "\" 4 " .. equal .. " " .. tempo_number .. "}"
   elseif command.lilypond_tempo_indication == "word" then
      score_info = "\n\nscore_info = {\\time " .. time .. " \\tempo \"" .. tempo_word .. "\"}"
   elseif command.lilypond_tempo_indication == "bpm" then
      score_info = "\n\nscore_info = {\\time " .. time .. " \\tempo  4 " .. equal .. " " .. tempo_number .. "}"
   end
   
   local score = "" ; final_score = ""

   score = score .. score_info
   score = score .. "\n\nvoice_one = {\\key "..key.." "..score_one

   -- local lyrics 
   -- local lilypond_lyrics = ""
   
   -- if #score_lyrics ~= 0 then 
   --    for counter = 1,#score_lyrics do
   -- 	 lilypond_lyrics = lilypond_lyrics..score_lyrics[counter].." "
   --    end
   -- else
   --    lyrics = ""
   -- end
   
   -- if #score_lyrics ~= 0 then
   --    -- lyrics = "\\new Lyrics \\lyricsto \"first\" {"..lilypond_lyrics.."}\n"
   --    lyrics = "\n\n\\addlyrics {" .. lilypond_lyrics .. "} " print(lyrics)
   -- end
   
   if score_three ~= nil then
      score = score .."}\n\nvoice_three = {"..score_three
   end
   if score_five ~= nil then
      score = score .."}\n\nvoice_five = {"..score_five
   end
   if down_clef ~= "0" then 
      score = score.."}\n\nvoice_two = {\\key "..key.." "..score_two
      if score_three ~= nil then
	 score = score .. "}\n\nvoice_four = {"..score_four
      end
      if score_five ~= nil then
	 score = score .. "}\n\nvoice_six = {"..score_six
      end
   end
   
   final_score = score .. " \\bar \"|.\"}"

   -- final_score = final_score .. lyrics
   
   return final_score, score_info
end

-- Generate Lilypond footer
lilypond2footer = function(score_info, voices, score_lyrics, use_ragged_right, no_indent)
   local instrument = command.instrument
   
   local lilypond_lyrics = "" ; local lyrics = "" ; local ragged_right = "" ; local add_lyrics = ""
   local temperament_range = calculated_scale_degrees.P8 --header.tuning_octave
   local paper_size = header.paper
   local ottava_treble,ottava_bass = midi_instrument_list[instrument].ottava_treble,midi_instrument_list[instrument].ottava_bass
   if ottava_treble == nil then ottava_treble = "" end
   if ottava_bass == nil then ottava_bass = "" end
   local down_clef = midi_instrument_list[instrument].second_clef 
   local naturalize_music = ""
   if no_indent == "yes" then lily_indent = "score = 0" else lily_indent = "" end

   local upper_clef = midi_instrument_list[instrument].lilypond_clef
   local lower_clef = midi_instrument_list[instrument].second_clef
   
   if use_ragged_right == "yes" then
      ragged_right = "ragged-right = ##t"
   end

   if temperament_range then
      if header.note_scheme == "simple" then naturalize_music = "\n\t\t\t\\naturalizeMusic " end
   end

   layout_addition = "\\context { \\Voice \\remove \"Note_heads_engraver\" \\consists \"Completion_heads_engraver\" \\remove \"Rest_engraver\" \\consists \"Completion_rest_engraver\"}"
   
   if no_indent == "yes" then layout_addition = layout_addition .. "indent = 0" end

   if #score_lyrics ~= 0 then
      for counter = 1,#score_lyrics do
   	 lilypond_lyrics = lilypond_lyrics..score_lyrics[counter].." "
	 lilypond_lyrics = string.gsub(lilypond_lyrics, '"', "")
      end
   end
   
   if #score_lyrics ~= 0 then
      -- lyrics = "\\new Lyrics \\lyricsto \"first\" {"..lilypond_lyrics.."}\n"
      lyrics = "\n\nlyrics_one =  \\lyricmode {" .. lilypond_lyrics .. "} "
      add_lyrics = "\\addlyrics {\\lyrics_one} "
   end

   lilypond_footer = lyrics ..
      "\n\n\\paper {#(set-paper-size \""..
      paper_size..
      "\") "..lily_indent.."}\n\n\\score { \n \t\\new PianoStaff << \n"..
      "\t\t\\new Staff = \"upper\" << \n\t\t\t\\score_info \\clef \""..upper_clef.."\" "..ottava_treble.." " --[[..naturalize_music]]  .."\\voice_one " .. add_lyrics --"\\lyrics_one"
   if voices >= 2 then
      lilypond_footer = lilypond_footer .. "\\\\ " --[[..naturalize_music]] .."\\voice_three "
   end
   if voices == 3 then
      lilypond_footer = lilypond_footer .. "\\\\ " --[[.. naturalize_music]] .."\\voice_five " 
   end
   lilypond_footer = lilypond_footer .. ">> \n"
   
   if down_clef ~= "0" then
      lilypond_footer = lilypond_footer .. 
	 "\t\t\\new Staff = \"lower\" << \\clef " .. lower_clef .. " " .. ottava_bass .. " " --[[.. naturalize_music]] .. "\\voice_two "
      if voices >= 2 then
	 lilypond_footer = lilypond_footer .. "\\\\ " --[[.. naturalize_music]] .."\\voice_four "
      end
      if voices == 3 then
	 lilypond_footer = lilypond_footer .. "\\\\ " --[[.. naturalize_music]] .."\\voice_six "
      end
      lilypond_footer = lilypond_footer .. ">> \n"
   end
   lilypond_footer = lilypond_footer .. "\t\t\t>> \n"
   lilypond_footer = lilypond_footer .. "\n\t\\layout{ragged-bottom = ##t " .. ragged_right .. " " .. layout_addition .. "} \n}"

   return lilypond_footer
end -- end Lilypond Score function

-- Generate pdf from Lilypond file
generate_lilypond_pdf = function(score)
   local lilypond_file_name
   if command.generate_lilypond == "yes" then
      local instrument = command.instrument
      instrument = string.gsub(instrument," ","_")
      local algorithm = string.gsub(header.algorithm," ","_")
      lilypond_file_name = instrument .. "_" .. algorithm .. ".ly"
      output_lilypond_score = io.open(lilypond_file_name,"w")
      output_lilypond_score:write(score)
      output_lilypond_score:close()

      local file_name = (instrument .. "_" .. algorithm .. ".ly")

      os.execute("lilypond " .. file_name .. " 2> lilypond.txt")
      io.write("Generating Lilypond score ... done.\n")
   end
   return lilypond_file_name
end -- Generate pdf from Lilypond file

-- generate_lilypond_c_octave_table = function()
--    -- tmp_table = {}
--    -- local counter = 1
--    -- for _,value in pairs(c_octave_table) do
--    --    tmp_table[counter] = value --; print(tmp_table[counter])
--    --    counter = counter + 1
--    -- end
--    -- table.sort(tmp_table)
--    -- -- for key,value in pairs(tmp_table) do print(key,value) end
--    -- return tmp_table
   
-- end
