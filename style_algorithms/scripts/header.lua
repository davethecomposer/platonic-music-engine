-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

header = {}
command = {}
header.lilypond_version = "2.19.82"
header.paper = "letter"
header.version = "Heraclitus"

require("scripts/get_input")
require("scripts/get_args")

require("scripts/tuning") ; require("scripts/tuning_tables")

require("scripts/generic_functions")

require("scripts/csound") ; require("scripts/csound_instruments_tables")
require("scripts/latex_creation")

require("scripts/music_functions") ; require("scripts/music_functions_tables")

require("scripts/pre_parse") ; require("scripts/pre_parse_tables")

random = require("scripts/pcg")
