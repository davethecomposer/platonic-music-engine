-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

platonic_generator = function(name, low, high, num_voices, round)
   local result = {}
   local seed = header.user_data .. name
   local counter = 0
   local highnum,lownum = 0, 0
   do -- Random number generator
      -- math.randomseed(hash(seed))
      random.seed(hash(seed),hashstream(seed))
      for counter = 1,num_voices do
	 if low == -1 then
	    result[counter] = random.randomDouble() + .5
	 elseif round == "yes" then 
	    result[counter] = random.random(low, high)
	 elseif round == "no" then
	    if low == high then print("low and high are the same") os.exit() end
	    result[counter] = random.random(low, high -1) + random.randomDouble() +.5
	 end
      end
   end
   return result
end

get_scale_octave = function() -- Get baserange from mode
   local key = command.scale
   local scale_degrees = calculated_scale_degrees
   
   local tmp_table={}
   local temperament_octave = calculated_scale_degrees.P8 --header.tuning_octave
   -- if string.sub(key,1,1) == "#" then
   --    tmp_table = key:split(":")
   --    result = tmp_table[2] 
   --    key = tmp_table[1] 
   -- else
   _,mode = bifurcate(key,"-")
   if mode == "Panchromatic" then result = temperament_octave
   else
      -- local tmp_result = scale_key[mode].repeat_point
      mode = string.gsub(mode," ","_")
      local tmp = io.open("scripts/scales/" .. mode .. ".pscl")
      local data = tmp:read("*a")
      tmp:close()
      local _,name = bifurcate(data,"name:") 
      local name,restof = bifurcate(name,"description:")
      local description,rest = bifurcate(restof,"\nrepeat point:") 
      local repeat_point,tmp_scale_pattern = bifurcate(rest,"\nscale:") 
      
      name = string.gsub(name,"^%s*", "")
      description = string.gsub(description,"^%s*", "")
      repeat_point = string.gsub(repeat_point,"^%s*", "")
      local stuff = string.gsub(tmp_scale_pattern," ","") 
      repeat_point = string.gsub(repeat_point,"^%\n*","") -- remove leading newlines    
      repeat_point = string.gsub(repeat_point,"%\n*$","") -- remove all trailing newlines

      result = scale_degrees[repeat_point] 
   end
   
   if result == nil then result = 12 end -- I wonder why this exists? I mean it's a nice fallback but this information should be in the table
   -- end 
   return result--,key
end  

instrument_range = function(freq_table, low_freq, high_freq)
   local instrument = command.instrument
   local tuning_name = command.tuning_name
   c_octave_table = {} 
   local low_match_value = midi_instrument_list[instrument].low_range
   local high_match_value = midi_instrument_list[instrument].high_range
   local low_match_octave = string.match(low_match_value,"%d+")
   if string.find(low_match_value,"-") then low_match_octave = -1 * low_match_octave end
   local high_match_octave = string.match(high_match_value,"%d+")
   if string.find(high_match_value,"-") then high_match_octave = -1 * high_match_octave end
   local middle = math.floor((low_match_octave + high_match_octave)/2)

   local low_range = scientific_pitch_table[low_match_value] 
   local high_range = scientific_pitch_table[high_match_value] 

   local lowrange = tonumber(binary_search(low_range,low_freq,high_freq,freq_table))

   if freq_table[lowrange] < (low_range - .00000001) then lowrange = lowrange + 1 end
   
   local highrange = tonumber(binary_search(high_range--[[high_match_value]],low_freq,high_freq,freq_table))
   if freq_table[highrange] > (high_range + .1) then highrange = highrange -1 end
   
   local midi_num = midi_instrument_list[instrument].midi_instrument

   for counter = -1,9 do
      local c_octave = "c"
      c_octave = c_octave .. counter
      local match = (scientific_pitch_table[c_octave]) -- / ((header.tuning_octave * header.repeat_ratio / 2)/12))
      c_octave_table[c_octave] = tonumber(binary_search(match,low_freq,high_freq,freq_table))
   end
   
   -- Let's find the middle pitch of the instrument based on the tuning root. Useful in a number of places.
   local root, _ = bifurcate(tuning_name,"-")
   middle = "c" .. middle
   middle = c_octave_table[middle] 
   -- find distance from root to C
   local interval_distance = distance_of_C_to_key[root]
   interval_distance = calculated_scale_degrees[interval_distance] 
   -- add that to middle
   middle = middle + interval_distance
   -- return that value for storing in header global variable.

   header.middle_of_instrument = middle
   write_log("Instrument Range","lowrange",lowrange," = " .. freq_table[lowrange] .. " Hz")
   write_log("Instrument Range","highrange",highrange," = " .. freq_table[highrange] .. " Hz")
   write_log("Instrument Range","c_octave_table",c_octave_table)
   write_log("Instrument Range","header.middle_of_instrument",header.middle_of_instrument)
   return midi_num, lowrange, highrange
end

quantize = function(table_name, base_table, number_notes)
   if number_notes == nil then
      number_notes = command.number_of_notes
   end
   random = require("scripts/pcg")
   local result = {}
   local seed = header.user_data .. table_name
   
   do -- Random number generator
      random.seed(hash(seed),hashstream(seed))
      for counter = 1, number_notes do
	 local len_base_table = #(base_table)
	 if len_base_table == 0 then len_base_table = 1 end
	 local index = random.random(1,len_base_table) 
	 result[counter] = base_table[index] 
      end
   end
   write_log("music_functions.lua","quantize " .. table_name,result)
   return result
end

-- Bel Canto function
-- raises or lower successive notes by an octave to get them within a perfect fifth
-- of previous note. Fails at extremes of instrument range. The *normalize* parameter
-- if set to yes raises or lowers the first note to within a perfect fifth of the middle note
-- of the instrument. Otherwise the first note stays the same and everything after is altered.

belcanto = function(belcanto_string, calculated_scale_degrees, belcanto_number_of_notes, normalize, interval_adjustment)
   if not interval_adjustment then interval_adjustment = "P8" end
   local bel_number = calculated_scale_degrees.P5 --(math.floor(belcanto_baserange/2))-1 print(bel_number)
   local mid_number = header.middle_of_instrument --(math.floor(header.lowrange + header.highrange)/2)

   local tmp = calculated_scale_degrees[interval_adjustment] 
   if not tmp then tmp = tonumber(interval_adjustment) end
   local belcanto_baserange = tmp 

   if normalize == "yes" then start = 1 ; belcanto_string[0] = mid_number else start = 2 end 
   for counter = start,belcanto_number_of_notes do
      local secondnote = tonumber(belcanto_string[counter]) ; local firstnote=tonumber(belcanto_string[counter-1]) 
      while secondnote > firstnote + bel_number do secondnote = secondnote - belcanto_baserange end 
      while secondnote < firstnote - bel_number do secondnote = secondnote + belcanto_baserange end
      if secondnote > header.highrange then secondnote = secondnote - belcanto_baserange end
      if secondnote < header.lowrange then secondnote = secondnote + belcanto_baserange end
      
      belcanto_string[counter] = secondnote 
   end
   
   return(belcanto_string)
end --function

generate_panchromatic_scale = function(range)
   local pitch = {} ; local duration = {} ; local velocity = {}
   local start = 0 ; local stop = 0
   local octave = calculated_scale_degrees.P8
   
   if range == "full" then
      start = header.lowrange ; stop = header.highrange 
   elseif range == "octave" then
      start = header.middle_of_instrument ; stop = start + octave
   else 
      print("Either full or octave") ; os.exit()
   end

   local increment = 1
   for counter = start,stop do
      pitch[increment] = counter
      velocity[increment] = 100
      duration[increment] = 1
      increment = increment + 1
   end

   local new_number = increment - 1

   return pitch, velocity, duration, new_number
end

analyze_intervals = function()
   tuning = command.tuning_name
   local analyze_table = table.pack("P1","m2","M2","m3","M3","P4","TT","P5","m6","M6","m7","M7","P8")
   local file_name = string.gsub(header.algorithm," ","_") .. ".log"
   local log_file = io.open(file_name, "a")

   log_file:write("Analysis of the generated intervals for " .. tuning .. " compared to Platonic Ideals (5-lim JI)\n")

   log_file:write(string.format("%-8s %-2s %-11s %-2s %-10s %-2s %-7s %-2s %-11s %-2s %-11s %-2s %-16s","Interval","|","Your ratio","|","Cents", "|", "Ideal","|","% of Ideal","|","Cents off","|","Assessment") .. "\n")

   for subcounter = 1,94 do
      log_file:write("-")
   end
   log_file:write("\n")
   
   for counter = 1,#analyze_table do
      local assessment = nil
      local interval = analyze_table[counter] 
      local csd = ratio_table[calculated_scale_degrees[interval]]
      local pd = platonic_degree[interval] 
      local cents = 1200 * math.log((csd),2)
      local cents_off = 1200 * math.log((csd/pd),2)
      if math.abs(cents_off) <= 2 then
	 assessment = "Excellent!"
      elseif math.abs(cents_off) <= 10 then
	 assessment = "Good"
      elseif math.abs(cents_off) <= 20 then
	 assessment = "Ok"
      else
	 assessment = "Poor"
      end

      log_file:write(string.format("%-8s %-2s %-11s %-2s %-10s %-2s %-7s %-2s %-11s %-2s %-11s %-2s %-16s" , interval , "|", round(csd,4), "|", round(cents,4), "|", round(pd,4), "|", round(csd/pd * 100 ,2) , "|", round(cents_off,4), "|", assessment) .. "\n")

      if counter % 2 == 1 then
	 for subcounter = 1,94 do
	    log_file:write("-")
	 end
	 log_file:write("\n")
      end
   end

   log_file:write("\n\n")
   log_file:close()
   
   return
end

calibrate_to_a4 = function()
   local calibration = 1
   local c4 = c_octave_table.c4
   local a4 = c4 + calculated_scale_degrees.M6
   local a4_freq = audio_freq_table[a4]
   if #audio_freq_table >= 24 then
      calibration =  440/a4_freq
   end
   write_log("Old reference frequency (a4)", "audio_frequency", a4_freq)
   write_log("Calibrated reference frequency", "calibration", calibration)
   write_log("New reference freqency (a4)", "audio_frequency", audio_freq_table[a4] * calibration)
   return 1 --calibration -- Have disabled this for now as I'm not sure about it.
end
