get_args =   function(args)
   for counter = 1,#args do
      if args[counter] == "--log" then gen_log = "yes" end
      if args[counter] == "-l" or args[counter] == "--lilypond" then command.generate_lilypond = "yes" end
      if args[counter] == "-F" or args[counter] == "--Feldman" then command.generate_feldman = "yes" end
      if args[counter] == "-p15" or args[counter] == "--pattern15" then command.generate_pattern_15 = "yes" end
      if args[counter] == "-p35" or args[counter] == "--pattern35" then command.generate_pattern_35 = "yes" end
      if args[counter] == "-o" or args[counter] == "--ogg" then command.ogg = "yes" end
      if args[counter] == "-f" or args[counter] == "--flac" then command.flac = "yes" end
      if args[counter] == "-w" or args[counter] == "--wave" then command.wave = "yes" end
      if args[counter] == "-n" or args[counter] == "--number-notes" then command.number_of_notes = args[counter + 1] end
      if args[counter] == "-ins" or args[counter] == "--instrument" then command.instrument = args[counter + 1] end
      if args[counter] == "-t" or args[counter] == "--tempo" then command.tempo_word = args[counter + 1] end
      if args[counter] == "-rp" or args[counter] == "--reference-pitch" then command.system = args[counter + 1] end
      if args[counter] == "-i" or args[counter] == "--interactive" then command.user_input = "" end
      if args[counter] == "-r" or args[counter] == "--random" then command.user_input = "r" end
      if args[counter] == "-u" or args[counter] == "--use" then command.user_input = args[counter + 1] end
      if args[counter] == "-sr" or args[counter] == "--scale-recipe" then command.scale_recipe = args[counter + 1] end
      if args[counter] == "-sf" or args[counter] == "--scale-fill" then command.scale_fill = tonumber(args[counter + 1]) end
      if args[counter] == "-or" or args[counter] == "--octave-recipe" then command.octave_recipe = args[counter + 1] end
      if args[counter] == "-dyr" or args[counter] == "--dynamics-recipe" then command.dynamics_recipe = args[counter + 1] end
      if args[counter] == "-dur" or args[counter] == "--durations-recipe" then command.durations_recipe = args[counter + 1] end
      if args[counter] == "-s" or args[counter] == "--scale" then command.scale = args[counter + 1] end
      if args[counter] == "-sc" or args[counter] == "--score" then command.generate_score = "yes" end
      if args[counter] == "--tuning" then command.tuning_name = args[counter + 1] end
      if args[counter] == "-c" or args[counter] == "--command" then command[args[counter +1]] = args[counter +2] end
      if args[counter] == "-v" or args[counter] == "--version" then
	 print("The Platonic Music Engine is at version")
	 print("Heraclitus")
	 print("Use the -h flag for a list of command line options")
	 os.exit()
      end
      if args[counter] == "-h" or args[counter] == "--help" then
	 print("The following flags are for notation styles")
	 print("-l, --lilypond \t\tgenerate Lilypond file (standard music notation)")
	 print("-F, --Feldman \t\tgenerate Feldman graph notation")
	 print("-p15, --pattern15 \tgenerate Kirkpatrick graphic notation Pattern 15")
	 print("-p35, --pattern35 \tgenerate Kirkpatrick graphic notation Pattern 35")
	 print("----------------------------")
	 print("The following flags are for audio files")
	 print("-o, --ogg \t\tgenerate ogg audio file")
	 print("-f, --flac \t\tgenerate flac audio file")
	 print("-w, --wave \t\tgenearte wave audio file")
	 print("----------------------------")
	 print("The following flags are for entering the initial input")
	 print("-i, --interactive \tinteractive mode that asks for initial input")
	 print("-r, --random \t\tuses randomly generated string for initial input")
	 print("-u, --use \"input\" \tuses value in \"input\" for initial input")
	 print("----------------------------")
	 print("The following sets various musical parameters")
	 print("-n, --number-notes \"input\" \tsets number of notes")
	 print("-ins, --instrument \"input\" \tsets the instrument")
	 print("-t, --tempo \"input\" \t sets the tempo")
	 print("-rp, --reference-pitch \"input\" \tsets the reference pitch")
	 print("-s, --scale \"input\" \tsets the scale")
	 print("--tuning \"input\" \tsets how the instrument is tuned")
	 print("----------------------------")
	 print("The following flags indicate how to emphasize musical output")
	 print("-sr, --scale-recipe \"input\" \t set scale recipe")
	 print("-sf, --scale-fill \"input\" \t set scale fill")
	 print("-of, --octave-recipe \"input\" \t set octave recipe")	 
	 print("-dyr, --dynamics-recipe \"input\" \t set dynamics recipe")
	 print("-dur, --durations-recipe \"input\" \t set durations recipe")
	 print("----------------------------")
	 print("--log \t\t\tgenerates log file with interesting information")
	 print("-h, --help \t\tprints out this helpful information")
	 os.exit()
      end
   end 
   return 
end


