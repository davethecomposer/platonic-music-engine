-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

reference_pitch = function()
   local concert_tuning_system = command.system
   local tuned_to = 0 ; local system = ""
   if tonumber(string.sub(concert_tuning_system,1,1)) then 
      tuned_to = tonumber(concert_tuning_system) 
      system = "custom reference pitch (A4 = "..tuned_to..")"         
   else
      tuned_to = concert_pitch_system_list[concert_tuning_system].tuning   
      system = concert_pitch_system_list[concert_tuning_system].name
   end
   write_log("tuning.lua","reference pitch",tuned_to)
   return tuned_to, system
end

get_tuning_range = function(scale)
   local range = 0
   local tmp_table = {} 
   local method
   local interval
   local tune_table = {}
   
   local starting_pitch,tmp = bifurcate(scale,"-")
   
   local num_div,tmp2 = bifurcate(tmp,"/") 
   if tmp2 then
      method,interval = bifurcate(tmp2,":")
   end
   
   if method == "ED" or method == "!ED" or method == "EDO" or method == "!EDO" or method == "notED" or method == "notEDO" or method == "Harmonic" then 
      range = num_div
   elseif method =="CET" then range = math.ceil(1200/num_div)
   else  
      file_name = string.lower(tmp)
      file_name = string.gsub(file_name,"^%s*", "")
      file_name = string.gsub(file_name," ","_")

      local tmp = io.open("scripts/tunings/" .. file_name .. ".ptun") 
      if not tmp then
	 print("Tuning not found, check name")
	 os.exit()
      end

      local data = tmp:read("*a")
      tmp:close()
      local _,name = bifurcate(data,"name:") 
      local name,rest = bifurcate(name,"\ndescription:") 
      local description,tuning = bifurcate(rest,"\ntuning:") 

      name = string.gsub(name,"^%s*", "")
      description = string.gsub(description,"^%s*", "")
      local stuff = string.gsub(tuning," ","")
      stuff = string.gsub(stuff,"^%\n*","")     
      stuff = string.gsub(stuff,"%\n*$","")
      
      tune_table = stuff:split("\n")
      
      range = #tune_table 
   end 
   return range, starting_pitch, num_div, method, interval, tune_table
end

generate_audio_frequency_table = function()
   local tuning = command.tuning_name
   local instrument = command.instrument
   ratio_octave_table = {}
   local audio_freq = {}

   local rotate_number = 0
   local rotate_degree_value = 0
   local rotate = 0 -- This is the value that we're ultimately looking for ; set to 0 for equal-division tunings
   local tave = 0
   local divisions = 1 -- dummy value so that when the EDO stuff takes over everything else will work. Sigh.
   local degrees = ""
   local not_equal = "no" -- variable so we don't have to check for = and $ after this first time
   local mode
      
   -- 2 September 2016
   -- The following math.ceil bit is necessary for fractional tunings (eg, pi-edo, 1.1) to work.
   -- Otherwise the tuning_range is fractional and it throws off the line:
   -- reference_freq = initial_reference_freq * (1/ratio_octave_table[tuning_range - rotate])
   -- should probably investigage further?

   local full_tuning_range, starting_pitch, num_div_or_scale, method, interval, tune_table = get_tuning_range(tuning) 
   local tuning_range = math.ceil(full_tuning_range)

   ratio_octave_table[0] = 1 
   ratio_octave_table[1] = 2  -- This is for 0-EDO to work. Sigh.
   if method ~= "ED" and method ~= "!ED" and method ~="CET" and method ~= "EDO" and method ~="!EDO" and method ~= "notED" and method ~= "notEDO" then
      if method ~= "Harmonic" then
	 not_equal = "yes" ; mode = num_div_or_scale 
	 
	 for counter = 1,tuning_range do
	    local tmp_value = tune_table[counter] 
	    if string.find(tune_table[counter],"cent") then
	       tmp_value = string.gsub(tmp_value,"cents","") ; tmp_value = string.gsub(tmp_value,"cent","")  
	       tmp_value = load("return " .. tmp_value)()
	       ratio_octave_table[counter] = (2^((tmp_value)/1200))
	    else
	       tmp_value = load("return " .. tmp_value)()
	       ratio_octave_table[counter] = tmp_value 
	    end
	 end

      else
	 local tmp_tuning = {}
	 tmp_tuning[1] = 1
	 local counter = 0

	 for i = 1, num_div_or_scale do
	    for j = 1, num_div_or_scale - 1 do
	       local tmp = i/j 
	       if tmp <= 2 and tmp >= 1 then
		  counter = counter + 1
		  tmp_tuning[counter] = tmp
	       end
	    end
	 end
	 table.sort(tmp_tuning)
	 local counter = 0
	 for i = 1, #tmp_tuning do
	    local tmp = tmp_tuning[i]
	    local next_tmp = tmp_tuning[i + 1]
	    if tmp ~= next_tmp then
	       ratio_octave_table[counter] = tmp
	       counter = counter + 1
	    end
	 end

	 tuning_range = #ratio_octave_table
	 -- **************************
	 -- The following is taken directly from the lines below dealing
	 -- with n-EDOs where 0 < n < 1
	 -- the only change is to the audio_freq table which are altered
	 -- to match 0-EDO
	 if tonumber(num_div_or_scale) == 1 then
	    reference_freq = equal_base_frequencies[starting_pitch] 
	    tuning_range = 0
	    ratio_octave_table[1] = 10
	    local low_octave = midi_instrument_list[instrument].low_range 
	    local high_octave = midi_instrument_list[instrument].high_range
	    low_octave = string.match(low_octave, "%d+") + 2 --print(low_octave)
	    high_octave = string.match(high_octave, "%d+") + 2 -- print(high_octave)
	    local difference = high_octave - low_octave 	   
	    local average = math.floor((low_octave + high_octave) / 2) 
	    reference_freq = reference_freq * (2 ^ average)
	    audio_freq[0] = 0
	    audio_freq[1] = reference_freq
	    ratio_octave_table[1] = 10
   	 end
	 -- Done with this copy'n'paste
	 -- *************************
      end

   elseif method == "ED" or method == "!ED" or method == "EDO" or method == "!EDO" or method == "notEDO" or method == "notED" then 
      divisions = tonumber(num_div_or_scale)
      if divisions > 0 and divisions < .001 then divisions = .001 end

      if not interval then interval = "P8" end

      tave = platonic_degree[interval]

      if not tave then -- Equal division of cents
	 tave,_ = bifurcate(interval," ")
	 tave = 2^((tave)/1200)
      end
      
      local increment = 1

      if tonumber(divisions) ~= 0 then -- This is what makes 0-EDO work. Sigh. I feel like it should be more elegant or something.

	 for counter = 1,tuning_range do 
	    ratio_octave_table[counter] = tonumber(tave)^(counter/tonumber(divisions)) 
	    increment = increment + 1
	 end 
	 
	 if method == "!ED" or method == "!EDO" or method == "notED" or method == "notEDO" then 
	    repeat_ratio = tave
	    ratio_octave_table[#ratio_octave_table] = tave
	 end

	 while ratio_octave_table[increment - 1] < 2 do -- had this at 4 (two octaves) but it screws up using scales, d'oh
	    -- This is complicated but will try. Right now ratio_octave_table just has the ratios based on our EDI. increment 
	    -- holds the next value to be computed to fill up our 2:1 octave. tmp_value is our modified modulo calculation that
	    -- makes sure that we squeeze in our fractional !EDI value. That's the tricky bit.
	    -- if tuning_range <= 2 then print(tuning_range) tuning_range = tuning_range ^ 2 print(tuning_range) end
	    local tmp_value = (increment - tuning_range)  
	    while tmp_value > tuning_range do tmp_value = tmp_value - tuning_range end 
	    ratio_octave_table[increment] = ratio_octave_table[increment - tmp_value] * (ratio_octave_table[tmp_value]) 
	    increment = increment + 1  
	 end 
	 tuning_range = increment -1 
      end
   elseif method == "CET" then
      for counter = 1,tuning_range do
	 mode = num_div_or_scale
	 ratio_octave_table[counter] = 2^((mode*counter)/1200)

      end
      
   end   

   local rotate_degree = distance_of_C_to_key[starting_pitch] 
   rotate_degree_value = platonic_degree[rotate_degree] 
   tuning_range = tonumber(tuning_range) 
   if not_equal == "yes" then

      rotate = binary_search(rotate_degree_value,1,tuning_range,ratio_octave_table) 
      
      reference_freq = equal_base_frequencies[starting_pitch] 
      
   else  
      rotate = binary_search(rotate_degree_value,0,tuning_range,ratio_octave_table)
      reference_freq = equal_base_frequencies[starting_pitch] 
   end

   header.reference_frequency = reference_freq
   repeat_ratio = load("return "..ratio_octave_table[#ratio_octave_table])() 
   header.repeat_ratio = repeat_ratio
   header.tuning_rotate = rotate 

   -- do -- Made major changes elsewhere that I think work, needs more testing here 9 Nov 2016
   --    -- Check to see if our base frequency lines up with our reference pitch
   --    -- With certain tunings there might not be the symmetry between the inverted interval.
   --    -- In other words, going from A to C and calling that a minor 3rd might not result
   --    -- in the C to A being a major 6th. This code moves that C until the A is a major sixth
   --    -- in order to preserve our A4=X-Hz tuning. Or something. 
   --    local calibrate = distance_of_key_to_A[starting_pitch] -- print("calibrate = ",calibrate)
   --    local calibrate_value = platonic_degree[calibrate] -- print("calibrate_value = ",calibrate_value)
   --    local calibrate_freq = (reference_freq * calibrate_value)  -- print("cal_freq = ",calibrate_freq)
   --    local calibrate_ratio = (calibrate_freq/(initial_reference_freq*2)) -- print("cal_ratio = ",calibrate_ratio)

   --    if calibrate_ratio > 1.01 or calibrate_ratio < .99 then 
   -- 	 delta_freq = (initial_reference_freq*2)/calibrate_freq -- print("delta=",delta_freq)
   -- 	 reference_freq = reference_freq * delta_freq 
   -- 	 -- rotate = round(rotate * delta_freq)     
   -- 	 -- local reference_number = 69 + rotate - tuning_range  
   -- 	 write_log("Calibrated reference_freq","calibrate_ratio",calibrate_ratio)
   -- 	 header.reference_frequency = header.reference_frequency * 1/calibrate_ratio
   --    end 
   -- end
   -- local original_reference_number = reference_number -- needed later when we calculate the octave pitch table below

   local freq_increment = 1
   
   if divisions <= .3 then 
      local low_octave = midi_instrument_list[instrument].low_range 
      local high_octave = midi_instrument_list[instrument].high_range
      low_octave = string.match(low_octave, "%d+") + 2 --print(low_octave)
      high_octave = string.match(high_octave, "%d+") + 2 -- print(high_octave)
      local difference = high_octave - low_octave 
      
      if divisions >= .2 then 
   	 if difference < (1/divisions) then
	    local average = math.floor((low_octave + high_octave) / 2) 
   	    reference_freq = reference_freq * (2 ^ average)
	    audio_freq[1] = 0
	    audio_freq[2] = reference_freq
	    ratio_octave_table[1] = 10
	 else
	    local first = round(high_octave * (1/3))
	    local second 
	    audio_freq[1] = 0
	    audio_freq[2] = reference_freq * (2 ^ first)
	    audio_freq[3] = audio_freq[2] * ratio_octave_table[1]
	    ratio_octave_table[0] = 1
	    ratio_octave_table[1] = 1/divisions
   	 end
	 
      else
   	 local average = math.floor((low_octave + high_octave) / 2) 
   	 reference_freq = reference_freq * (2 ^ average) 
	 audio_freq[0] = 0
   	 audio_freq[1] = reference_freq 
	 ratio_octave_table[1] = 10
      end      
   else
      while reference_freq <= 22050 do
	 for counter = 0, tuning_range -1 do 
   	    audio_freq[freq_increment] = reference_freq * ratio_octave_table[counter]
   	    freq_increment = freq_increment + 1
   	 end
   	 reference_freq = reference_freq * repeat_ratio
      end      
   end

   local high_audio_freq_num = #audio_freq --reference_number --audio_freq[reference_number]
   local low_audio_freq_num = 1 --reference_number + tuning_range --audio_freq[reference_number + tuning_range]

   write_log("tuning.lua","instrument",instrument)
   write_log("tuning.lua:generate_audio_frequency_tables","audio_freq",audio_freq)
   write_log("tuning.lua:generate_audio_frequency_tables","ratio_octave_table",ratio_octave_table)
   
   -- Global variable for octave table
   
   header.repeat_ratio = repeat_ratio
   ratio_table = shallowcopy(ratio_octave_table)
   return audio_freq, low_audio_freq_num, high_audio_freq_num
end

generate_relative_scale_degrees = function()
   local scale = command.scale
   local scale_degrees = {}
   local local_ratio_table = {}
   local platonic_table = get_keys_from_table(platonic_degree)

   local key,mode = bifurcate(scale,"-") 
   
   local increment = 0 
   local tuning_range =  #ratio_table 
   if tuning_range == 0 then tuning_range = 1 end

   local_ratio_table[0] = 1
   
   if #audio_freq_table < 2 then -- Check for 0-EDO -- 2 Nov 2017
      local_ratio_table[1] = 10
   else
      for counter = 1, (#ratio_octave_table) * 2 do --#platonic_table do
	 local_ratio_table[counter] = audio_freq_table[counter + 1] / audio_freq_table[1] 
      end
   end
   
   tuning_range = #local_ratio_table
   for counter = 1,#platonic_table do 
      local degree = platonic_table[counter] 
      value = platonic_degree[platonic_table[counter]] 
      result = binary_search(value,0,tuning_range,local_ratio_table) 
      scale_degrees[degree] = result 
      increment = increment + 1
   end

   if mode == "major" then
      for degree,value in pairs(major_scale_degrees) do 
	 scale_degrees[degree] = scale_degrees[value]
      end
   end

   if mode == "minor" or mode == "harmonic minor" then
      for degree,value in pairs(minor_scale_degrees) do 
	 scale_degrees[degree] = scale_degrees[value]
      end
   end
   write_log("tuning.lua","generate_relative_scale_degrees",scale_degrees)
   return scale_degrees
end

find_intervals_from_pitches = function(pitch_table, freq_table, scale_degrees_table)
   local local_ratio_table = {} ; local reverse_scale_degrees_table = {}
   local result = ""
   local c_table = get_keys_from_table(c_octave_table)
   for k, v in pairs(scale_degrees_table) do
      reverse_scale_degrees_table[v] = k
   end
   
   for counter = 1, #pitch_table do
      local pitch = pitch_table[counter] 
      for sub = -1, 8 do
      	 local tmp = "c" .. sub
      	 local tmp1 = "c" .. sub +1
      	 if pitch >= c_octave_table[tmp] and pitch < c_octave_table[tmp1] then
      	    tmp_result = tmp
      	 end
      end
      local tmp = reverse_scale_degrees_table[pitch_table[counter] - c_octave_table[tmp_result]] 
      result = result .. tmp 
      if counter < #pitch_table then result = result .. "," end
   end
   return result
end
