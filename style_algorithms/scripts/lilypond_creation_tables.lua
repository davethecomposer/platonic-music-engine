-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Lilypond duration list
lilypond_duration_list = {}
lilypond_duration_list = {
   [8] = "\\breve",
   [6] = "1.",
   [4] = "1",
   [3.75] = "2...",
   [3.5] = "2..",
   [3] = "2.",
   [2] = "2",
   [1.75] = "4..",
   [1.5] = "4.",
   [1] = "4",
   [.75] = "8.",
   [.5] = "8",
   [.425] = "16.",
   [.25] = "16",
   [.125] = "32",
   [.0625] = "64",
   [.03125] = "128",
}

-- Lilypond velocity table list
lilypond_velocity_list = {}
lilypond_velocity_list = {
   [0] = "r",
   [4] = "\\ppppp",
   [8] = "\\pppp",
   [13] = "\\ppp",
   [19] = "\\pp",
   [24] = "\\p",
   [31] = "\\mp",
   [39] = "\\mf",
   [47] = "\\f",
   [57] = "\\ff",
   [69] = "\\fff",
   [84] = "\\ffff",
   [100] = "\\fffff",
}

sharp2flat = {}
sharp2flat = {
   ["a"] = "a",
   ["as"] = "bf",
   ["b"] = "b",
   ["c"] = "c",
   ["cs"] = "df",
   ["d"] = "d",
   ["ds"] = "ef",
   ["e"] = "e",
   ["f"] = "f",
   ["fs"] = "gf",
   ["g"] = "g",
   ["gs"] = "af",
}

