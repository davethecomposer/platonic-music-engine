-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Text Mesostic"
header.style = "" 
header.style_algorithm_author = "David Bellows"

command.user_input = "Heraclitus" 
command.generate_score = "no"
number_of_poems = 4
command.number_of_notes = number_of_poems

local full_text = "walden_thoreau" -- "test" -- "common" -- "all" --"walden_thoreau"
local spine = "John Cage"

-- In the following sections are various options one can use. Options are either in
-- a "Cage" section or a "Not Cage section. The "Cage" ones are more in line
-- with how Cage composed mesostics

local rule = "100%" -- "0%" "50%" or "100%"
-- Cage
-- "50%" allows wing words that have the current spine but not the previous one
-- "100%" does not allow for any wing words that contain the current or previous spine
-- Not Cage
-- "0%" allows wing words that have either the current or the previous spine

local word_rule = "linear" -- "random" "linear"
-- Cage
-- "linear" searching for spine words starts from the beginning and increments by each word
-- Not Cage
-- "random" searching for spine words happens at random spots within the source

local wing_rule = "neither" -- "50/50", "surrounding", "both", "neither"
-- Cage
-- "surrounding" given a set of wing words, this option randomly determines how many wing word to use by
-- removing some or none from the ends while preserving the order
-- "neither" uses all available wing words (up to eleven on each side) but following the 45 character limit
-- Not Cage
-- "50/50" there's a 50% chance that any eligible wing word is used or not
-- "both" uses both "surrounding" and "50/50"

local punctuation_rule = "yes" -- "yes" "no"
-- Cage
-- "yes" means that punctuation breaks are not included in left wing and end right wing
-- "no" allows punctuation breaks

local bold = "yes" -- "yes" "no"
-- Cage
-- "no" do not bold the spine letter in mesostic
-- Not Cage
-- "yes" bold the spine letter in mesostic

local style = "regular" -- "regular", "merce", "cantos"
-- Cage
-- "merce" simulates Cage's typographic manipulations in his *62 Mesostics re Merce Cunningham*
-- "cantos" simulates Cage's *Writing Through the Cantos*
-- "regular" just normal stuff

command.generate_text_file = "no" -- "yes" "no"
command.generate_lyric_file = "no" -- "yes" "no"
command.generate_latex_file = "yes" -- "yes" "no"
command.generate_log = "yes" -- "yes" or "no"

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()
number_of_poems = command.number_of_notes

-------------------------------------------------------------------

require("dir_mesostic/text_mesostics")

command.number_of_notes = number_of_poems
local text_table, title, author = import_text(full_text)

local mesostic_table, mesostic_number_table, number_of_characters, cleaned_spine = find_mesostics(text_table, spine, rule, wing_rule, word_rule, punctuation_rule)

local formatted_mesostic_table = format_mesostics(mesostic_table, number_of_characters, cleaned_spine, style)

title = string.gsub(title, " ", "_")
author = string.gsub(author, " ", "_")

local mesostic_name = "Mesostic_for_" .. title .. "_by_" .. author

local printable_text = generate_text(mesostic_name, formatted_mesostic_table)
local lyric_text = generate_lyrics(mesostic_name, mesostic_table)
generate_latex(mesostic_table, lyric_text, mesostic_name, formatted_mesostic_table, title, author, spine, cleaned_spine, bold, style)
