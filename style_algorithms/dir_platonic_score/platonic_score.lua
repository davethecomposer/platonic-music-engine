  -- Platonic Music Engine -- manipulate music in interesting ways.
  -- Copyright (C) 2016 David Bellows davebellows@gmail.com

  -- This program is free software: you can redistribute it and/or modify
  -- it under the terms of the GNU Affero General Public License as
  -- published by the Free Software Foundation, either version 3 of the
  -- License, or (at your option) any later version.

  -- This program is distributed in the hope that it will be useful,
  -- but WITHOUT ANY WARRANTY; without even the implied warranty of
  -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  -- GNU Affero General Public License for more details.

  -- You should have received a copy of the GNU Affero General Public License
  -- along with this program. If not, see <http://www.gnu.org/licenses/>.

platonic_score_generator = function(sonic_events, audio_len, tempo, form)

   require("scripts/csound_instruments_tables")

   local algorithm = header.algorithm

   if audio_len ~= "max" then
      audio_len = (tempo/60) * audio_len
   end

   if audio_len == "max" then
      if form == "Vertical" and header.style ~="Time Lord Version" then audio_len = 2*((2^32)-1) end
   end

   local amplitude_table = {} ; local frequency_table = {} ; local start_table = {} ; local duration_table = {} ; local ins_num_table = {} ; local ins_name_table = {} ; midi_ins_num_table = {}

   amplitude_table = platonic_generator("amplitude_table",-1,-1,sonic_events,"no") -- -1,-1 means a random num between 0 & 1
   frequency_table = platonic_generator("frequency_table",0,136,sonic_events,"no")
   duration_table = platonic_generator("duration_table",0,2^32-1,sonic_events,"no") 
   start_table = platonic_generator("start_table",0,2^32-1,sonic_events,"no")
   ins_num_table = platonic_generator("ins_num_table",1,4,sonic_events,"yes")
   pan_table = platonic_generator("pan_table",-1,-1,sonic_events,"no")

   -- create timer
   amplitude_table[0] = 0 ; frequency_table[0] = 0 ; duration_table[0] = (2^32) *2 ; start_table[0] = 0 ; ins_num_table[0] = 0 ; pan_table[0] = .5 ; midi_ins_num_table[0] = -1

   for counter = 1, sonic_events do 
      frequency_table[counter] = (2^((frequency_table[counter]-69)/12)) * 440 
      amplitude_table[counter] = (amplitude_table[counter] ) * (1/sonic_events) -- Simplistic compression algorithm
      midi_ins_num_table[counter] = -1 -- dummy variable, doesn't matter
   end

   if form == "Horizontal" then
      start_table[1] = 0
      for counter = 2,sonic_events do
	 start_table[counter] = start_table[counter -1] + duration_table[counter-1] 
      end
      if audio_len == "max" then audio_len = start_table[sonic_events] + duration_table[sonic_events] end
   end

   -- Time Lord
   if header.style == "Time Lord Version" then
      random.seed(os.time())
      local voice_to_use = random.random(1,sonic_events) 
      local new_start = start_table[voice_to_use]
      for counter = 1, sonic_events do
	 if start_table[counter] >= new_start then 
	    start_table[counter] = start_table[counter] - new_start 
	 else
	    if start_table[counter] + duration_table[counter] > new_start then
	       duration_table[counter] = start_table[counter] + duration_table[counter] - new_start
	       start_table[counter] = 0
	    else
	       start_table[counter],duration_table[counter] = 0,0
	    end
	    
	 end	 	       
      end      
   end

   -- God's Eye View
   if header.style == "God-Eye View" then
      local god_max = 0
      if form == "Vertical" then god_max = (2^32)-1
      else god_max = start_table[sonic_events] + duration_table[sonic_events]
      end

      local scale = 0
      if form == "Vertical" then
	 scale = (audio_len/2) / god_max
      else scale = (audio_len) / god_max
      end
      
      for counter = 1,sonic_events do
	 start_table[counter] = start_table[counter] * scale
	 duration_table[counter] = duration_table[counter] * scale
      end
      duration_table[0] = audio_len
   end

   -- Record just for the audio length
   if header.style ~= "God-Eye View" then
      for counter = 1,sonic_events do
	 if start_table[counter] > audio_len then
	    start_table[counter],duration_table[counter] = 0,0
	 else
	    local current_duration = start_table[counter] + duration_table[counter]
	    if current_duration > audio_len then
	       duration_table[counter] = audio_len - start_table[counter]
	    end
	 end
      end
   end
   duration_table[0] = audio_len

   
   return amplitude_table, frequency_table, duration_table, start_table, ins_num_table, pan_table, ins_name_table 
end
