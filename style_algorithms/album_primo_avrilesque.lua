-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Album Primo Avrilesque"
header.style = "" 
header.style_algorithm_author = "David Bellows"

command.user_input = "Heraclitus" 
command.generate_score = "no"
command.number_of_notes = 10

command.generate_log = "yes" -- "yes" or "no"

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()

-------------------------------------------------------------------

require("dir_album_primo_avrilesque/album") 
generate_album_primo_avrilesque()
