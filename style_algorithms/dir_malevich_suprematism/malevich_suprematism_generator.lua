-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

style_table = {}
style_table = {"White on White", "Black Square", "Red Square", "Black Circle", "Black Cross", "Suprematism"}

function generate_malevich_suprematism_latex()
   local black_square
   local score
   if command.style == "" then
      random_style = table.unpack(platonic_generator("random_style", 1, #style_table, 1, "yes"))
      command.style = style_table[random_style]
   end
   write_log("malevich_suprematism_generator", "command.style", command.style)

   -- Black Square
   if command.style == "Black Square" then
      local lower_left = table.unpack(platonic_generator("lower_left", 2, 4, 1, "no"))
      upper_right = 20 - lower_left
      painting = "[fill=black](" .. lower_left .. "," .. lower_left .. ")" .. "rectangle(" .. upper_right .. "," .. upper_right .. ");"
      write_log("malevich_suprematism_generator", "black square dimensions", painting)
   end

   -- Black Circle
   if command.style == "Black Circle" then
      local radius = table.unpack(platonic_generator("radius", 40, 69, 1, "no")) 
      radius = radius /10 -- This allows us to get random number from 4 to 6.9 
      local x = table.unpack(platonic_generator("x", 7, 13, 1, "no")) 
      local y = table.unpack(platonic_generator("y", 7, 13, 1, "no")) 
      painting = "[fill=black](" .. x .. "," .. y .. ")circle[radius=" .. radius .. "];"
      write_log("malevich_suprematism_generator", "black circles dimensions", painting)
   end

   -- Red Square
   if command.style == "Red Square" then
      local llx = table.unpack(platonic_generator("llx", 2, 4, 1, "no"))
      local lly = table.unpack(platonic_generator("uly", 2, 4, 1, "no"))
      
      local ulx = table.unpack(platonic_generator("ulx", 2, 4, 1, "no"))
      local uly = table.unpack(platonic_generator("ulx", 16, 18, 1, "no"))

      local urx = table.unpack(platonic_generator("urx", 16, 18, 1, "no"))
      local ury = table.unpack(platonic_generator("ury", 16, 18, 1, "no"))

      local lrx = table.unpack(platonic_generator("lrx", 16, 18, 1, "no"))
      local lry = table.unpack(platonic_generator("lry", 2, 4, 1, "no"))

      painting = "[fill=red,draw=none](" .. "(" .. llx .. "," .. lly .. ") -- (" .. ulx .. "," .. uly .. ") -- ("
      painting = painting .. urx .. "," .. ury .. ") -- (" .. lrx .. "," .. lry .. ") -- cycle;"
      write_log("malevich_suprematism_generator", "red square dimensions", painting)
   end

   -- Black Cross
   if command.style == "Black Cross" then
      local hlx = table.unpack(platonic_generator("hlx", -1, 1, 1, "no")) 
      hlx = hlx + .5
      local hly = table.unpack(platonic_generator("hly", 7, 9, 1, "no")) 
      local hrx = 20 - hlx
      local hry = 20 - hly

      local vlx = hly
      local vly = hlx
      local vrx = hry
      local vry = hrx
      
      painting = "[fill=black](" .. hlx .. "," .. hly .. ")" .. "rectangle(" .. hrx .. "," .. hry .. ");"
      painting = painting .. "\n\\draw[fill=black](" .. vlx .. "," .. vly .. ")" .. "rectangle(" .. vrx .. "," .. vry .. ");"
      write_log("malevich_suprematism_generator", "black cross dimensions", painting)
   end

   -- White on White
   if command.style == "White on White" then
      local lx = table.unpack(platonic_generator("lx", 3, 4, 1, "no")) 
      local ly = table.unpack(platonic_generator("ly", 3, 4, 1, "no")) 
      local ux = 13 + lx
      local uy = 13 + ly
      local mx =  (lx + ux) /2
      local my =  (ly + uy) /2
      local rotate = table.unpack(platonic_generator("rotate" , 0, 90, 1, "no")) 

      painting = "[fill=blue!3,draw=none,rotate around={" .. rotate .. ":(" .. mx .. "," .. my .. ")}](" .. lx .. "," .. ly .. ")rectangle" .. "(" .. ux .. "," .. uy .. ");"
      write_log("malevich_suprematism_generator", "white on white dimensions", painting)
   end

      -- Suprematism
   if command.style == "Suprematism" then
      local hlx = table.unpack(platonic_generator("hlx", -1, 1, 1, "no")) 
      hlx = hlx + .5
      local hly = table.unpack(platonic_generator("hly", 7, 9, 1, "no")) 
      local hrx = 20 - hlx
      local hry = 20 - hly
      local vertical_lift = table.unpack(platonic_generator("vertical_lift", 1, 7, 1, "no"))

      local vlx = hly
      local vly = hlx
      local vrx = hry
      local vry = hrx

      hry = hry + vertical_lift
      hly = hly + vertical_lift
      painting = "[fill=red,color=red](" .. vlx .. "," .. vly .. ")" .. "rectangle(" .. vrx .. "," .. vry .. ");"
      painting = painting .. "\n\\draw[fill=black](" .. hlx .. "," .. hly .. ")" .. "rectangle(" .. hrx .. "," .. hry .. ");"

      write_log("malevich_suprematism_generator", "black cross dimensions", painting)
   end

   
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Art"
      local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
      local font = "newcent" ; local poet = "Inspired by Kazimir Malevich" ; local algorithm = header.algorithm
      local meter = ""
      local piece = "piece" 
      local instrument = command.style
      local symbol = ""
      local amount
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. latex_title

      local body = "\n\n\\begin{tikzpicture}"
      body = body .. "\n\\tikz[line width=1mm]{"
      body = body .. "\n\\draw " .. painting
      body = body .. "\n\\draw[gray](0,0)rectangle(20,20);"
      
      local score = score .. body .. "\n}\\end{tikzpicture}"

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\n\\end{document}"

      create_latex(score, "Malevich_Suprematism")

   end
   return
end
