-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Mesostics and Simple Melody for Piano and Voice"
header.style = "" 
header.style_algorithm_author = "David Bellows"
command.user_input = "John Cage" 

--------------------------------------------
--------------------------------------------
-- Mesostics for Sappho
--------------------------------------------
--------------------------------------------
local full_text = "song_sappho" -- "my_life_thoreau" --"raven_poe" -- "test" -- "common" -- "all" --"walden_thoreau"
local spine = "wine"
number_of_poems = 3
command.number_of_notes = number_of_poems

local rule = "50%" -- "0%" "50%" or "100%"
local word_rule = "linear" -- "random" "linear"
local wing_rule = "surrounding" -- "50/50", "surrounding", "both", "neither"
local punctuation_rule = "yes" -- "yes" "no"
local bold = "yes" -- "yes" "no"
local style = "merce" -- "merce", "cantos", "regular"
command.generate_text_file = "no" -- "yes" "no"
command.generate_lyric_file = "no" -- "yes" "no"
command.generate_latex_file = "yes" -- "yes" "no"
command.generate_log = "yes" -- "yes" or "no"
command.generate_lilypond = "yes"
command.lilypond_use_bpm = "word" -- "bpm" "word" "both"
----------------------------------------
-- Generate Mesostics
----------------------------------------
local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()
require("dir_mesostic/text_mesostics")

local text_table, title, author = import_text(full_text)

local mesostic_table, mesostic_number_table, number_of_characters, cleaned_spine = find_mesostics(text_table, spine, rule, wing_rule, word_rule, punctuation_rule)

local formatted_mesostic_table = format_mesostics(mesostic_table, number_of_characters, cleaned_spine, style)

title = string.gsub(title, " ", "_")
author = string.gsub(author, " ", "_")

local mesostic_name = "Mesostic_for_" .. title .. "_by_" .. author

local printable_text = generate_text(mesostic_name, formatted_mesostic_table)
local lyric_text = generate_lyrics(mesostic_name, mesostic_table)
local combined_lyrics = lyric_text
generate_latex(mesostic_table, lyric_text, mesostic_name, formatted_mesostic_table, title, author, spine, cleaned_spine, bold, style)
local lyrics = shallowcopy(mesostic_table)
--------------------------------------------
--------------------------------------------
-- Simple Melody
--------------------------------------------
--------------------------------------------
-- header.algorithm = "Simple Melody"
-- header.style = "" 
-- header.style_algorithm_author = "David Bellows"

command.command = "" -- full interactive mode
command.generate_log = "yes" -- "yes" or "no"
-- command.generate_lilypond = "yes"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" 
local _, number_of_words = string.gsub(lyric_text, "%S+", "")
command.number_of_notes = number_of_words 
command.instrument = "Connor Gibbs Voice"
command.tempo_word = "freely"
command.system = "western iso" 
command.scale = "c-ionian"
command.scale_recipe = "all" --"P1:5,P4:3,P5:4"
command.scale_fill = 1
command.octave_recipe = "all"
command.dynamics_recipe = "ff:1,pp:1"
command.duration_recipe = "8th:2,quarter:3" 
command.tuning_name = "c-Western Standard"

------------------------------------------
-- Generate Simple Melody
------------------------------------------

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"
local pitch_table = {} ; local frequency_table = {} ; local volume_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {} 
local midi_ins_number = 0

header.reference_pitch,header.reference_pitch_name = reference_pitch()

local tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

local scale_octave = get_scale_octave() 
midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

local base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

local base_table = {}
base_table = preparse_velocities()
volume_table = quantize("volume_table", base_table)

local base_table = {}
base_table = preparse_durations()
duration_table = quantize("duration_table", base_table)

-- Bel Canto style algorithm (smooths out the melody to use leaps less than a perfect fifth (if possible))
local normalize_note_to_middle = "yes"
local interval_adjustment = "P8"
pitch_table = belcanto(pitch_table, calculated_scale_degrees, command.number_of_notes, normalize_note_to_middle, interval_adjustment)

local a4_calibration = calibrate_to_a4()
for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration 
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end
start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()
local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/" .. sound_font .. "\" \nsfpassign   0, isf\n "
end
-- End Csound preamble
local start_of_table = 1
ins_num_table[1] = 1 
-- create specific Orchestra instrument
local orchestra_instrument = csound_instrument ; local instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
local csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num) 
local csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)
local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)
csound_score_ins = finish_csound_score_ins(csound_score_ins)
local csound_orchestra = csound_orchestra1 
local csound_score = csound_score_function1 .. "\n" .. csound_score_ins 
local orchestra_finish = create_finish_orchestra(tempo_number)
local csound_file = csound_preamble .. csound_orchestra .. orchestra_finish .. csound_score
local csound_file_name = create_csound_file(csound_file)
create_csound_audio_file(csound_file_name, ogg_tag)

-- Simpe Melody sheet music
local tmp_algorithm = header.algorithm
header.algorithm = header.algorithm .. " " .. full_text
header.algorithm = string.gsub(header.algorithm, "_", " ")
require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
local pme = "Platonic Music Engine" ; local original_composer = ""
local original_title = "" ; local voices = 1 

local time = "4/4" ; local no_indent = "no"
local rest_or_space = "r" ; local use_ragged_right = "no" ; local avoid_collisions = "no"

header.note_scheme = "simple"
-- header.note_scheme = "complex"

local lilypond_header = lilypond2header(pme, original_composer, original_title, additional_info)

local lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_2)

local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)

local complete_lilypond_score = lilypond_header..lilypond_score..footer

generate_lilypond_pdf(complete_lilypond_score)
header.algorithm = tmp_algorithm
------------------------------------------
------------------------------------------
-- Musical Mesostics
------------------------------------------
------------------------------------------

require("dir_musical_mesostic/mesostics") ; require("dir_musical_mesostic/musical_mesostic_tables")
command.number_of_notes = 300
local repeat_motif = "no"
local colorize = "yes"
local format = "standard" -- acrostic, mesostic, standard
local simple_pitch_table = shallowcopy(pitch_table)
local simple_calculated_scale_degrees = shallowcopy(calculated_scale_degrees)

command.instrument = "Piano"
command.tempo_word = "andante"
command.system = "western iso"
command.scale_recipe = "all"
command.scale_fill = 1
command.octave_recipe = "C4:1" 
command.dynamics_recipe = "pppp"
command.duration_recipe = "16th"
command.tuning_name = "c-Western Standard"

-----------------------------------------

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"  

local frequency_table = {} ; local volume_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {}

local midi_ins_number = 0

local tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

local scale_octave = get_scale_octave() 
midi_ins_number, header.lowrange, header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

local base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

local base_table = {}
base_table = preparse_velocities(dynamics_recipe)
volume_table = quantize("volume_table", base_table)

local base_table = {}
base_table = preparse_durations(duration_recipe)
duration_table = quantize("duration_table", base_table)

----------------------------------------------
-- Music mesostic function
----------------------------------------------
musical_mesostic_seed = {}
local motif_melody = find_intervals_from_pitches(simple_pitch_table, audio_freq_table, simple_calculated_scale_degrees)
musical_mesostic_seed = {
   ["Simple Melody"] = {motif = motif_melody, composer = "David Bellows"}
 }

local motif = "Simple Melody"

volume_table, lilypond_spot, command.number_of_notes, red_spot = musical_mesostic(pitch_table, volume_table, motif, repeat_motif, format, calculated_scale_degrees)

write_log("musical_mesostic.lua","mesostic volume_table",volume_table)

---------------------------------------------
local a4_calibration = calibrate_to_a4()

for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
end	     

local start_of_table = 1
ins_num_table[1] = 1

---------------------------------------
-- create specific Orchestra instrument
local orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument

local csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num)
local csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)

local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

csound_score_ins = finish_csound_score_ins(csound_score_ins)

local csound_orchestra = csound_orchestra1 

local csound_score = csound_score_function1 .. "\n" .. csound_score_ins

local orchestra_finish = create_finish_orchestra(tempo_number)

local csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
local csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)

----------------------------------
-- Lilypond
tmp_algorithm = header.algorithm
header.algorithm = header.algorithm .. " " .. full_text
header.algorithm = string.gsub(header.algorithm, "_", " ")

if command.generate_lilypond == "yes" then 
   require ("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")

   local pme = "Platonic Music Engine"
   local original_composer = musical_mesostic_seed[motif].composer
   
   local algorithm = header.algorithm .. " " .. format .. "" ; local original_title = "spine = "..motif ; local additional_info = format  
   
   local time = "4/4" ; local voices = 1 ; local lyrics = {} ; local use_ragged_right = "yes" ; local no_indent = "yes"
   local rest_or_space = "r" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"
      
   local header = lilypond2header(pme, original_composer, original_title, additional_info)
   header = header .. "\nred_note = { \\once \\override NoteHead.color = #red \\once \\override Stem.color = #red }"
   
   local lily_note_table_1,lily_note_table_3,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions) --, #frequency_table)

   local lily_note_table_1,lily_note_table_3 = lilypond_mesostic_formatting(colorize, format, lily_note_table_1, lily_note_table_3, red_spot, lilypond_spot)

   local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_3)
   
   local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(complete_lilypond_score)

end -- Produce score
header.algorithm = tmp_algorithm

--------------------------------------------
--------------------------------------------
-- Mesostics for Thoreau
--------------------------------------------
--------------------------------------------
full_text = "my_life_thoreau" --"raven_poe" -- "test" -- "common" -- "all" --"walden_thoreau"
spine = "diary"

number_of_poems = 3
command.number_of_notes = number_of_poems

rule = "50%" -- "0%" "50%" or "100%"
word_rule = "linear" -- "random" "linear"
wing_rule = "surrounding" -- "50/50", "surrounding", "both", "neither"
punctuation_rule = "yes" -- "yes" "no"
bold = "yes" -- "yes" "no"
style = "merce" -- "merce", "cantos", "regular"
command.generate_text_file = "no" -- "yes" "no"
command.generate_lyric_file = "no" -- "yes" "no"
command.generate_latex_file = "yes" -- "yes" "no"
command.generate_log = "yes" -- "yes" or "no"
command.generate_lilypond = "yes"
----------------------------------------
-- Generate Mesostics
----------------------------------------
command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()
require("dir_mesostic/text_mesostics")

text_table, title, author = import_text(full_text)

mesostic_table, mesostic_number_table, number_of_characters, cleaned_spine = find_mesostics(text_table, spine, rule, wing_rule, word_rule, punctuation_rule)

formatted_mesostic_table = format_mesostics(mesostic_table, number_of_characters, cleaned_spine, style)

title = string.gsub(title, " ", "_")
author = string.gsub(author, " ", "_")

mesostic_name = "Mesostic_for_" .. title .. "_by_" .. author

printable_text = generate_text(mesostic_name, formatted_mesostic_table)
lyric_text = generate_lyrics(mesostic_name, mesostic_table)
combined_lyrics = combined_lyrics .. lyric_text
generate_latex(mesostic_table, lyric_text, mesostic_name, formatted_mesostic_table, title, author, spine, cleaned_spine, bold, style)
lyrics = shallowcopy(mesostic_table)
--------------------------------------------
--------------------------------------------
--------------------------------------------
-- Simple Melody
--------------------------------------------
--------------------------------------------
-- header.algorithm = "Simple Melody"
-- header.style = "" 
-- header.style_algorithm_author = "David Bellows"

command.command = "" -- full interactive mode
command.generate_log = "yes" -- "yes" or "no"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" 
_, number_of_words = string.gsub(lyric_text, "%S+", "")
command.number_of_notes = number_of_words 
command.instrument = "Connor Gibbs Voice"
command.tempo_word = "freely"
command.system = "western iso" 
command.scale = "fs-major pentatonic"
command.scale_recipe = "all" --"M2:4,M3:4,M6:4,M7:4"
command.scale_fill = 1
command.octave_recipe = "all"
command.dynamics_recipe = "ff:3,mf:1"
command.duration_recipe = "8th:2,quarter:1,16th:2" 
command.tuning_name = "c-Western Standard"

------------------------------------------
-- Generate Simple Melody
------------------------------------------

csound_instrument = "MIDI" ; sound_font = "fluid.sf2"
pitch_table = {} ; frequency_table = {} ; volume_table = {} ; amplitude_table = {}
duration_table = {} ; pan_table = {} ; csound_frequency_table = {} ; start_table = {}
ins_name_table = {} ; ins_num_table = {} ; midi_ins_num_table = {} 
midi_ins_number = 0

header.reference_pitch,header.reference_pitch_name = reference_pitch()

tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

scale_octave = get_scale_octave() 
midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

base_table = {}
base_table = preparse_velocities()
volume_table = quantize("volume_table", base_table)

base_table = {}
base_table = preparse_durations()
duration_table = quantize("duration_table", base_table)

-- Bel Canto style algorithm (smooths out the melody to use leaps less than a perfect fifth (if possible))
normalize_note_to_middle = "yes"
interval_adjustment = "P8"
pitch_table = belcanto(pitch_table, calculated_scale_degrees, command.number_of_notes, normalize_note_to_middle, interval_adjustment)

a4_calibration = calibrate_to_a4()
for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration 
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end
start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()
csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/" .. sound_font .. "\" \nsfpassign   0, isf\n "
end
-- End Csound preamble
start_of_table = 1
ins_num_table[1] = 1 
-- create specific Orchestra instrument
orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num) 
csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)
csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)
csound_score_ins = finish_csound_score_ins(csound_score_ins)
csound_orchestra = csound_orchestra1 
csound_score = csound_score_function1 .. "\n" .. csound_score_ins 
orchestra_finish = create_finish_orchestra(tempo_number)
csound_file = csound_preamble .. csound_orchestra .. orchestra_finish .. csound_score
csound_file_name = create_csound_file(csound_file)
create_csound_audio_file(csound_file_name, ogg_tag)

-- Simple Melody sheet music 
local tmp_algorithm = header.algorithm
header.algorithm = header.algorithm .. " " .. full_text
header.algorithm = string.gsub(header.algorithm, "_", " ")

require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
pme = "Platonic Music Engine" ; original_composer = ""
original_title = "" ; voices = 1 

time = "4/4" ; no_indent = "no"
rest_or_space = "r" ; use_ragged_right = "no" ; avoid_collisions = "no"

header.note_scheme = "simple"
-- header.note_scheme = "complex"

lilypond_header = lilypond2header(pme, original_composer, original_title, additional_info)

lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_2)

footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)

complete_lilypond_score = lilypond_header..lilypond_score..footer

generate_lilypond_pdf(complete_lilypond_score)
header.algorithm = tmp_algorithm
------------------------------------------
------------------------------------------
-- Musical Mesostics
------------------------------------------
------------------------------------------

require("dir_musical_mesostic/mesostics") ; require("dir_musical_mesostic/musical_mesostic_tables")
command.number_of_notes = 300
repeat_motif = "no"
colorize = "yes"
format = "standard" -- acrostic, mesostic, standard
simple_pitch_table = shallowcopy(pitch_table)
simple_calculated_scale_degrees = shallowcopy(calculated_scale_degrees)

command.instrument = "Piano"
command.tempo_word = "andante"
command.system = "western iso"
-- command.scale = "fs-major_pentatonic" -- Shouldn't need this
command.scale_recipe = "all"
command.scale_fill = 1
command.octave_recipe = "C4:1" 
command.dynamics_recipe = "pppp"
command.duration_recipe = "16th"
command.tuning_name = "c-Western Standard"

-----------------------------------------

csound_instrument = "MIDI" ; sound_font = "fluid.sf2"  

frequency_table = {} ; volume_table = {} ; amplitude_table = {}
duration_table = {} ; pan_table = {} ; csound_frequency_table = {} ; start_table = {}
ins_name_table = {} ; ins_num_table = {} ; midi_ins_num_table = {}

midi_ins_number = 0

--header.user_data, header.dedication = get_input()

--header.reference_pitch, header.reference_pitch_name = reference_pitch()

tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

scale_octave = get_scale_octave() 
midi_ins_number, header.lowrange, header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

base_table = {}
base_table = preparse_velocities(dynamics_recipe)
volume_table = quantize("volume_table", base_table)

base_table = {}
base_table = preparse_durations(duration_recipe)
duration_table = quantize("duration_table", base_table)

----------------------------------------------

-- Music mesostic function

musical_mesostic_seed = {}
motif_melody = find_intervals_from_pitches(simple_pitch_table, audio_freq_table, simple_calculated_scale_degrees)
musical_mesostic_seed = {
   ["Simple Melody"] = {motif = motif_melody, composer = "David Bellows"}
 }

-- musical_mesostic_seed["Simple Melody"].motif = motif_melody
-- musical_mesostic_seed["Simple Melody"].composer = "David Bellows"

motif = "Simple Melody"

volume_table, lilypond_spot, command.number_of_notes, red_spot = musical_mesostic(pitch_table, volume_table, motif, repeat_motif, format, calculated_scale_degrees)

write_log("musical_mesostic.lua","mesostic volume_table",volume_table)

---------------------------------------------
a4_calibration = calibrate_to_a4()

for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
end	     

start_of_table = 1
ins_num_table[1] = 1

---------------------------------------
-- create specific Orchestra instrument
orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument

csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num)
csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)

csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

csound_score_ins = finish_csound_score_ins(csound_score_ins)

csound_orchestra = csound_orchestra1 

csound_score = csound_score_function1 .. "\n" .. csound_score_ins

orchestra_finish = create_finish_orchestra(tempo_number)

csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)

----------------------------------
-- Lilypond
local tmp_algorithm = header.algorithm
header.algorithm = header.algorithm .. " " .. full_text
header.algorithm = string.gsub(header.algorithm, "_", " ")

if command.generate_lilypond == "yes" then 
   require ("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")

   local pme = "Platonic Music Engine"
   local original_composer = musical_mesostic_seed[motif].composer
   
   local algorithm = header.algorithm .. " " .. format .. "" ; local original_title = "spine = "..motif ; local additional_info = format  
   
   local time = "4/4" ; local voices = 1 ; local lyrics = {} ; local use_ragged_right = "yes" ; local no_indent = "yes"
   local rest_or_space = "r" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"
      
   local header = lilypond2header(pme, original_composer, original_title, additional_info)
   header = header .. "\nred_note = { \\once \\override NoteHead.color = #red \\once \\override Stem.color = #red }"
   
   local lily_note_table_1,lily_note_table_3,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions) --, #frequency_table)

   local lily_note_table_1,lily_note_table_3 = lilypond_mesostic_formatting(colorize, format, lily_note_table_1, lily_note_table_3, red_spot, lilypond_spot)

   local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_3)
   
   local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(complete_lilypond_score)

end -- Produce score
header.algorithm = tmp_algorithm

--------------------------------------------
--------------------------------------------
-- Mesostics for Sappho and Thoreau combined
--------------------------------------------
--------------------------------------------
full_text = "Title: [Fragment] and My Life\n" .. "Author: Sappho and Thoreau\n\n" .. combined_lyrics --"raven_poe" -- "test" -- "common" -- "all" --"walden_thoreau"
spine = "two"

number_of_poems = 2
command.number_of_notes = number_of_poems

rule = "50%" -- "0%" "50%" or "100%"
word_rule = "random" -- "random" "linear"
wing_rule = "neither" -- "50/50", "surrounding", "both", "neither"
punctuation_rule = "yes" -- "yes" "no"
bold = "yes" -- "yes" "no"
style = "merce" -- "merce", "cantos", "regular"
command.generate_text_file = "no" -- "yes" "no"
command.generate_lyric_file = "no" -- "yes" "no"
command.generate_latex_file = "yes" -- "yes" "no"
command.generate_log = "yes" -- "yes" or "no"
command.generate_lilypond = "yes"
----------------------------------------
-- Generate Mesostics
----------------------------------------
command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()
require("dir_mesostic/text_mesostics")


text_table, title, author = import_text(full_text)

mesostic_table, mesostic_number_table, number_of_characters, cleaned_spine = find_mesostics(text_table, spine, rule, wing_rule, word_rule, punctuation_rule)

formatted_mesostic_table = format_mesostics(mesostic_table, number_of_characters, cleaned_spine, style)

title = string.gsub(title, " ", "_")
author = string.gsub(author, " ", "_")

mesostic_name = "Mesostic_for_" .. title .. "_by_" .. author

printable_text = generate_text(mesostic_name, formatted_mesostic_table)
lyric_text = generate_lyrics(mesostic_name, mesostic_table)
-- combined_lyrics = combined_lyrics .. " " .. lyric_text
generate_latex(mesostic_table, lyric_text, mesostic_name, formatted_mesostic_table, title, author, spine, cleaned_spine, bold, style)
lyrics = shallowcopy(mesostic_table)
--------------------------------------------
--------------------------------------------
--------------------------------------------
-- Simple Melody
--------------------------------------------
--------------------------------------------

command.command = "" -- full interactive mode
-- command.generate_log = "no" -- "yes" or "no"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" 
_, number_of_words = string.gsub(lyric_text, "%S+", "")
command.number_of_notes = number_of_words 
command.instrument = "Connor Gibbs Voice"
command.tempo_word = "freely"
command.system = "western iso" 
command.scale = "c-chromatic"
command.scale_recipe = "all"
command.scale_fill = 0
command.octave_recipe = "all"
command.dynamics_recipe = "mp:2,pp:1"
command.duration_recipe = "whole:1,half:2,quarter:1,dotted half:2" 
command.tuning_name = "c-Western Standard"

------------------------------------------
-- Generate Simple Melody
------------------------------------------

csound_instrument = "MIDI" ; sound_font = "fluid.sf2"
pitch_table = {} ; frequency_table = {} ; volume_table = {} ; amplitude_table = {}
duration_table = {} ; pan_table = {} ; csound_frequency_table = {} ; start_table = {}
ins_name_table = {} ; ins_num_table = {} ; midi_ins_num_table = {} 
midi_ins_number = 0

header.reference_pitch,header.reference_pitch_name = reference_pitch()

tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

scale_octave = get_scale_octave() 
midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

base_table = {}
base_table = preparse_velocities()
volume_table = quantize("volume_table", base_table)

base_table = {}
base_table = preparse_durations()
duration_table = quantize("duration_table", base_table)

-- Bel Canto style algorithm (smooths out the melody to use leaps less than a perfect fifth (if possible))
normalize_note_to_middle = "yes"
interval_adjustment = "P8"
pitch_table = belcanto(pitch_table, calculated_scale_degrees, command.number_of_notes, normalize_note_to_middle, interval_adjustment)

a4_calibration = calibrate_to_a4()
for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration 
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end
start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()
csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/" .. sound_font .. "\" \nsfpassign   0, isf\n "
end
-- End Csound preamble
start_of_table = 1
ins_num_table[1] = 1 
-- create specific Orchestra instrument
orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num) 
csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)
csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)
csound_score_ins = finish_csound_score_ins(csound_score_ins)
csound_orchestra = csound_orchestra1 
csound_score = csound_score_function1 .. "\n" .. csound_score_ins 
orchestra_finish = create_finish_orchestra(tempo_number)
csound_file = csound_preamble .. csound_orchestra .. orchestra_finish .. csound_score
csound_file_name = create_csound_file(csound_file)
create_csound_audio_file(csound_file_name, ogg_tag)

-- Simple Melody sheet music 
local tmp_algorithm = header.algorithm
header.algorithm = header.algorithm .. " " .. "Sappho and Thoreau"
header.algorithm = string.gsub(header.algorithm, "_", " ")

require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
pme = "Platonic Music Engine" ; original_composer = ""
original_title = "" ; voices = 1 

time = "4/4" ; no_indent = "no"
rest_or_space = "r" ; use_ragged_right = "no" ; avoid_collisions = "no"

header.note_scheme = "simple"
-- header.note_scheme = "complex"

lilypond_header = lilypond2header(pme, original_composer, original_title, additional_info)

lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_2)

footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)

complete_lilypond_score = lilypond_header..lilypond_score..footer

generate_lilypond_pdf(complete_lilypond_score)
header.algorithm = tmp_algorithm

------------------------------------------
------------------------------------------
-- Musical Mesostics
------------------------------------------
------------------------------------------

require("dir_musical_mesostic/mesostics") ; require("dir_musical_mesostic/musical_mesostic_tables")
command.number_of_notes = 300
repeat_motif = "no"
colorize = "yes"
format = "standard" -- acrostic, mesostic, standard
simple_pitch_table = shallowcopy(pitch_table)
simple_calculated_scale_degrees = shallowcopy(calculated_scale_degrees)

command.instrument = "Piano"
command.tempo_word = "andante"
command.system = "western iso"
-- command.scale = "fs-major_pentatonic" -- Shouldn't need this
command.scale_recipe = "all"
command.scale_fill = 1
command.octave_recipe = "C4:1" 
command.dynamics_recipe = "pppp"
command.duration_recipe = "16th"
command.tuning_name = "c-Western Standard"
header.algorithm = header.algorithm .. " combined"
-----------------------------------------

csound_instrument = "MIDI" ; sound_font = "fluid.sf2"  

frequency_table = {} ; volume_table = {} ; amplitude_table = {}
duration_table = {} ; pan_table = {} ; csound_frequency_table = {} ; start_table = {}
ins_name_table = {} ; ins_num_table = {} ; midi_ins_num_table = {}

midi_ins_number = 0

--header.user_data, header.dedication = get_input()

--header.reference_pitch, header.reference_pitch_name = reference_pitch()

tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

scale_octave = get_scale_octave() 
midi_ins_number, header.lowrange, header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

base_table = {}
base_table = preparse_velocities(dynamics_recipe)
volume_table = quantize("volume_table", base_table)

base_table = {}
base_table = preparse_durations(duration_recipe)
duration_table = quantize("duration_table", base_table)

----------------------------------------------

-- Music mesostic function

musical_mesostic_seed = {}
motif_melody = find_intervals_from_pitches(simple_pitch_table, audio_freq_table, simple_calculated_scale_degrees)
musical_mesostic_seed = {
   ["Simple Melody"] = {motif = motif_melody, composer = "David Bellows"}
 }

-- musical_mesostic_seed["Simple Melody"].motif = motif_melody
-- musical_mesostic_seed["Simple Melody"].composer = "David Bellows"

motif = "Simple Melody"

volume_table, lilypond_spot, command.number_of_notes, red_spot = musical_mesostic(pitch_table, volume_table, motif, repeat_motif, format, calculated_scale_degrees)

write_log("musical_mesostic.lua","mesostic volume_table",volume_table)

---------------------------------------------
a4_calibration = calibrate_to_a4()

for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
end	     

start_of_table = 1
ins_num_table[1] = 1

---------------------------------------
-- create specific Orchestra instrument
orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument

csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num)
csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)

csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

csound_score_ins = finish_csound_score_ins(csound_score_ins)

csound_orchestra = csound_orchestra1 

csound_score = csound_score_function1 .. "\n" .. csound_score_ins

orchestra_finish = create_finish_orchestra(tempo_number)

csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)

----------------------------------
-- Lilypond
local tmp_algorithm = header.algorithm
header.algorithm = header.algorithm .. " " .. "Sappho and Thoreau"
header.algorithm = string.gsub(header.algorithm, "_", " ")

if command.generate_lilypond == "yes" then 
   require ("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")

   local pme = "Platonic Music Engine"
   local original_composer = musical_mesostic_seed[motif].composer
   
   local algorithm = header.algorithm .. " " .. format .. "" ; local original_title = "spine = "..motif ; local additional_info = format  
   
   local time = "4/4" ; local voices = 1 ; local lyrics = {} ; local use_ragged_right = "yes" ; local no_indent = "yes"
   local rest_or_space = "r" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"
      
   local header = lilypond2header(pme, original_composer, original_title, additional_info)
   header = header .. "\nred_note = { \\once \\override NoteHead.color = #red \\once \\override Stem.color = #red }"
   
   local lily_note_table_1,lily_note_table_3,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions) --, #frequency_table)

   local lily_note_table_1,lily_note_table_3 = lilypond_mesostic_formatting(colorize, format, lily_note_table_1, lily_note_table_3, red_spot, lilypond_spot)

   local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_3)
   
   local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(complete_lilypond_score)

end -- Produce score
header.algorithm = tmp_algorithm
