race_modifier_table = {}
race_modifier_table = {
   ["strength"] = 1,
   ["intelligence"] = 0,
   ["wisdom"] = 0,
   ["dexterity"] = 1,
   ["constitution"] = 0,
   ["charisma"] = 2,
}
