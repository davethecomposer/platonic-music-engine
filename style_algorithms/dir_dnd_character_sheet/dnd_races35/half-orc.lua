race_modifier_table = {}
race_modifier_table = {
   ["strength"] = 2,
   ["intelligence"] = -2,
   ["wisdom"] = 0,
   ["dexterity"] = 0,
   ["constitution"] = 0,
   ["charisma"] = -2,
}
