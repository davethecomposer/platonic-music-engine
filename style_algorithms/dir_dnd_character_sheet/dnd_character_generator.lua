-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Data used in this program and its supporting files is licensed under the
-- Open Gaming License from Wizards of the Coast. See the full license in the file:
-- dnd_open_gaming_license.txt

function generate_system(system)
   local result 
   if system == "" then
      local random_system = table.unpack(platonic_generator("random_system", 1, 2, 1, "yes"))
      if random_system == 1 then
	 result = "d20 3.5"
      elseif random_system == 2 then
	 result = "5e SRD"
      end
   else
      result = system
   end 
   return result
end

function generate_class(class)
   if class == "" then
      if dnd.dnd_system == "d20 3.5" then
	 random_class = platonic_generator("random_class", 1, #e3_class_table, 1, "yes")
	 result = e3_class_table[random_class[1]]
      else
	 random_class = platonic_generator("random_class", 1, #e5_class_table, 1, "yes")
	 result = e5_class_table[random_class[1]]
      end
   else
      result = class
   end

   return result
end

function generate_race(race)
   if race == "" then
      if dnd.dnd_system == "d20 3.5" then
	 random_race = platonic_generator("random_race", 1, #e3_race_table, 1, "yes")
	 result = e3_race_table[random_race[1]]
      else
	 random_race = platonic_generator("random_race", 1, #e5_race_table, 1, "yes")
	 result = e5_race_table[random_race[1]]
      end
   else
      result = race
   end

   return result
end

function generate_name(name)
   if name == "" then
      name_table = {}
      local consonants = {"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z", "'", "ch", "sh", "th", "tch", "br", "gr", "st", "dr", "dd", "ff", "ll", "bb"}
      local vowels = {"a", "e", "i", "o", "u", "y", "ee", "ea", "ie", "oo"}
      local letters = {"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z", "a", "e", "i", "o", "y",  "'", "ch", "sh", "th", "tch", "br", "gr", "st", "dr"}
      random_number_of_letters = table.unpack(platonic_generator("random_number_of_letters", 2, 7, 1, "yes"))

      local tmp = table.unpack(platonic_generator("letter", 1, #letters, 1, "yes"))
      name_table[1] = letters[tmp]

      tmp = table.unpack(platonic_generator("letter", 1, #consonants, 1, "yes"))
      consonant_table = platonic_generator("consonant", 1, #consonants, random_number_of_letters, "yes")

      tmp = table.unpack(platonic_generator("letter", 1, #vowels, 1, "yes"))
      vowel_table = platonic_generator("consonant", 1, #vowels, random_number_of_letters, "yes")

      for counter = 2, random_number_of_letters do
	 local is_vowel = "no"
	 for subcounter = 1, #vowels do
	    if name_table[counter -1] == vowels[subcounter] then 
	       is_vowel = "yes"
	    end
	 end
	 
	 if is_vowel == "yes" then 
	    local tmp = consonants[consonant_table[counter]]
	    name_table[counter] = tmp
	 else
	    local tmp = vowels[vowel_table[counter]] 
	    name_table[counter] = tmp
	 end
      end

      result = table.concat(name_table)
      local first_letter = string.sub(result, 1, 1)
      local upper_letter = string.upper(first_letter)
      result = string.gsub(result, first_letter, upper_letter, 1)
      
   else
      result = name
   end

   return result
end

function generate_level(level)
   if level == "" then
      random_level = platonic_generator("random_level", 1, 20, 1, "yes")
      result = random_level[1]
   else
      result = level
   end

   return result
end

function generate_modifier(value)
   result = math.floor((value -10) /2)
   return result
end

function generate_stats()
   local race = dnd.race
   local constitution_bonus
   local start
   local bonus
   local hit_dice
   local hit_dice_table = {}
   
   if dnd.dnd_system == "d20 3.5" then
      require("dir_dnd_character_sheet/dnd_races35/" .. race)
--      hit_dice_table = shallowcopy(e3_hit_dice_table)
   elseif dnd.dnd_system == "5e SRD" then
      require("dir_dnd_character_sheet/dnd_races51/" .. race)
--      hit_dice_table = shallowcopy(e5_hit_dice_table)
   end

   local hit_points = 0

   if dnd.dnd_system == "d20 3.5" then
      hit_dice = e3_hit_dice_table[dnd.class]
   else
      hit_dice = e5_hit_dice_table[dnd.class]
   end
   
   for counter = 1, #stats_table do
      dnd[stats_table[counter]] = 0
      local generated_table = platonic_generator(stats_table[counter], 1, 6, 4, "yes")  
      table.sort(generated_table)
      write_log("dnd_character_generator", "stats rolls, \n" .. stats_table[counter] .. " = ", generated_table)
      for subcounter = 2, 4 do
	 dnd[stats_table[counter]] = dnd[stats_table[counter]] + generated_table[subcounter] 
      end
      dnd[stats_table[counter]] = dnd[stats_table[counter]] + race_modifier_table[stats_table[counter]]
   end
   
   local constitution = dnd.constitution
   local constitution_bonus = generate_modifier(constitution) 

   hit_dice_table = platonic_generator("hit_dice_table", 1, hit_dice, dnd.level, "yes")
   write_log("dnd_character_generator", "hit dice", hit_dice)
   
   if dnd.dnd_system == "d20 3.5" then
      start = 1
   elseif dnd.dnd_system == "5e SRD" then
      start = 2
      hit_points = hit_dice + constitution_bonus
      write_log("dnd_character_generator", "hit points", hit_dice, " + " .. constitution_bonus .. " = " .. hit_points)
   end
   
   for counter = start, dnd.level do
      local tmp = hit_dice_table[counter]
      local new_hit_points = constitution_bonus + tmp
      if new_hit_points < 1 then new_hit_points = 1 end
      hit_points = hit_points + new_hit_points
      write_log("dnd_character_generator", "hit points", tmp, " + " .. constitution_bonus .. " = " .. new_hit_points)
   end 
   dnd.hit_points = hit_points

   local dexterity = dnd.dexterity
   local ac_bonus = generate_modifier(dexterity)
   dnd.armor_class = 10 + ac_bonus

   if dnd.dnd_system == "d20 3.5" then 
      if dnd.class == "paladin" then
	 dnd.alignment = paladin_alignment_table[1]
      elseif dnd.class == "barbarian" or dnd.class == "bard" then 
	 local alignment = table.unpack(platonic_generator("alignment", 1, #barbarian_alignment_table, 1, "yes"))
	 dnd.alignment = barbarian_alignment_table[alignment]
      elseif dnd.class == "monk" then
	 local alignment = table.unpack(platonic_generator("alignment", 1, #monk_alignment_table, 1, "yes"))
	 dnd.alignment = monk_alignment_table[alignment]
      elseif dnd.class == "druid" then
	 local alignment = table.unpack(platonic_generator("alignment", 1, #druid_alignment_table, 1, "yes"))
	 dnd.alignment = druid_alignment_table[alignment]
      else
	 local alignment = table.unpack(platonic_generator("alignment", 1, #alignment_table, 1, "yes"))
	 dnd.alignment = alignment_table[alignment]
      end
   else
      local alignment = table.unpack(platonic_generator("alignment", 1, #alignment_table, 1, "yes"))
      dnd.alignment = alignment_table[alignment]
   end
      
   if dnd.class == "fighter" or dnd.class == "paladin" or dnd.class == "barbarian" then
      if race == "human" or race == "half-elf" or race == "tiefling" or race == "dragonborn" then image_table = shallowcopy(human_fighter_image_table)
      elseif race == "dwarf" or race == "half-orc" then image_table = shallowcopy(dwarf_fighter_image_table)
      elseif race == "elf" then image_table = shallowcopy(elf_fighter_image_table)
      elseif race == "halfling" or race == "gnome" then image_table = shallowcopy(halfling_fighter_image_table)
      end
   elseif dnd.class == "ranger" then
      image_table = shallowcopy(ranger_image_table)
   elseif dnd.class == "monk" then
      image_table = shallowcopy(monk_image_table)
   elseif dnd.class == "cleric" then
      image_table = shallowcopy(cleric_image_table)
   elseif dnd.class == "druid" then
      image_table = shallowcopy(druid_image_table)
   elseif dnd.class == "wizard" or dnd.class == "sorcerer" or dnd.class == "warlock" then
      image_table = shallowcopy(wizard_image_table)
   elseif dnd.class == "rogue" or dnd.class == "bard" then
      image_table = shallowcopy(rogue_image_table)
   end
   
   local image = table.unpack(platonic_generator("image", 1, #image_table, 1, "yes"))
   dnd.image = image_table[image]

   if dnd.dnd_system == "d20 3.5" then
      dnd.bonus_word = "base attack bonus"
      if dnd.class == "fighter" or dnd.class == "barbarian" or dnd.class == "ranger" or dnd.class == "paladin" then
	 bonus = dnd.level
      elseif dnd.class == "sorcerer" or dnd.class == "wizard" then
	 bonus = math.floor(dnd.level /2)
      else
	 bonus = proficiency_table[dnd.level]
      end
      dnd.proficiency = "+" .. bonus
   else
      dnd.bonus_word = "proficiency bonus"
      dnd.proficiency = "+" .. (1 + math.ceil(dnd.level /4))
   end
   
   return
end

function generate_character_sheet_latex()
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Text"
      local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
      local font = "newcent" ; local poet = dnd.dnd_system ; local algorithm = header.algorithm
      local meter = ""
      local piece = "piece" 
      local instrument = ""
      local symbol = ""
      local amount
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. latex_title

      local body = "\n\n\\begin{center}\n\\Large{" .. dnd.name .. "}\\\\\n\\small{" .. dnd.race .. " " .. dnd.class .. ", level " .. dnd.level .. "}\n\\end{center}"

      body = body .. "\n\n\\begin{tabular}\n{ p{.225\\textwidth} l c c p{.05\\textwidth} l l}"

      amount = generate_modifier(dnd.strength)
      if amount > 0 then symbol = "+" else symbol = "" end
      if amount == 0 then amount = "" end
      body = body .. "\n& \\textit{strength} & " .. dnd.strength .. " &  \\textbf{" .. symbol .. amount .. "} && \\\\"

      amount = generate_modifier(dnd.intelligence)
      if amount > 0 then symbol = "+" else symbol = "" end
      if amount == 0 then amount = "" end
      body = body .. "\n& \\textit{intelligence} & " .. dnd.intelligence .. " & \\textbf{" .. symbol .. amount .. "} && \\textit{armor class} & " .. dnd.armor_class .. " \\\\"

      amount = generate_modifier(dnd.wisdom)
      if amount > 0 then symbol = "+" else symbol = "" end
      if amount == 0 then amount = "" end
      body = body .. "\n& \\textit{wisdom} & " .. dnd.wisdom .. " & \\textbf{" .. symbol .. amount .. "} && \\textit{hit points} & " .. dnd.hit_points .. " \\\\"

      amount = generate_modifier(dnd.dexterity)
      if amount > 0 then symbol = "+" else symbol = "" end
      if amount == 0 then amount = "" end
      body = body .. "\n& \\textit{dexterity} & " .. dnd.dexterity .. " & \\textbf{" .. symbol .. amount .. "} && \\textit{alignment} & " .. dnd.alignment .. " \\\\"

      amount = generate_modifier(dnd.constitution)
      if amount > 0 then symbol = "+" else symbol = "" end
      if amount == 0 then amount = "" end  
      body = body .. "\n& \\textit{constitution} & " .. dnd.constitution .. " & \\textbf{" .. symbol .. amount .. "} && \\textit{" .. dnd.bonus_word .. "} & " .. dnd.proficiency .. " \\\\"

      amount = generate_modifier(dnd.charisma)
      if amount > 0 then symbol = "+" else symbol = "" end
      if amount == 0 then amount = "" end  
      body = body .. "\n& \\textit{charisma} & " .. dnd.charisma .. " & \\textbf{" .. symbol .. amount .. "} && \\\\"

      body = body .. "\n\\end{tabular}"
      
      body = body .. "\n{\\center{\\includegraphics[width=.5\\textwidth]{dir_dnd_character_sheet/dndart/" .. dnd.image .. "}}\n\n"
      
      local score = score .. body

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\n  {\\small This work features art by Daniel F. Walthall, originals found at: \\href{drivethrurpg.com/product/181517}{drivethrurpg.com}, available under a CC BY 4.0 license: \\href{https://www.creativecommons.org/licenses/by/4.0/}{creativecommons.org}" .. "\n\\end{document}"
      
      create_latex(score, "DnD_Character_Sheet")
      write_log("dnd_character_generator", "dnd", dnd)   
   end
   return
end
