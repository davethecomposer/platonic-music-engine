-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Data used in this program and its supporting files is licensed under the
-- Open Gaming License from Wizards of the Coast. See the full license in the file:
-- dnd_open_gaming_license.txt

e3_class_table = {}
e3_class_table = {"fighter", "barbarian", "ranger", "monk", "paladin", "cleric", "druid", "rogue", "wizard", "sorcerer", "bard"}

e5_class_table = {}
e5_class_table = {"fighter", "barbarian", "ranger", "monk", "paladin", "cleric", "druid", "rogue", "wizard", "sorcerer", "warlock", "bard"}

e3_race_table = {}
e3_race_table = {"human", "elf", "dwarf", "halfling", "gnome", "half-elf", "half-orc"}

e5_race_table = {}
e5_race_table = {"human", "elf", "dwarf", "halfling", "gnome", "half-elf", "half-orc", "dragonborn", "tiefling"}

stats_table = {}
stats_table = {"strength", "intelligence", "wisdom", "dexterity", "constitution", "charisma"}

e3_hit_dice_table = {}
e3_hit_dice_table = {
   ["fighter"] = 10,
   ["barbarian"] = 12,
   ["paladin"] = 10,
   ["ranger"] = 8,
   ["monk"] = 8,
   ["cleric"] = 8,
   ["druid"] = 8,
   ["wizard"] = 4,
   ["sorcerer"] = 4,
   ["rogue"] = 6,
   ["bard"] = 6,
}

e5_hit_dice_table = {}
e5_hit_dice_table = {
   ["fighter"] = 10,
   ["barbarian"] = 12,
   ["paladin"] = 10,
   ["ranger"] = 10,
   ["monk"] = 8,
   ["cleric"] = 8,
   ["druid"] = 8,
   ["wizard"] = 6,
   ["sorcerer"] = 6,
   ["warlock"] = 8,
   ["rogue"] = 8,
   ["bard"] = 8,
}

alignment_table = {}
alignment_table = {"lawful good", "lawful neutral", "lawful evil", "neutral good", "neutral", "neutral evil", "chaotic good", "chaotic neutral", "chaotic evil"}

paladin_alignment_table = {}
paladin_alignment_table = {"lawful good"}

monk_alignment_table = {}
monk_alignment_table = {"lawful good", "lawful neutral", "lawful evil"}

druid_alignment_table = {}
druid_alignment_table = {"neutral good", "lawful neutral", "neutral", "chaotic neutral", "neutral evil"}

barbarian_alignment_table = {}
barbarian_alignment_table = {"neutral good", "neutral", "neutral evil", "chaotic good", "chaotic neutral", "chaotic evil"}

human_fighter_image_table = {}
human_fighter_image_table = {"2handed_sword_free4", "2hand_sword_free1", "2hand_sword_free2", "2hand_sword_free3"}

dwarf_fighter_image_table = {}
dwarf_fighter_image_table = {"2hand_axe_free1", "2hand_axe_free2", "axe_free1", "axe_free2", "axe_free3", "axe_free4"}

elf_fighter_image_table = {}
elf_fighter_image_table = {"sword_free1", "sword_free2", "sword_free3", "sword_free4", "sword_free5"}

halfling_fighter_image_table = {}
halfling_fighter_image_table = {"dagger_free1", "dagger_free2", "dagger_free3"}

ranger_image_table = {}
ranger_image_table = {"bow_free1", "bow_free2"}

monk_image_table = {}
monk_image_table = {"shuriken_free1", "shuriken_free2", "shuriken_free3"}

cleric_image_table = {}
cleric_image_table = {"mace_free1", "mace_free2"}

druid_image_table = {}
druid_image_table = {"staff_free1", "staff_free2", "staff_free3"}

wizard_image_table = {}
wizard_image_table = {"spellbook_free1", "spellbook_free2", "spellbook_free3", "spellbook_free4", "spellbook_free5", "spellbook_free6"}

rogue_image_table = {}
rogue_image_table = {"dagger_free4", "dagger_free5", "dagger_free6"}

proficiency_table = {} -- This is only for some characters in 3e
proficiency_table = {0, 1, 2, 3, 3, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11, 12, 12, 13, 14, 15}
