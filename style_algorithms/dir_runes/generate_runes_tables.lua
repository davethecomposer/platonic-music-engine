-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

runes_list = {}
runes_list = {"ash", "wealth", "cattle", "thorn", "mouth", "ride", "torch", "gift", "joy", "hail", "need", "ice", "year", "yew", "game", "elk-sedge", "sun", "tyr", "birch", "horse", "man", "lake", "ing", "estate", "day", "oak", "bow", "earth", "serpent", "chalice", "spear", "fire", "stone"}

runes_table = {}
runes_table = {
   ["ash"] = "15,15|15,60 11,57|30,45 11,47|28,36",
   ["wealth"] = "15,15|15,60 11,30|32,45 11,45|30,59",
   ["cattle"] = "13,15|13,60 9,58|33,15",
   ["thorn"] = "13,15|13,60 9,30|32,42 9,45|32,38",
   ["mouth"] = "13,15|13,60 9,50|24,38 20,38|31,50 9,60|23,48 21,48|31,60",
   ["ride"] = "13,15|13,60 11,37|30,15 11,60|32,48 30,50|11,37",
   ["torch"] = "15,15|15,60 11,35|33,15",
   ["gift"] = "11,15|31,60 11,60|31,15",
   ["joy"] = "13,15|13,60 32,52|11,37 11,60|32,48",
   ["hail"] = "11,15|11,60 31,15|31,60 9,55|33,30 9,45|33,20",
   ["need"] = "21,15|21,60 13,40|29,34",
   ["ice"] = "21,15|21,60",
   ["year"] = "21,15|21,60 17,45|33,35 17,30|33,37 25,45|11,35 25,30|11,37",
   ["yew"] = "21,15|21,60 17,56|33,45 25,17|9,25",
   ["game"] = "13,15|13,60 9,60|23,48 21,48|31,60 9,15|23,27 21,27|31,15",
   ["elk-sedge"] = "21,15|21,60 17,40|31,60 25,40|11,60",
   ["sun"] = "13,60|13,25 31,15|31,45 9,25|35,45",
   ["tyr"] = "21,15|21,60 17,60|31,50 25,60|11,50",
   ["birch"] = "13,15|13,60 30,50|11,37 11,60|32,48 30,25|11,37 11,15|32,27",
   ["horse"] = "13,15|13,60 11,60|23,50 31,15|31,60 33,60|21,50",
   ["man"] = "13,15|13,60 31,15|31,60 11,60|35,45 33,60|9,45",
   ["lake"] = "13,15|13,60 9,60|30,45",
   ["ing"] = "11,15|31,38 31,15|11,38 11,36|31,60 29,38|11,60",
   ["estate"] = "11,15|31,50 31,15|11,50 11,48|23,60 31,48|19,60",
   ["day"] = "11,15|11,60 7,15|35,60 7,60|35,15 31,15|31,60",
   ["oak"] = "13,15|13,60 9,60|23,48 21,48|31,60 9,50|24,38",
   ["bow"] = "13,15|13,60 9,58|33,15 20,15|20,25",
   ["earth"] = "21,15|21,60 19,60|27,50 25,50|33,60 23,60|15,50 17,50|11,60",
   ["serpent"] = "21,15|21,60 11,27|32,47 32,27|11,47",
   ["chalice"] = "11,15|31,60 11,60|31,15 13,37|19,42 13,39|19,29 29,37|21,42 29,37|23,29 21,15|21,60",
   ["spear"] = "11,15|31,60 11,60|31,15 13,37|19,42 13,39|19,29 29,37|21,42 29,37|23,29",
   ["fire"] = "21,15|21,60 19,60|27,50 25,50|33,60 23,15|15,25 17,25|11,15",
   ["stone"] = "11,15|11,60 7,60|21,50 7,15|21,25 19,50|33,60 33,15|17,25 31,15|31,60",
}

stone_color_table = {}
stone_color_table = {"gray", "blue", "red", "yellow", "black", "purple", "green", "brown", "orange", "white"}

line_color_table = {}
line_color_table = {"black", "blue", "red", "yellow"}
   
