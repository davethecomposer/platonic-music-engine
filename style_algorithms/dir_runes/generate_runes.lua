-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

function generate_number()
   local number = command.number_of_notes
   if number == "" then
	 number = table.unpack(platonic_generator("method" .. command.question, 1, 20, 1, "yes")) 
   end
   command.number_of_notes = number
   write_log("generate_number", "number", number)
   return 
end

function generate_stone_color()
   local stone_color = command.stone_color
   if stone_color == "" then
      local tmp = table.unpack(platonic_generator("stone_color" .. command.question, 1, #stone_color_table, 1, "yes"))
      stone_color = stone_color_table[tmp]
   end

   if stone_color == "grey" or stone_color == "gray" then stone_color = "lightgray" end
   command.stone_color = stone_color
   write_log("generate_stone_color", "stone_color", stone_color)
   return 
end

function generate_line_color()
   local line_color = command.line_color
   if line_color == "" then
      local tmp = table.unpack(platonic_generator("line_color" .. command.question, 1, #line_color_table, 1, "yes"))
      line_color = line_color_table[tmp]
   end
   command.line_color = line_color
   write_log("generate_line_color", "line_color", line_color)
   return
end

function generate_runes_latex()
   require("dir_runes/generate_runes_tables")
   generate_number() --command.number_of_notes
   local number = command.number_of_notes
   
   local question = command.question

   generate_stone_color()
   local stone_color = command.stone_color
   white_table = platonic_generator("white_table" .. question, 10, 90, number, "yes")

   generate_line_color()
   local line_color = command.line_color

   local result = ""
   local x1coord = {}
   local y1coord = {}
   local x2coord = {}
   local y2coord = {}
   local x3coord = {}
   local y3coord = {}
   local x4coord = {}
   local y4coord = {}
   local xlcoord = {}
   local ylcoord = {}
   local xrcoord = {}
   local yrcoord = {}
   local rune_box = {}

   local off_the_table = 0
   local xsize = 42 --85
   local ysize = 78  --156
   local xshift = 7 --14
   local yshift1 = 25 --50
   local yshift2 = 40 --80
   local gap = 40 --60
   local widthneg = -7 --14
   local widthpos = 7 --14
   
   result = result .. "\n\n\\node[yshift=-120] at (current page.north){\\centering\\LARGE{}\\textit{" .. question .. "}};\n"

   xcoord = platonic_generator("xcoord" .. question, 100, 500, number, "yes")
   ycoord = platonic_generator("ycoord" .. question, 150, 550, number, "yes")
   x2shift = platonic_generator("x2shift" .. question, widthneg, widthpos, number, "yes")
   y2shift = platonic_generator("y2shift" .. question, widthneg, widthpos, number, "yes")
   x3shift = platonic_generator("x3shift" .. question, widthneg, widthpos, number, "yes")
   y3shift = platonic_generator("y3shift" .. question, widthneg, widthpos, number, "yes")
   x4shift = platonic_generator("x4shift" .. question, widthneg, widthpos, number, "yes")
   y4shift = platonic_generator("y4shift" .. question, widthneg, widthpos, number, "yes")
   xlshift = platonic_generator("xlshift" .. question, 0, xshift, number, "yes")
   ylshift = platonic_generator("ylshift" .. question, yshift1, yshift2, number, "yes")
   xrshift = platonic_generator("xrshift" .. question, 0, xshift, number, "yes")
   yrshift = platonic_generator("yrshift" .. question, yshift1, yshift2, number, "yes")

   local table_counter = 1
   for counter = 1, number do
      local x1 = xcoord[counter]
      local y1 = ycoord[counter]
      local tmp = x2shift[counter]
      local x2 = x1 + tmp
      tmp = y2shift[counter]
      local y2 = y1 + ysize + tmp
      tmp = x3shift[counter] 
      local x3 = x1 + xsize + tmp
      tmp = y3shift[counter]
      local y3 = y1 + ysize + tmp
      tmp = x4shift[counter]
      local x4 = x1 + xsize + tmp
      tmp = y4shift[counter]
      local y4 = y1 + tmp

      tmp = xlshift[counter]
      local xl = x1 - tmp
      tmp = ylshift[counter]
      local yl = y1 + tmp

      tmp = xrshift[counter]
      local xr = x4 + tmp
      tmp = yrshift[counter]
      local yr = y4 + tmp

      local overlap = "no"
      for sub = 1, #rune_box do 
	 local tmp = rune_box[sub]
	 local low, high = bifurcate(tmp, "|")
	 local X3, Y3 = bifurcate(low, "*")
	 local X4, Y4 = bifurcate(high, "*")
	 X3 = tonumber(X3) ; Y3 = tonumber(Y3) ; X4 = tonumber(X4) ; Y4 = tonumber(Y4)
	 if (x3 < X3) or (X4 < x1) or (y3 < Y3) or (Y4 < y1) then
	    overlap = "no"
	 else
	    overlap = "yes" break
	 end
      end      

      if overlap == "no" then
	 x1coord[table_counter] = x1
	 y1coord[table_counter] = y1
	 x2coord[table_counter] = x2
	 y2coord[table_counter] = y2
	 x3coord[table_counter] = x3
	 y3coord[table_counter] = y3
	 x4coord[table_counter] = x4
	 y4coord[table_counter] = y4

	 xlcoord[table_counter] = xl
	 ylcoord[table_counter] = yl
	 xrcoord[table_counter] = xr
	 yrcoord[table_counter] = yr
	 rune_box[table_counter] = x1 - gap .. "*" .. y1 - gap .. "|" .. x3 + gap .. "*" .. y3 + gap
	 
	 table_counter = table_counter + 1
      else
	 off_the_table = off_the_table +1
      end     
   end
   table_counter = table_counter -1
   write_log("generate_runes", "off_the_table", off_the_table)
   write_log("generate_runes", "rune_box", rune_box)
   
   rotate_table = platonic_generator("rotate_table" .. question, 0, 359, number, "yes")
   local runes_to_use_table = platonic_generator("runes_to_use_table" .. question, 1, #runes_list, number, "yes")
   local heads_or_tails = platonic_generator("heads_or_tails" .. question, 1, 2, number, "yes")
   for counter = 1, table_counter do
      local stone = "\n\\node[xshift=" .. x1coord[counter] .. "pt,yshift=" .. y1coord[counter] .."pt] at (current page.south west)\n"
      local stone = stone .. "{\\begin{tikzpicture}[rotate=" .. rotate_table[counter] .. "]"
      stone = stone .. "\n\\draw[line width=0cm, rounded corners=.5cm,fill=" .. stone_color .. "!" .. white_table[counter] .. "!white,drop shadow={opacity=1}](" .. x1coord[counter] .. "pt," .. y1coord[counter] .. "pt)--(" .. xlcoord[counter] .. "pt," .. ylcoord[counter] .. "pt)--(" .. x2coord[counter] .. "pt," .. y2coord[counter] .. "pt)--(" .. x3coord[counter] .. "pt," .. y3coord[counter] .. "pt)--(" ..xrcoord[counter] .. "pt," .. yrcoord[counter] .. "pt)--(" .. x4coord[counter] .. "pt," .. y4coord[counter] .. "pt)--cycle;\n"

      local tmp1 = runes_to_use_table[counter] 
      local tmp = runes_list[tmp1]
      write_log("generate_runes.lua", "runes", tmp)
      tmp = runes_table[tmp]

      if heads_or_tails[counter] == 1 then 

	 local tmp_table = string.split(tmp," ")
	 for sub = 1, #tmp_table do
	    local low, high = bifurcate(tmp_table[sub],"|") 
	    local lowx, lowy = bifurcate(low,",") 
	    local highx, highy = bifurcate(high,",") 

	    stone = stone .. "\\draw[line width=2pt,color=" .. line_color .. "](" .. x1coord[counter] + lowx .."pt," .. y1coord[counter] + lowy.. "pt)--(" .. x1coord[counter] + highx .. "pt," .. y1coord[counter] + highy .. "pt);"
	 end
      end
      stone = stone .. "\n\\end{tikzpicture}\n};\n\n"
      result = result .. stone
   end
   
   result = "\n\\begin{tikzpicture}[remember picture,overlay]\n" .. result .. "\n\\end{tikzpicture}\n"
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Divination"
      local composer = "David Bellows"
      local arranger = "Platonic Music Engine"
      local font = "newcent"
      local poet = number .. " runes cast" --""-- "\\textit{" .. method .. "} method"
      algorithm = header.algorithm
      local meter = "with " .. off_the_table .. " that fell off the page"
      local piece = "" 
      local instrument = ""
      local symbol = ""
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. "\\usetikzlibrary{shadows}\n" .. latex_title 

      local body = result 
      local score = score .. body --.. "\n}\\end{tikzpicture}"

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\n\\end{document}" 

      create_latex(score, "Runes_Cast")
      create_latex(score, "Runes_Cast")
   end
   return
end
