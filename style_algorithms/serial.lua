-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Serial"
header.style = "" 
header.style_algorithm_author = "David Bellows"

-------------------------------------------
command.user_input = "Heraclitus"

command.generate_log = "yes"

local serialize_recipe_1 = "P:P1,P:-1,I:P1,R:P1,RI:P8,P:-M7" 
local allow_repeat = "yes"
local serialize_recipe_2 = "P,R,I,RI" ; local thing_to_serialize_1 = "volume"
local serialize_recipe_3 = "" --[["P,RI,R,I"]] ; local thing_to_serialize_2 = "duration"

command.generate_lilypond = "no"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" ; local enforce_nine = "no"
command.wave = "no" ; command.flac = "no" ; command.ogg = "no" ; local ogg_tag = "yes"

command.number_of_notes = 48

command.instrument = "Piano"

command.tempo_word = "andante"

command.system = "western iso"

command.scale = "c-Panchromatic"

command.scale_recipe = "all"
command.scale_fill = 0
command.octave_recipe = "all" 

command.dynamics_recipe = "pp,mf:2,fff"

command.duration_recipe = "16th,8th:2,quarter,quarter"

command.tuning_name = "c-Western Standard"

----------------------------------------

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"

local pitch_table = {} ; local frequency_table = {} ; local volume_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {}

local midi_ins_number = 0

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()

header.reference_pitch,header.reference_pitch_name = reference_pitch()

local tempo_number = tempo_value() 

audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

calculated_scale_degrees = generate_relative_scale_degrees()

analyze_intervals()

local scale_octave = get_scale_octave(calculated_scale_degrees) 
midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table,low_freq, high_freq) 

local base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

local base_table = {}
base_table = preparse_velocities(dynamics_recipe)
volume_table = quantize("volume_table", base_table)

local base_table = {}
base_table = preparse_durations(duration_recipe)
duration_table = quantize("duration_table", base_table)

-----------------------------------------------
-- Serialism function
-----------------------------------------------
require("dir_serial/serialize") ; require("scripts/lilypond_creation_tables")
pitch_table, duration_table, volume_table, command.number_of_notes = twelve_tone(pitch_table, volume_table, duration_table, serialize_recipe_1, allow_repeat)

-- Serliaze some stuff
volume_table = serialize(volume_table, thing_to_serialize_1, serialize_recipe_2)

duration_table = serialize(duration_table, thing_to_serialize_2, serialize_recipe_3)

------------------------------------------------
local a4_calibration = calibrate_to_a4()

for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
end	     

local start_of_table = 1
ins_num_table[1] = 1

-- create specific Orchestra instrument
local orchestra_instrument = csound_instrument ; local instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
local csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
local csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)

local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

csound_score_ins = finish_csound_score_ins(csound_score_ins)

local csound_orchestra = csound_orchestra1 

local csound_score = csound_score_function1 .. csound_score_function1 .. "\n" .. csound_score_ins

local orchestra_finish = create_finish_orchestra(tempo_number)

local csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
local csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)

if command.generate_lilypond == "yes" then -- Generate Lilypond score
   require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
   local adapter = style_algorithm_author ; local pme = "Platonic Music Engine" ; local original_composer = ""
   local original_title = "" 

   local time = "4/4" ; local voices = 1 ; local lyrics = {} ; local use_ragged_right = "no" ; local no_indent = "no"
   local rest_or_space = "r" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"

   local header = lilypond2header(pme, original_composer, original_title, additional_info)

   local lily_note_table_1,lily_note_table_3,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

   local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1,lily_note_table_3)
   
   local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(complete_lilypond_score)
end

if command.generate_feldman == "yes" then -- Feldman Graphic notation
   require("scripts/feldman_notation")
   local volume = "yes" 

   local feldman_score,latexname = midi2feldman(pitch_table, duration_table, volume_table, volume, tempo_number)
   create_latex(feldman_score,latexname)
end

if command.generate_pattern_15 == "yes" then -- Pattern 15 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_15") 
   
   local midinotes = shallowcopy(platonic_future) ; local mididuration = shallowcopy(platonic_duration)

   pattern_15_lilypond_score,latexname = 
      midi2pattern_15(pitch_table, duration_table)

   create_latex(pattern_15_lilypond_score,latexname)
end

if command.generate_pattern_35 == "yes" then -- Pattern 35 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_35")

   pattern_35_score,latexname = midi2pattern_35(pitch_table, duration_table, volume_table)
   
   create_latex(pattern_35_score,latexname)
end
