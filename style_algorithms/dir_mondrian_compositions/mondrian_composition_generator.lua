-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

color_table = {}
color_table = {"white","red", "blue", "yellow", "darkgray"}

function generate_mondrian_composition_latex()
   local vertical_lines = {}
   local horizontal_lines = {}
   local used_y = {}
   local used_x = {}
   local allowable_y = {}
   local allowable_x = {}
   local box_coords = {}
   local color_box = {}
   local filled_box = {}
 
   local number_of_vertical_lines = table.unpack(platonic_generator("number_of_vertical_lines", 1, 20, 1, "yes")) 
   local number_of_horizontal_lines = table.unpack(platonic_generator("number_of_horizontal_lines", 1, 20, 1, "yes")) 
   local number_of_boxes = table.unpack(platonic_generator("number_of_boxes", 1, 20, 1, "yes"))

   write_log("mondrian_composition_generator", "number of vertical lines attempted", number_of_vertical_lines)
   write_log("mondrian_composition_generator", "number of horizontal lines attempted", number_of_horizontal_lines)
   write_log("mondrian_composition_generator", "number of boxes attempted", number_of_boxes)

   local vertical_line_spot = platonic_generator("vertical_line_spot", 1, 19, number_of_vertical_lines, "yes")
   local horizontal_line_spot = platonic_generator("horizontal_line_spot", 1, 19, number_of_horizontal_lines, "yes")
   local colors = platonic_generator("colors", 1, #color_table, number_of_boxes, "yes")

   for counter = 1, number_of_vertical_lines do
      vertical_lines[counter] = "(" .. vertical_line_spot[counter] .. ",0) -- (" .. vertical_line_spot[counter] .. ",20);"
      used_x[counter] = vertical_line_spot[counter]
   end
   table.insert(used_x, 0) ;    table.insert(used_x, 20) 
   
   for counter = 1, number_of_horizontal_lines do
      horizontal_lines[counter] = "(0," .. horizontal_line_spot[counter] .. ") -- (20," .. horizontal_line_spot[counter] .. ");"
      used_y[counter] = horizontal_line_spot[counter]
   end
   table.insert(used_y, 0) ;    table.insert(used_y, 20) 

   write_log("mondrian_composition_generator", "vertical lines used", vertical_lines)
   write_log("mondrian_composition_generator", "horizontal lines used", horizontal_lines)
   
   -- Eliminate duplicates
   local hash = {}
   for _,v in ipairs(used_y) do
      if (not hash[v]) then
	 allowable_y[#allowable_y+1] = v
	 hash[v] = true
      end
   end
   
   hash = {}
   for _,v in ipairs(used_x) do
      if (not hash[v]) then
	 allowable_x[#allowable_x+1] = v
	 hash[v] = true
      end
   end
   table.sort(allowable_y) ; table.sort(allowable_x)
   
   local box_spots_y = platonic_generator("box_spots_y", 1, #allowable_y, number_of_boxes *2, "yes")
   local box_spots_x = platonic_generator("box_spots_x", 1, #allowable_x, number_of_boxes *2, "yes")

   local subcount = 1
   local fill_counter = 1
   for counter = 1, number_of_boxes do
      local x1 = allowable_x[box_spots_x[subcount]] ; local y1 = allowable_y[box_spots_y[subcount]] 
      local x2 = allowable_x[box_spots_x[subcount +1]] ; local y2 = allowable_y[box_spots_y[subcount +1]]

      -- Make sure that x1,y1 are the lower-left coordinates
      if x2 < x1 and y2 < y1 then
	 x1, x2 = x2, x1
	 y1, y2 = y2, y1
      elseif x2 < x1 and y2 > y1 then
	 x1, x2 = x2, x1
      elseif x2> x1 and y2 < y1 then
	 y1, y2 = y2, y1
      elseif x2 > x1 and y2 > y1 then
	 -- do nothing
      end

      local overlap = "no" 
      for subcounter = 1, #filled_box  do
	 -- Get previous boxes that were accepted
	 local low, high = bifurcate(filled_box[subcounter], "|") 
	 local x3, y3 = bifurcate(low, "*") 
	 local x4, y4 = bifurcate(high, "*") 

	 x3 = tonumber(x3) ; y3 = tonumber(y3) ; x4 = tonumber(x4) ; y4 = tonumber(y4)
	 -- This is the complicated algorithm bit. Found the solution online in various places.
	 -- It checks to see if two rectangles overlap.
	 if (x2 <= x3) or (x4 <= x1) or (y2 <= y3) or (y4 <= y1) then
	    overlap = "no"
	 else
	    overlap = "yes" break
	 end
      end

      -- Don't use a rectangle if it overlaps an earlier one. Don't use a rectangle that is just a line.
      if overlap == "no" and (x1 ~= x2 and y1 ~= y2) then 
	 box_coords[fill_counter] = "(" .. x1 .. "," .. y1 .. ")rectangle(" .. x2 .. "," .. y2 .. ");"

	 filled_box[fill_counter] = x1 .. "*" .. y1 .. "|" .. x2 .. "*" .. y2
	 	 fill_counter = fill_counter + 1
      else
	 write_log("mondrian_composition_generator", "x1, y1, x2, y2 (box that overlaps with an existing box or is a line)", x1 .. "," .. y1 .. " " ..  x2 .."," .. y2)
      end subcount = subcount + 2
   end

   write_log("mondrian_composition_generator", "box_coords (These are the boxes that get used)", box_coords)
   for counter = 1, number_of_boxes do
      color_box[counter] = color_table[colors[counter]] 
   end
   write_log("mondrian_composition_generator", "color_box", color_box)
   
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Art"
      local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
      local font = "newcent" ; local poet = "Inspired by Piet Mondrian" ; local algorithm = header.algorithm
      local meter = ""
      local piece = "piece" 
      local instrument = ""
      local symbol = ""
      local amount
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. latex_title

      local body = "\n\n\\begin{tikzpicture}"
      body = body .. "\n\\tikz[line width=1mm]{"
      for counter = 1, number_of_vertical_lines do
	 body = body .. "\n\\draw " .. vertical_lines[counter]
      end

      for counter = 1, number_of_horizontal_lines do
	 body = body .. "\n\\draw " .. horizontal_lines[counter]
      end    

       for counter = 1, #box_coords do
	 body = body .. "\n\\draw[fill=" .. color_box[counter] .. "]" .. box_coords[counter]
      end

      body = body .. "\n\\draw[gray](0,0)rectangle(20,20);"
      
      local score = score .. body .. "\n}\\end{tikzpicture}"

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\n\\end{document}"

      create_latex(score, "Mondrian_Composition")

   end
   return
end
