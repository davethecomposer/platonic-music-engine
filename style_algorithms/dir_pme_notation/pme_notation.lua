-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

function generate_pme_music(row_notes)
   local rp = 0 ; local old_rp
   local pitch_table = {}
   local duration_table = {}
   local volume_table = {}
   local entry
   local duration
   local volume
   local octave

   local key,_ = bifurcate(command.scale,"-")
   local rotate_interval = distance_of_C_to_key[key]
   local rotate_degree = calculated_scale_degrees[rotate_interval]

   for counter = 1, command.number_of_notes do
      entry = row_notes[counter]

      if string.find(entry, "/") then
	 entry,volume = bifurcate(entry, "/") 
	 volume_table[counter] = velocity_dynamic_list[volume]
      else
	 if counter == 1 then
	    volume_table[counter] = "f"
	 else
	    volume_table[counter] = volume_table[counter-1] 
	 end
      end

      if string.find(entry, ":") then
	 entry, duration = bifurcate(entry, ":")
	 duration_table[counter] = durations2seconds[duration]
      else
	 if counter == 1 then
	    duration_table[counter] = 4
	 else
	    duration_table[counter] = duration_table[counter -1]
	 end
      end

      if string.find(entry, "@") then
	 octave, entry = bifurcate(entry, "@")
	 rp = c_octave_table[octave]
      else
	 if counter == 1 then
	    rp = header.middle_of_instrument 
	 else
	    rp = old_rp
	 end
      end

      local multiplier, pitch = multiplier(entry)

      pitch_table[counter] = rp + (multiplier * calculated_scale_degrees[pitch]) + rotate_degree

      old_rp = rp
   end
   return pitch_table, duration_table, volume_table
end
