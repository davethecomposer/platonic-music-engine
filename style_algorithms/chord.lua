-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Simple Melody"
header.style = "" 
header.style_algorithm_author = "David Bellows"

-- You can start editing things here. See the Tutorial or Reference Manual if something doesn't make sense.
-- Note, instead of changing these options you can indicate your choices on the command line.
-- Type: "lua simple_melody.lua -h" for a list of commands line options.

command.user_input = "hi" -- full interactive mode
-- local command.user_input = "r" -- random, non-interactive
-- local command.user_input = "Heraclitus" -- specific string, non-interactive

command.generate_log = "yes" -- "yes" or "no"

-- Change these to "yes" if you want to generate those kinds of files (sheet music, graphic notation, various audio formats).

command.generate_lilypond = "no"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" 
command.wave = "no" ; command.flac = "no" ; command.ogg = "no" ; local ogg_tag = "yes"

command.number_of_notes = 24

command.instrument = "Piano"

command.tempo_word = "andante"
-- local tempo_word = "120.5"

command.system = "western iso" -- A4 = 440Hz
-- local system = "verdi" -- A4 = 432Hz
-- local system = "432.5" -- set A4 to 432.5Hz

command.scale = "c-Panchromatic"
-- local scale = "c-major"
-- local scale = "c-chromatic"
-- local scale = "#0,0,0,2,4,5,5,7,7,9,11:12" -- c-major scale with emphasis -- CURRENTLY UNSUPPORTED; NOT SURE IF IT WILL BE!!! 9/9/16
-- local scale = "#0,2,4:5" -- Maija scale, repeats this pattern at every fifth semitone -- DOESN'T WORK! 9/9/16

-- Set emphasis for scale
command.scale_recipe = "all"
-- local scale_recipe = "P1:4,P5:2"
-- local scale_recipe = "tonic:3,dominant:2"
-- local scale_recipe = "0:3,7:2"
-- local scale_recipe = "P1:3,7:2"

command.scale_fill = 0
-- local scale_fill = 1

command.octave_recipe = "all"
-- local octave_recipe = "C3:1,C4:1"

-- Set emphasis for dynamics
command.dynamics_recipe = "all"
-- local dynamics_recipe = "rest,p,mf:2,ff,ff"
-- local dynamics_recipe = "p,mf:2,ff,ff"
-- local dynamics_recipe = "87,ff,37.2222:2,mf" 
-- local dynamics_recipe = "rest,p:2,ff"

-- Set emphasis for durations
command.duration_recipe = "all"
-- local duration_recipe = "16th,8th:2,quarter,crotchet"
-- local duration_recipe = "1.1:2,2:1" 

command.tuning_name = "c-Western Standard"
-- local tuning_name = "c-Pythagorean" -- Pythagorean tuning
-- local tuning_name = "cs-5 limit JI" -- 5-limit Just Intonation
-- local tuning_name = "d-3/EDO" -- 3-equal divisions of the octave. This can be any number. Just "ED" or "EDO" defaults to "ED:P8"
-- local tuning_name = "ds-"..math.pi .. "/!ED:m3" -- pi-equal divisions of the minor third while preserving the 2:1 octave
-- local tuning_name = "e-Harry Partch"
-- local tuning_name = "f-Gamma"
-- local tuning_name = "fs-88/CET" -- 88-cET: equal intervals of cents. This can be any number.
-- local tuning_name = "gf-Reinhard 128"
-- local tuning_name = "g-12/ED:1200" -- equal divisions of cents
-- local tuning_name = "gs-5/Harmonic" -- Use the 5th harmonic or overtone series. This uses all the possible ratios.

-----------------------------------------
-- Declare some variables and tables and call some functions. No need to edit anything here
-----------------------------------------

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"

local pitch_table = {} ; local frequency_table = {} ; local volume_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {} 

local midi_ins_number = 0

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()

header.reference_pitch,header.reference_pitch_name = reference_pitch()

local tempo_number = tempo_value() 

-- Generate audio frequencies
audio_freq_table, low_freq, high_freq = generate_audio_frequency_table()

-- Calculate degrees for tuning
calculated_scale_degrees = generate_relative_scale_degrees()

-- Analyze your tuning against an ideal Just Intonation
analyze_intervals()

local scale_octave = get_scale_octave() 
midi_ins_number,header.lowrange,header.highrange = instrument_range(audio_freq_table, low_freq, high_freq) 

local base_table = {}
base_table = preparse_pitches(scale_octave)
pitch_table = quantize("pitch_table", base_table)

local base_table = {}
base_table = preparse_velocities()
volume_table = quantize("volume_table", base_table)

local base_table = {}
base_table = preparse_durations()
duration_table = quantize("duration_table", base_table)

-- -- Bel Canto style algorithm (smooths out the melody to use leaps less than a perfect fifth (if possible))
-- local normalize_note_to_middle = "yes"
-- local interval_adjustment = "P8"
-- pitch_table = belcanto(pitch_table, calculated_scale_degrees, command.number_of_notes, normalize_note_to_middle, interval_adjustment)

-- panchromatic scale (utility to produce panchromatic scales (useful for testing out tunings, for example))
-- local range = "full"
-- local range = "octave"
-- pitch_table, volume_table, duration_table, command.number_of_notes = generate_panchromatic_scale(range)
 
-- Create information to be used especially for Csound but useful elsewhere. 
------------------------------------------------

pitch_table = {}
pitch_table[1] = 61
pitch_table[2] = 61
pitch_table[3] = 65
pitch_table[4] = 68
pitch_table[5] = 73

duration_table = {}
duration_table[1] = 1
duration_table[2] = 1
duration_table[3] = "."
duration_table[4] = "."
duration_table[5] = 1
duration_table[6] = 1

command.number_of_notes = 5


local a4_calibration = calibrate_to_a4()

for counter = 1, command.number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440) * a4_calibration 
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end
start_table = generate_start_table(duration_table)

-- Create Csound
-- Csound preamble
local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

if sound_font ~= "" then
   csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/" .. sound_font .. "\" \nsfpassign   0, isf\n "
end

-- End Csound preamble

local start_of_table = 1
ins_num_table[1] = 1
   
-- create specific Orchestra instrument
local orchestra_instrument = csound_instrument ; local instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument

local csound_orchestra1 = create_csound_orchestra(orchestra_instrument, instrument_num) 

local csound_score_function1 = create_csound_score_function(orchestra_instrument, instrument_num)

local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, volume_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

-- local csoundscore = csound_score_ins -- for API playback

csound_score_ins = finish_csound_score_ins(csound_score_ins)

local csound_orchestra = csound_orchestra1 

local csound_score = csound_score_function1 .. "\n" .. csound_score_ins
   
local orchestra_finish = create_finish_orchestra(tempo_number)

local csound_file = csound_preamble .. csound_orchestra .. orchestra_finish .. csound_score

local csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)

-- Lilypond
if command.generate_lilypond == "yes" then 
   require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
   local pme = "Platonic Music Engine" ; local original_composer = ""
   local original_title = "" ; local voices = 1 ; local lyrics = {}
   
   local time = "4/4" ; local no_indent = "no"
   local rest_or_space = "r" ; local use_ragged_right = "no" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"

   local header = lilypond2header(pme, original_composer, original_title, additional_info)

   local lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(calculated_scale_degrees, pitch_table, frequency_table, duration_table, volume_table, rest_or_space, avoid_collisions)

   local lilypond_score,score_info = lilypond2variables(lily_key, time, tempo_number, lily_note_table_1, lily_note_table_2)
   
   local footer = lilypond2footer(score_info, voices, lyrics, use_ragged_right, no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(complete_lilypond_score)
end      

if command.generate_feldman == "yes" then -- Feldman Graph Notation
   require("scripts/feldman_notation")
   local volume = "yes" 

   local feldman_score,latexname = midi2feldman(pitch_table, duration_table, volume_table, volume, tempo_number)
   create_latex(feldman_score,latexname)
end

if command.generate_pattern_15 == "yes" then -- Pattern 15 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_15") 
   
   pattern_15_lilypond_score,latexname = 
      midi2pattern_15(pitch_table, duration_table)

   create_latex(pattern_15_lilypond_score, latexname)
end      

if command.generate_pattern_35 == "yes" then -- Pattern 35 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_35")
   pattern_35_score,latexname = midi2pattern_35(pitch_table, duration_table, volume_table)
   
   create_latex(pattern_35_score,latexname)
end


