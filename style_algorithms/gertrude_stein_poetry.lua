-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Gertrude Stein Poetry"
header.style = "" 
header.style_algorithm_author = "David Bellows"

command.user_input = "Heraclitus" 
command.generate_score = "no"
number_of_poems = 4
command.number_of_notes = number_of_poems

command.generate_text_file = "no" -- "yes" "no"
command.generate_lyric_file = "no" -- "yes" "no"
command.generate_latex_file = "yes" -- "yes" "no"
command.generate_log = "yes" -- "yes" or "no"

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()
number_of_poems = command.number_of_notes

-------------------------------------------------------------------

require("dir_gertrude_stein_poetry/gs_poetry") ; require("dir_gertrude_stein_poetry/gs_poetry_tables")
local poetry = generate_stein_poetry()

