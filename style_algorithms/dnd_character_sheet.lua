-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Character Sheet"
header.style = "" 
header.style_algorithm_author = "David Bellows"

command.user_input = "Heraclitus" 
command.generate_score = "no"
dnd = {}

command.rules = "" -- "d20 3.5" "5e SRD"
command.class = "" -- or indicate class: "fighter" "ranger" "cleric" "wizard" "rogue" "barbarian" "monk" "paladin" "druid" "sorcerer"
command.race = "" -- or indicate a race: "human" "elf" "dwark" "halfling" "half-elf" "half-orc"
command.name = "" -- or use a name
command.level = "" -- or indicate level 1-20
command.name = ""

command.generate_log = "yes" -- "yes" or "no"

local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data,header.dedication = get_input()

-------------------------------------------------------------------

require("dir_dnd_character_sheet/dnd_character_generator") ; require("dir_dnd_character_sheet/dnd_character_generator_tables")

dnd.dnd_system = generate_system(command.rules)
dnd.class = generate_class(command.class)
dnd.race = generate_race(command.race)
dnd.name = generate_name(command.name)
dnd.level = generate_level(command.level)

generate_stats()

generate_character_sheet_latex()
