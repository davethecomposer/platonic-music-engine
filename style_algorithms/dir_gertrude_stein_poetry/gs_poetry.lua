-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

function generate_stein_poetry()
   local base_word; local first_word; local second_word; local word; local total = 0
   local tmp_result = "" ; local result = {}
   local word_list = get_keys_from_table(homophone_list)
   
   local number = command.number_of_notes

   local random_base_word = platonic_generator("random_base_word", 1, #word_list, number, "yes")
   local which_word = platonic_generator("which_word", 1, 2, number, "yes")
   local number_phrases = platonic_generator("number_phrase", 2, 10, number, "yes")
   for counter = 1, number do
      total = total + number_phrases[counter]
   end
   local transform = platonic_generator("transform", 1, 6, number * total, "yes")

   
   random.seed(hash(header.user_data .. "mesostic"),hashstream(header.user_data .. "mesostic"))
   local meta = 1
   for counter = 1, number do
      tmp_result = ""
      first_word = word_list[random_base_word[counter]]
      second_word = homophone_list[first_word]

      write_log("gs_poetry.lua", "first and second word", first_word .. " / " .. second_word)
      index1 = random.random(1, #homophone_phrases[first_word])
      index2 = random.random(1, #homophone_phrases[second_word])
      index3 = random.random(1, #homophone_phrases[first_word])
      write_log("gs_poetry.lua", "first and second phrase", homophone_phrases[first_word][index1] .. " / " .. homophone_phrases[second_word][index2] .. " / " .. homophone_phrases[first_word][index3])
      
      for sub = 1, number_phrases[counter] do
	 local transform_number = transform[meta]
	 if transform_number == 1 then
	    tmp_result = tmp_result .. first_word .. " "
	 elseif transform_number == 2 then
	    tmp_result = tmp_result .. homophone_phrases[first_word][index1] .. " "
	 elseif transform_number == 3 then
	    tmp_result = tmp_result .. string.gsub(homophone_phrases[second_word][index2], second_word, first_word) .. " "
	    -- local tmp_table = homophone_phrases[first_word][index1]:split(" ") 
	    -- for subsub = #tmp_table, 1, -1 do
	    --    tmp_result = tmp_result .. tmp_table[subsub] .. " "
	    -- end
	 elseif transform_number == 4 then
	    tmp_result = tmp_result .. homophone_phrases[second_word][index2] .. " "
	 elseif transform_number == 5 then
	    tmp_result = tmp_result .. string.gsub(homophone_phrases[first_word][index1], first_word, second_word) .. " "
	 elseif transform_number == 6 then
	    tmp_result = tmp_result .. homophone_phrases[first_word][index3] .. " "
	 end
	 meta = meta + 1
      end
      result[counter] = tmp_result
      tmp_result = " "
   end
   write_log("gs_poetry.lua", "result", result)

   if command.generate_score == "yes" then
      local body = "\n\\setlength{\\vleftmargin}{8em}\n"
      local dedication = header.dedication
      local title = "Text"
      local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
      local font = "newcent" ; local poet = "" ; local algorithm = header.algorithm
      local meter = "inspired by Gertrude Stein"
      local piece = "" 
      local instrument = ""
      local symbol = ""
      local amount
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)
      for counter = 1, #result do
	 body = body .. "\n\\begin{verse}\n" .. result[counter] .. "\n\\end{verse}"
      end
      body = body 
      local score = preamble .. latex_title .. "\n\n" .. body

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\\end{document}"
      
      create_latex(score, "Gertrude_Stein_Poetry")
   end

   return result
end
