This file discusses the format for texts taken from https://www.gutenberg.org.

For each text there should be two files in the following format:

* title_author.txt

* title_author_full.txt

The one with "full" in the name is the original as downloaded from gutenberg.org. It has the license information.

The first one has all the license information stripped out and is just the text. At the top of the file is the information for the title and author. It must be formatted as follows:

Title: Finnegans Wake
Author: James Joyce

<text begins here>

There must be exactly one blank line after the *Author* line.

I'm not sure if I am following the license requirements as set forth by gutenberg.org but I think so. The texts aren't formatted in a uniform manner so I don't think I would be able to process them automatically to just get the information we need. I feel like this is a good compromise.
