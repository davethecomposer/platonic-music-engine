-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

function choose_tarot_deck()
   local tarot_dir = command.card_deck
   
   if tarot_dir == "" then
      local random_number = #tarot_directories
      tarot_dir = table.unpack(platonic_generator("tarot_dir" .. command.question, 1, random_number, 1, "yes")) 
      tarot_dir = tarot_directories[tarot_dir]
   end
   command.card_deck = tarot_dir
   write_log("choose_tarot_deck", "deck", tarot_dir)
   return
end

function choose_spread()
   local spread = command.spread

   if spread == "" then
      local random_number = #tarot_spreads
      local tmp = table.unpack(platonic_generator("spread" .. command.question, 1, random_number, 1, "yes"))
      spread = tarot_spreads[tmp]
      command.spread = spread 
   end
   write_log("choose_spread", "spread", spread)
   return
end

function choose_reverse()
   local reverse = command.reverse
   
   if reverse == "" then
      local tmp = table.unpack(platonic_generator("reverse" .. command.question, 1, 2, 1, "yes")) 
      if tmp == 1 then
	 reverse = "no"
      else
	 reverse = "yes" 
      end     
   end
   command.reverse = reverse
   write_log("generate_tarot.lua", "reverse", reverse)
   return
end

function generate_tarot_cards_latex()
   require("dir_tarot/generate_tarot_tables")

   local card
   local xshift = {}
   local yshift = {}
   local log_table = {}
   local reverse_table = {}
   local reverse_text
   
   local result = ""

   local question = command.question

   choose_tarot_deck()
   local tarot_dir = command.card_deck
   
   result = result .. "\\node[yshift=-120] at (current page.north){\\centering\\LARGE{}\\textit{" .. question .. "}};"
   -- Uncomment the following line to print a helpful grid on the screen.
   -- Each square is 28.5 units in size. 
   -- result = result .. "\n\\node at (current page.south west){\\begin{tikzpicture}\\draw[help lines] (0,0) grid (500,500);\\end{tikzpicture}};\n"
   
   choose_spread()
   local spread = command.spread

   choose_reverse()
   local reverse = command.reverse 

   if reverse == "yes" then reverse_text = " with reversed cards allowed"
   else
      reverse_text = " with no reversed cards allowed"
   end
  
   write_log("generate_tarot.lua", "question", question)
   
   local width = spread_values[spread].width
   local number_of_cards = spread_values[spread].number_of_cards

   random.seed(hash(header.user_data .. question),hashstream(header.user_data .. question))

   local coords = spread_values[spread].coords 
   write_log("generate_tarot.lua", "coords", coords)
   
   local coords_table = coords:split(" ")

   for counter = 1, number_of_cards do
      local tmpx, tmpy = bifurcate(coords_table[counter],",")
      xshift[counter] = string.gsub(tmpx, "[()]", "")
      yshift[counter] = string.gsub(tmpy, "[()]", "")
   end

   reverse_table = platonic_generator(question, 1, 2, number_of_cards, "yes")
   
   for counter = 1, number_of_cards do
      tmp = random.random(1, #tarot_card_table)
      card = tarot_card_table[tmp]
      log_table[counter] = card
      table.remove(tarot_card_table, tmp)
      xshift[counter], tmp = bifurcate(xshift[counter],":")
      
      if reverse == "yes" and reverse_table[counter] == 2 then
	 if tmp then
	    tmp = tmp + 180
	 else
	    tmp = 180
	 end
      end
      
      if tmp then
	 rotate = ",angle=" .. tmp .. ",origin=c"
      else
	 rotate = ""
      end
      
      local tarot_card = "{\\begin{tikzpicture}\n\\includegraphics[width=" .. width .. "cm" .. rotate .. "]{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
      result = result .. "\n\\node[xshift=" .. xshift[counter] .. ",yshift=" .. yshift[counter] .. "] at (current page.south west)\n" .. tarot_card   
   end

   write_log("generate_tarot.lua", "cards used", log_table)
   
   local result = "\n\\begin{tikzpicture}[remember picture,overlay]\n" .. result .. "\n\\end{tikzpicture}\n"
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Divination"
      local composer = "David Bellows"
      local arranger = "Platonic Music Engine"
      local font = "newcent"
      command.card_deck = string.gsub(tarot_dir, "_", " ") 
      local poet = "\\textit{" .. command.card_deck .. "} tarot deck"
      algorithm = header.algorithm
      local meter = "\\textit{" .. command.spread .. "} spread" .. reverse_text
      local piece = "" 
      local instrument = ""
      local symbol = ""
      local amount
         
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. latex_title 

      local body = result 
      local score = score .. body --.. "\n}\\end{tikzpicture}"

      local footer = latex_footer()
      
      local score = score .. footer .. copyright_art[tarot_dir] .. "\n\n\\end{document}" 

      create_latex(score, "Tarot_Cards")
      create_latex(score, "Tarot_Cards")
   end
   return
end
