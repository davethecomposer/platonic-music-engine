-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

tarot_directories = {}
tarot_directories = {"clip_art", "highway_art"}

tarot_spreads = {}
tarot_spreads = {"Yes or No", "Past, Present, Future", "Celtic Cross", "Blind Spot", "The Cross", "Horseshoe", "Ankh", "High Priestess"}

spread_values = {}
spread_values = {
   ["Yes or No"] = {["width"] = 5, ["number_of_cards"] = 1, ["coords"] = "(228,313)"},
   ["Past, Present, Future"] = {["width"] = 5, ["number_of_cards"] = 3, ["coords"] = "(57,313) (228,313) (399,313)"},
   ["Celtic Cross"] = {["width"] = 3, ["number_of_cards"] = 10, ["coords"] = "(142.5,313) (224:90,315) (28.5,313) (370.5,313) (200,454) (200,171) (484.5,114) (484.5,256.5) (484.5,398) (484.5,540.5)"},
   ["Blind Spot"] = {["width"] = 5, ["number_of_cards"] = 4, ["coords"] = "(114,399) (342,114) (114,114) (342,399)"},
   ["The Cross"] = {["width"] = 5, ["number_of_cards"] = 4, ["coords"] = "(57,256.5) (399,256.5) (228,57) (228,456)"},
   ["Horseshoe"] = {["width"] = 3, ["number_of_cards"] = 7, ["coords"] = "(114,57) (114,228) (114,399) (256.5,512) (399,399) (399,228) (399,57)"},
   ["Ankh"] = {["width"] = 2, ["number_of_cards"] = 9, ["coords"] = "(285,57) (285,142.5) (285,228) (142.5,313.5) (285,399) (142.5,484.5) (285,570) (427.5,484.5) (427.5,313.5"},
   ["High Priestess"] = {["width"] = 3, ["number_of_cards"] = 9, ["coords"] = "(256.5,57) (256.6,199.5) (57,199.5) (57,342) (145,342) (57,484.5) (256,541.5) (455.5,484.5) (455.5,199.5)"},
}

tarot_card_table = {}
tarot_card_table = {"ace_of_cups", "empress", "hermit", "knight_of_cups", "page_of_pentacles", "six_of_cups", "the_devil",           "two_of_pentacles", "ace_of_pentacles", "five_of_cups", "hierophant", "knight_of_pentacles", "page_of_swords", "six_of_pentacles", "the_moon", "two_of_swords", "ace_of_swords", "five_of_pentacles", "high_priestess", "knight_of_swords", "page_of_wands", "six_of_swords", "the_star",         "two_of_wands", "ace_of_wands", "five_of_swords", "judgement", "knight_of_wands", "queen_of_cups", "six_of_wands", "the_sun",               "wheel_of_fortune", "chariot", "five_of_wands", "justice", "lovers", "queen_of_pentacles", "strength", "the_tower", "death", "fool",     "magician", "queen_of_swords", "temperance", "the_world", "eight_of_cups", "four_of_cups", "king_of_cups", "nine_of_cups",         "queen_of_wands", "three_of_cups", "eight_of_pentacles", "four_of_pentacles", "king_of_pentacles", "nine_of_pentacles", "seven_of_cups",        "ten_of_cups", "three_of_pentacles", "eight_of_swords", "four_of_swords", "king_of_swords", "nine_of_swords", "seven_of_pentacles",   "ten_of_pentacles", "three_of_swords", "eight_of_wands", "four_of_wands", "king_of_wands", "nine_of_wands", "seven_of_swords", "ten_of_swords", "three_of_wands", "emperor", "hangman", "page_of_cups", "seven_of_wands", "ten_of_wands", "two_of_cups"}

copyright_art = {}
copyright_art = {
   ["clip_art"] = "\n{\\small{}Some of the artwork used in this work is from \\href{https://openclipart.org/}{openclipart.org} and is in the public domain",
   ["highway_art"] = "\n{\\small{}Some of the artwork used in this work is from \\href{https://github.com/mcnuttandrew/tarot-deck}{Andrew McNutt} under the MIT License",
}
