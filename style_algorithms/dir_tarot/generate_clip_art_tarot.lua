-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

style_table = {}
style_table = {"White on White", "Black Square", "Red Square", "Black Circle", "Black Cross"}

function generate_tarot_cards_latex()
   require("dir_tarot/generate_tarot_tables")
   local result = ""
   local node1x = 10 --math.random(100,500) -- 100, 175 -- 500, 550
   local node1y = 450 --math.random(175,500)
   local node2x = 160 --math.random(100,500)
   local node2y = 450 --math.random(175,500)
   local node3x = 310 --math.random(100,500)
   local node3y = 450 --math.random(175,500)
   local node4x = 460
   local tarot_dir = command.card_deck
   local question = "Will I ever be happy?"
   result = "\\node[yshift=-120] at (current page.north){\\centering\\LARGE{}" .. question .. "};"
   local card = "fool"
   local tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "emperor"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   card = "empress"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y .. "] at (current page.south west)\n" .. tarot_card

   card = "magician"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "high_priestess"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "hierophant"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "lovers"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "chariot"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card

   card = "justice"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "hermit"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "wheel_of_fortune"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "strength"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "hangman"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   
   
   card = "death"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "temperance"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "the_devil"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "the_tower"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "the_star"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "the_moon"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "the_sun"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card
   
   card = "judgement"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "the_world"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "ace_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "two_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   card = "three_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y .. "] at (current page.south west)\n" .. tarot_card

   card = "four_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "five_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "six_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "seven_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "eight_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card

   card = "nine_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "ten_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "page_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "knight_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "queen_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "king_of_cups"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "ace_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "two_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   card = "three_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y .. "] at (current page.south west)\n" .. tarot_card

   card = "four_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "five_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "six_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "seven_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "eight_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card

   card = "nine_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "ten_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "page_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "knight_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "queen_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "king_of_pentacles"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "ace_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "two_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   card = "three_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y .. "] at (current page.south west)\n" .. tarot_card

   card = "four_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "five_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "six_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "seven_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "eight_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card

   card = "nine_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "ten_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "page_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "knight_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "queen_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "king_of_swords"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "ace_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "two_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   card = "three_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y .. "] at (current page.south west)\n" .. tarot_card

   card = "four_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "five_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "six_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "seven_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node3y -205 .. "] at (current page.south west)\n" .. tarot_card

   card = "eight_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card

   card = "nine_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "ten_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "page_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node3x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   card = "knight_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node4x .. ",yshift=" .. node1y -410 .. "] at (current page.south west)\n" .. tarot_card

   result = result .. "\n\\end{tikzpicture}\n\\newpage\n\\begin{tikzpicture}[remember picture,overlay]\n"

   card = "queen_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node1x .. ",yshift=" .. node1y .. "] at (current page.south west)\n" .. tarot_card   

   card = "king_of_wands"
   tarot_card = "{\\begin{tikzpicture}\n\\includegraphics{dir_tarot/" .. tarot_dir .. "/" .. card .. "};\n\\end{tikzpicture}\n};\n"
   result = result .. "\n\\node[xshift=" .. node2x .. ",yshift=" .. node2y .. "] at (current page.south west)\n" .. tarot_card

   
   local result = "\n\\begin{tikzpicture}[remember picture,overlay]\n" .. result .. "\n\\end{tikzpicture}\n"
   if command.generate_score == "yes" then
      local dedication = header.dedication
      local title = "Divination"
      local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
      local font = "newcent" ; local poet = "" ; algorithm = header.algorithm
      local meter = ""
      local piece = "piece" 
      local instrument = command.spread
      local symbol = ""
      local amount
      
      local preamble = latex_preamble(font)
      local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, "") --pretty_system)

      local score = preamble .. latex_title 

      local body = result 
      local score = score .. body --.. "\n}\\end{tikzpicture}"

      local footer = latex_footer()
      
      local score = score .. footer .. "\n\n\\end{document}"

      create_latex(score, "Tarot_Cards")

   end
   return
end
