-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Platonic_Score"
header.style = ""
header.style_algorithm_author = "David Bellows"

-------------------------------------------
command.user_input = "Heraclitus"

command.generate_log = "yes"

command.generate_lilypond = "no"
command.generate_feldman = "no"
command.generate_pattern_15 = "no"
command.generate_pattern_35 = "no" 
command.wave = "no" ; command.flac = "no" ; command.ogg = "no" ; local ogg_tag = "yes"

command.number_of_notes = 15

local audio_len = 20
-- local audio_len = "max"

command.tempo_word = 60

-- local form = "Horizontal"
local form = "Vertical"

-- header.style = "Standard" -- Standard
-- header.style = "Time Lord Version" -- Start at a certain time
header.style = "God-Eye View" -- Compress to fit within audio_len

------------------------------------------
local command_line_options = {...}
get_args(command_line_options)

create_log()
header.user_data, header.dedication = get_input(command)

local sonic_events = command.number_of_notes
local tempo = tempo_value() 

-----------------------------------------
-- Platonic Score Style Algorithm
-----------------------------------------
require("dir_platonic_score/platonic_score")
amplitude_table, frequency_table, duration_table, start_table, ins_num_table, pan_table, ins_name_table = platonic_score_generator(sonic_events, audio_len, tempo, form)

-- Create Csound
require("scripts/csound") ; require("scripts/csound_instruments_tables")
-- Csound preamble
command.instrument = ""

local csound_preamble = ""
local csound_orchestra = ""
local csound_score = ""
local csound_file_name = ""

local csound_preamble_comments, csound_preamble_options, csound_preamble_orchestra = create_csound_preamble()

local csound_preamble = csound_preamble_comments .. csound_preamble_options .. csound_preamble_orchestra

local start_of_table = 1

-- create specific Orchestra instrument
local csound_orchestra0 = ""
if form == "Vertical" then 
   csound_orchestra0 = "" ; csound_score_function0 = "f0 " .. duration_table[0] .. " ; timer in seconds\n" 
else csound_orchestra0 = "" ; csound_score_function0 = "" 
end

-- create specific Orchestra instrument
local orchestra_instrument = "SineWave" ; local instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
local csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
local csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)

-- create specific Orchestra instrument
orchestra_instrument = "Sawtooth" ; instrument_num = 2 ; ins_name_table[instrument_num] = orchestra_instrument
csound_orchestra2 = create_csound_orchestra(orchestra_instrument,instrument_num)
csound_score_function2 = create_csound_score_function(orchestra_instrument,instrument_num)

-- create specific Orchestra instrument
orchestra_instrument = "Square" ; instrument_num = 3 ; ins_name_table[instrument_num] = orchestra_instrument
csound_orchestra3 = create_csound_orchestra(orchestra_instrument,instrument_num)
csound_score_function3 = create_csound_score_function(orchestra_instrument,instrument_num)

-- create specific Orchestra instrument
orchestra_instrument = "Triangle" ; instrument_num = 4 ; ins_name_table[instrument_num] = orchestra_instrument
csound_orchestra4 = create_csound_orchestra(orchestra_instrument,instrument_num)
csound_score_function4 = create_csound_score_function(orchestra_instrument,instrument_num)

local csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, ins_num_table, ins_name_table, amplitude_table, frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

csound_score_ins = finish_csound_score_ins(csound_score_ins)

local csound_orchestra = csound_orchestra0 .. csound_orchestra1 .. csound_orchestra2 .. csound_orchestra3 .. csound_orchestra4


local csound_score = csound_score_function0 .. csound_score_function1 .. csound_score_function2 .. csound_score_function3 .. csound_score_function4 .. "\n" .. csound_score_ins 

local orchestra_finish = create_finish_orchestra(tempo)
local csound_file = csound_preamble .. csound_orchestra .. orchestra_finish .. csound_score
local csound_file_name = create_csound_file(csound_file)

-- Create Audio
create_csound_audio_file(csound_file_name, ogg_tag)
