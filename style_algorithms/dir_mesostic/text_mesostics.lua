-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

import_text = function(full_text)
   local tmp
   local tmp_text
   local j = 0
   source_text_table = {}
   -- Read in the text to be processed
   if full_text == "all" then
      full_text_name = "moby_word_II"
   elseif full_text == "common" then
      full_text_name = "moby_common"
   else
      full_text_name = full_text
   end

   tmp = io.open("texts/" .. full_text_name .. "/" .. full_text_name .. ".txt", "r")
  
   if tmp == nil then -- print("file not found") 
      tmp_text = full_text_name
  
      -- print(tmp_text, full_text_name)
      -- j = 0
      -- title = ""
      -- author = ""
   else
      tmp_text = tmp:read("*a")
      tmp:close()
   end   
      j = string.find(tmp_text,"\n\n") 
      local text_header = string.sub(tmp_text,1,j)
      title, author = bifurcate(text_header, "\n")
      _, title = bifurcate(title, ": ")
      _, author = bifurcate(author, ": ")
--   end
   
   -- Remove newlines and replace them with spaces
   tmp_text = string.gsub(tmp_text,"\n+"," ")
   tmp_text = string.lower(tmp_text) 
   
   local table = {}

   local counter = 1 ; local i = j +1 ; j = i ; local old_j = i ; text_table = {}

   local end_of_string = string.len(tmp_text)

   -- Find each space and put the letters between them
   -- into a table of words. The letter i represents where
   -- the word begins and j is where the space is.
   while i <= end_of_string do   
      j = string.find(tmp_text," ",i) 
      text_table[counter] = string.sub(tmp_text,i,j-1)
      if j then -- At the end of the file j will be nil.
	 i = j +1
	 old_j = j +1
      else 
	 i = old_j + i    
      end
      counter = counter +1
   end
   
   if full_text == "all" then
      random_word_table = {}
      random_word_table = platonic_generator("random_word_table", 1, 354984, 200000, "yes")
      for counter = 1, 200000 do
	 source_text_table[counter] = text_table[random_word_table[counter]]
      end
   elseif full_text == "common" then
      random_word_table = {}
      random_word_table = platonic_generator("random_word_table", 1, 74550, 200000, "yes")
      for counter = 1, 200000 do
	 source_text_table[counter] = text_table[random_word_table[counter]]
      end
   else source_text_table = shallowcopy(text_table)
   end
   write_log("text_mesostics.lua", "title", title)
   write_log("text_mesostics.lua", "author", author)
   return source_text_table, title, author
end

find_mesostics = function(source, spine, rule, wing_rule, word_rule, punctuation_rule)
   local spine_table = {} ; local mesostic_table = {} ; local current_word = {} ; local mesostic_number_table = {} ;  number_of_characters = {}
   local left_wing_words = "" ; local right_wing_words = ""
   local spine_counter ; local word_counter = 1 ; local table_counter = 1 
   local spine_letter ; local old_spine = "" ; local wing_word = ""
   local current_word ; local replace ; local result 
   local hit_pos = 0 ; local next_pos = 0 ; local old_pos = 0 ; local wing_old_spine
   local left_wing_number = 0 ; right_wing_number = 0 ; previous_word_counter = 0
   local fail_counter = 0 ; local punctuation
   local number = command.number_of_notes

   write_log("text_mesostics.lua", "spine", spine)
   write_log("text_mesostics.lua", "rule", rule)
   write_log("text_mesostics.lua", "wing_rule", wing_rule)
   write_log("text_mesostics.lua", "word_rule", word_rule)
   write_log("text_mesostics.lua", "punctuation_rule", punctuation_rule)
   
   spine = string.gsub(spine," ","")
   spine = string.lower(spine)
   
   local spine_length = #spine 
   for i = 1, spine_length do
      spine_table[i] = spine:sub(i, i) 
   end

   spine_letter = spine_table[1]
   random = require("scripts/pcg")
   random.seed(hash(header.user_data .. "mesostic"),hashstream(header.user_data .. "mesostic"))
   
   for counter = 1, number do -- how many poems to find
      spine_counter = 1
      old_spine = spine_table[1]
      
      while spine_counter < spine_length +1 do
	 wing_word = ""
	 left_wing_words = ""
	 right_wing_words = ""
 
	 if spine_counter > spine_length then
	    spine_counter = 1
	 end

	 if word_counter > #source then
	    word_counter = 1
	 end
	 
	 spine_letter = spine_table[spine_counter]

	 if word_rule == "random" then
	    word_counter = random.random(1, #source)
	 end
	 current_word = source[word_counter]
	 
	 local tmp = spine_counter + 1 
	 if tmp > #spine then tmp = 1 end
	 next_spine = spine_table[tmp] 

	 -- Check to see if current spine, next spine and previous spine are in the current
	 -- word and for current and spine where they are
	 
	 number_hits = select(2, string.gsub(current_word, spine_letter, ""))
	 hit_pos = string.find(current_word, spine_letter)
	 if spine_letter ~= next_spine then
	    number_next = select(2, string.gsub(current_word, next_spine, ""))
	    next_pos = string.find(current_word, next_spine, hit_pos) 
	 end
	 old_pos = string.find(current_word, old_spine) 
	 
	 if hit_pos == nil then hit_pos = 0 end 
	 if next_pos == nil then next_pos = 0 end 
	 if old_pos == nil then
	    old_pos = #current_word 
	 end 

	 -- If the next spine only occurs before the spot where the current spine is then
	 -- this is ok
	 if next_pos < hit_pos then number_next = 0 end 

         if number_hits == 1 and number_next == 0 and old_pos >= hit_pos then
	    replace = string.upper(spine_letter)
	    result = string.gsub(current_word, spine_letter, replace)
	    mesostic_table[table_counter] = result
	    mesostic_number_table[table_counter] = word_counter
	    table_counter = table_counter +1
	    spine_counter = spine_counter +1 
	    fail_counter = 0
	    
	    -- Start generating wing words
	    local tmp = spine_counter -2
	    if tmp == 0 then tmp = spine_length end
	    wing_old_spine = spine_table[tmp]

	    -- Left wing words
	    left_wing_number = word_counter

	    if word_counter < 12 then --9
	       low_spot = word_counter
	    else
	       low_spot = word_counter -11 -- -8
	    end
	    
	    for subcounter = word_counter -1, low_spot, -1 do
	       local tmp_old = string.find(source[subcounter], wing_old_spine) 
	       local tmp_current = string.find(source[subcounter], spine_letter)
	       if punctuation_rule == "yes" then
		  punctuation = string.find(source[subcounter], "[%.%!%?%,%;%--%:%---]") 
	       else
		  punctuation = nil
	       end    
	       
	       if rule == "50%" then
		  if tmp_old or punctuation then
		     left_wing_number = subcounter +1 
		     break
		  end
	       elseif rule == "100%" then	       
		  if tmp_old or tmp_current or punctuation then
		     left_wing_number = subcounter +1
		     break 
		  end
	       elseif punctuation then
		  left_wing_number = subcounter +1
		  break
	       else -- rule == "0%" then	  
		  left_wing_number = subcounter +1
	       end

	    end

	    if word_counter - left_wing_number > 10 then left_wing_number = 10 end 

	    while right_wing_number >= left_wing_number do
	       left_wing_number = left_wing_number +1
	    end

	    if wing_rule == "surrounding" or wing_rule == "both" then
	       if word_counter < left_wing_number then left_wing_number = word_counter end
	       local tmp = random.random(left_wing_number, word_counter) 
	       left_wing_number = tmp
	    end
	    
	    for i = left_wing_number, word_counter -1 do
	       if wing_rule == "50/50" or wing_rule == "both" then
		  yes_or_no = random.random(0, 1)
		  if yes_or_no == 1 then
		     left_wing_words = left_wing_words .. source[i] .. " "
		  end
	       else
		  left_wing_words = left_wing_words .. source[i] .. " "
	       end
	    end

	    while string.find(left_wing_words .. " " .. result, "%u") > 45 do
	       local first_space, _ = string.find(left_wing_words, " ") 
	       local delete_point = -(#left_wing_words - first_space) 
	       left_wing_words = string.sub(left_wing_words, delete_point) 
	    end
	    number_of_characters[table_counter -1] = #left_wing_words + hit_pos -1 -- we incremented table_counter earlier so we have to go back 1
	    wing_word = left_wing_words .. result .. " "

	    -- Right wing words
	    right_wing_number = word_counter +11 -- 7
	    
	    high_spot = word_counter +11 -- 7
	    if high_spot > #source then high_spot = #source end

	    for subcounter = word_counter +1, high_spot do 
	       local tmp_next = string.find(source[subcounter], next_spine)
	       if tmp_next == nil then tmp_next = "" end
	       local tmp_current = string.find(source[subcounter], spine_letter)
	       if punctuation_rule == "yes" then
		  local tmp = subcounter -1
		  if tmp == 0 then tmp = 1 end
		  punctuation = string.find(source[tmp], "[%.%!%?%,%;]")
	       else
		  punctuation = nil
	       end    
	       
	       if rule == "50%" or rule == "100%" then
		  if tmp_next ~= "" or tmp_current or punctuation then
		     right_wing_number = subcounter -1 -- - 1
		     break
		  end
	       else		 
		  right_wing_number = subcounter -1
		  if punctuation then break end
	       end
	    end

	    if wing_rule == "surrounding" or wing_rule == "both" then
	       right_wing_number = random.random(word_counter, right_wing_number)
	    end

	    if right_wing_number > #source then right_wing_number = 1 end
	    for i = word_counter +1, right_wing_number do
	       if wing_rule == "50/50" or wing_rule == "both" then
		  yes_or_no = random.random(0, 1)
		  if yes_or_no == 1 then
		     right_wing_words = right_wing_words .. source[i] .. " "
		  end
	       else
		  right_wing_words = right_wing_words .. source[i] .. " "
	       end
	    end

	    local first_space
	    local reverse_counter = 1
	    while #right_wing_words + hit_pos +2 >= 45 do -- the +2 is for the space after the hit_pos and the spot after it
	       first_space = string.find(right_wing_words, " ", #right_wing_words - reverse_counter)
	       if first_space then 
		  right_wing_words = string.sub(right_wing_words, 1, first_space)
	       end
	       reverse_counter = reverse_counter + 1
	    end

	    wing_word = wing_word .. right_wing_words
	    previous_word_counter = word_counter
	    old_spine = spine_letter
	    mesostic_table[table_counter -1] = wing_word
	 else
	    fail_counter = fail_counter + 1 
	 end -- if number_hits == 1 ...
	 if fail_counter > #source then print("Failed to find one or more mesostics that met your settings.")
	    for k in next, mesostic_table do rawset(mesostic_table, k, nil) end -- This erases the table in a clean way.
	    break
	 end
	
	 word_counter = word_counter +1	    
      end
   end
   write_log("text_mesostics.lua", "mesostic_table", mesostic_table)
   return mesostic_table, mesostic_number_table, number_of_characters, spine
end

format_mesostics = function(mesostic_table, number_of_characters, cleaned_spine, style)
   local formatted_mesostic_table = {}
   local spaces = ""
   formatted_mesostic_table[1] = ""
   if style == "cantos" then
      local end_of_spine = #cleaned_spine
      local table_counter = 0
      for counter = 1, command.number_of_notes do
	 formatted_mesostic_table[counter] = ""
	 for subcounter = 1, end_of_spine do
	    formatted_mesostic_table[counter] = formatted_mesostic_table[counter] .. mesostic_table[table_counter + subcounter]
	    formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "%s%s%s", " ")
	 end
	 table_counter = table_counter + end_of_spine
	 local padding = 95 - #formatted_mesostic_table[counter]
	 local spaces = " "
	 for i = 1, padding do
	    spaces = spaces .. " "
	 end
	 formatted_mesostic_table[counter] = spaces .. formatted_mesostic_table[counter]
      end
   end
   
   local center_of_line = 45

   --   for counter = 1, command.number_of_notes do
   if style ~= "cantos" then
      for counter = 1, #mesostic_table do
	 --for subcounter = 1, #spine do 
	 local padding = center_of_line - number_of_characters[counter]
	 spaces = " "
	 for i = 1, padding do
	    spaces = spaces .. " "
	 end

	 formatted_mesostic_table[counter] = (spaces .. mesostic_table[counter])

	 if counter % #cleaned_spine == 0 then
	    formatted_mesostic_table[counter] = formatted_mesostic_table[counter] .. "\n"
	 end 
      end
   end
   return formatted_mesostic_table
end

generate_text = function(mesostic_name, formatted_mesostic_table)
   local usable_text = {} 

   for counter = 1, #formatted_mesostic_table do
      usable_text[counter] = formatted_mesostic_table[counter] .. "\n"
   end
   if command.generate_text_file == "yes" then
      local log_file = io.open(mesostic_name .. ".txt", "w")
      for counter = 1, #formatted_mesostic_table do
	 log_file:write(usable_text[counter])
      end
      log_file:close()
   end
   return usable_text
end

generate_lyrics = function(mesostic_name, mesostic_table)
   local lyric_text
   lyric_text = table.concat(mesostic_table)
   lyric_text = string.gsub(lyric_text, "%s%s+", " ")
   if command.generate_lyric_file == "yes" then
      local log_file = io.open(mesostic_name .. ".lyric","w")
      log_file:write(lyric_text)
      log_file:close()
   end
   return lyric_text
end

generate_latex = function(mesostic_table, lyric_text, mesostic_name, formatted_mesostic_table, book_title, author, spine, cleaned_spine, bold, style)
   local dedication = header.dedication
   local title = "Poem"
   local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
   local font = "noto" ; local poet = "inspired by John Cage" ; local algorithm = header.algorithm

   local piece = "piece" 
   local instrument = ""
   local preamble = latex_preamble(font)
   local text = ""
   local font_size = ""
   local letter_space = ""
   local close_letter_space = ""
   local line_space = 1
   local blank_lines = {}
   local table_counter = 1 ; local font_counter = 1
   local tallest_font = {}
   
   local book_title = string.gsub(book_title, "_", " ")
   book_title = "\\textit{" .. book_title .. "}"
   author = string.gsub(author, "_", " ")
   local meter = book_title .. " " .. "by" .. " " .. author
   local temperament_name = "spine = ``" .. spine .. "''"

   local line_size = {}
   local font_height = {}
   
   local random_word_table = {}

   if style == "merce" then
      random.seed(hash(header.user_data .. "mesostic"),hashstream(header.user_data .. "merce"))
      letter_space = "{\\lsstyle "
      close_letter_space = " }"
      random_font_table = platonic_generator("random_font_table", 1, 4, #lyric_text, "yes")

      familysize_table = {1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2}
      random_family_table = platonic_generator("random_family_table", 1, #familysize_table, #lyric_text, "yes")

      fontsize_table = --[[{5, 6, 7, 8,]] {9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 28, 30, 32, 34, 36, 38, 40, 43, 46, 49, 52, 55, 58, 62, 66, 70, 74}
      random_fontsize_table = platonic_generator("random_fontsize_table", 1, #fontsize_table, #lyric_text, "yes")
   end
  
   if style ~= "cantos" then
      for counter = 1, #formatted_mesostic_table do
	 formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "%s%s+", "")
	 line_size[counter] = #formatted_mesostic_table[counter]
	 local tmp = ""
	 
	 if style == "merce" then
	    local tmp_table = {}
	    for i = 1, #formatted_mesostic_table[counter] do
	       tmp_table[i] = formatted_mesostic_table[counter]:sub(i, i) 
	    end
	    line_space = .5
	    for subcounter = 1, #tmp_table do
	       local tmp_random = random_font_table[table_counter]
	       local tmp_family = familysize_table[random_family_table[table_counter]]
	       local tmp_random_size = fontsize_table[random_fontsize_table[table_counter]]
	       if tmp_table[subcounter] == " " then
		  tmp_table[subcounter] = "{ }%\n"
		  font_height[font_counter] = 1 
	       else
		  if tmp_random == 1  then
	    	     tmp_table[subcounter] = string.gsub(tmp_table[subcounter], ".", "{%1}")
		  elseif tmp_random == 2  then
		     tmp_table[subcounter] = string.gsub(tmp_table[subcounter], ".", "{\\textit %1}")
		  elseif tmp_random == 3  then
	    	     tmp_table[subcounter] = string.gsub(tmp_table[subcounter], ".", "{\\textbf %1}")
		  elseif tmp_random == 4  then
		     tmp_table[subcounter] = string.gsub(tmp_table[subcounter], ".", "{\\textit{\\textbf %1}}")
		  end      

	    	  if tmp_family == 1 then
		     tmp_table[subcounter] = "{\\textsf" .. tmp_table[subcounter] .. "}"
		  elseif tmp_family == 2 then
		     tmp_table[subcounter] = "{\\textrm" .. tmp_table[subcounter] .. "}"
		  elseif tmp_family == 3 then
	     	     tmp_table[subcounter] = "{\\calligra" .. tmp_table[subcounter] .. "}"
		  -- elseif tmp_family == 4 then
	     	  --    tmp_table[subcounter] = "{\\hminfamily" .. tmp_table[subcounter] .. "}"
		  end

		  tmp_table[subcounter] = "{\\fontsize{" .. tmp_random_size .. "}{1em}\\selectfont " .. tmp_table[subcounter] .. "}%\n"
		  font_height[font_counter] = tmp_random_size
	       end

		     
	       if string.find(tmp_table[subcounter],"%u") then
		  tmp_table[subcounter] = "}|" .. letter_space .. tmp_table[subcounter] .. "}|" .. letter_space 
	       end
	       table_counter = table_counter + 1
	       font_counter = font_counter +1
	    end
	    tmp = table.concat(tmp_table)
	 elseif style == "regular" then
	    tmp = formatted_mesostic_table[counter]
	    if bold == "yes" then 
	       tmp = string.gsub(tmp, "[%u]", "|\\textbf{%1}|") 
	    else
	       tmp = string.gsub(tmp, "[%u]", "|%1|")
	    end
	 end
	 table.sort(font_height)
	 tallest_font[counter] = font_height[#font_height]
	 font_height = {}
	 font_counter = 1
	 formatted_mesostic_table[counter] = tmp 
      end
   end	 
	 
   if style == "cantos" then
        
      for counter = 1, command.number_of_notes do
	 line_size[counter] = #formatted_mesostic_table[counter]
	 formatted_mesostic_table[counter] = formatted_mesostic_table[counter] .. " \\\\\n"
	 
	 if bold == "yes" then
	    formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "[%u]", "\\textbf{%1}")
	 else
	    formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "[%u]", "{%1}")
	 end
	 formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "[%_%.%!%?%,%;%--%:%---]", "")
      end
   end
   
   -- Replace special characters from TeX/LaTeX. Will probably need to expand this list.  & % $ # _ { } ~ ^ \
   for counter = 1, #formatted_mesostic_table do
      formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "_", "\\_")
      formatted_mesostic_table[counter] = string.gsub(formatted_mesostic_table[counter], "&", "\\&")
   end
   
   table.sort(line_size)
     
   local biggest_line = line_size[#line_size]

   if style == "cantos" then
      if biggest_line <= 96 then
	 font_size = "\\normalsize"
      elseif biggest_line > 96 and biggest_line <= 105 then 
	 font_size = "\\small"
      elseif biggest_line > 105 and biggest_line <= 120 then
	 font_size = "\\footnotesize"
      elseif biggest_line > 120 and biggest_line <= 135 then
	 font_size = "\\scriptsize"
      elseif biggest_line > 135 then
	 font_size = "\\tiny"
      end
   else
      if biggest_line <=70 then
	 font_size = "\\large"
      elseif biggest_line > 70 and biggest_line < 85then
	 font_size = "\\normalsize"
      elseif biggest_line > 85 and biggest_line < 95 then 
	 font_size = "\\small"
      elseif biggest_line >= 95 then
	 font_size = "\\footnotesize"
      end
   end
   
   if style == "merce" then font_size = "\\footnotesize" end

   -- This is where we calculate how many blank lines to use after each line
   -- for most mesostics this should be 1. For "merce" this depends on the
   -- tallest font in the line.

   blank_lines[1] = ""
   for counter = 1, #formatted_mesostic_table do --#tallest_font do
      if style == "merce" then
	 -- line_space = .55
   	 if tallest_font[counter]<= 15 then
   	    blank_lines[counter -1] = ""
	 elseif tallest_font[counter] > 15 and tallest_font[counter] <= 25 then
	    blank_lines[counter -1] = " ||\\\\\n"
   	 elseif tallest_font[counter] > 25 and tallest_font[counter] <= 35 then
	    blank_lines[counter -1] = " ||\\\\\n||\\\\\n"
	 elseif tallest_font[counter] > 35 and tallest_font[counter] <= 45 then
	    blank_lines[counter -1] = " ||\\\\\n||\\\\\n||\\\\\n"
 	 elseif tallest_font[counter] > 45 and tallest_font[counter] <= 55 then
	    blank_lines[counter -1] = " ||\\\\\n||\\\\\n||\\\\\n||\\\\\n"
	 elseif tallest_font[counter] > 55 and tallest_font[counter] <= 65 then
	    blank_lines[counter -1] = " ||\\\\\n||\\\\\n||\\\\\n||\\\\\n||\\\\\n"
	 elseif tallest_font[counter] > 65 then
	    blank_lines[counter -1] = " ||\\\\\n||\\\\\n||\\\\\n||\\\\\n||\\\\\n||\\\\\n"
   	 end
      else
   	 blank_lines[counter -1] = ""
      end
      -- formatted_mesostic_table[counter] = formatted_mesostic_table[counter] .. blank_lines[counter]
   end
   
   local table_counter = 1 
	 local tmp_height = {}
   if style ~= "cantos" then
      for counter = 1, command.number_of_notes do
	 formatted_mesostic_table[table_counter] = "\n\\linespread{" .. line_space .. "}\n\\begin{mesostic}[" .. font_size .. "]\n" .. blank_lines[table_counter -1] .. letter_space .. formatted_mesostic_table[table_counter] 

	 for subcounter = 1, #cleaned_spine do
	    if subcounter < #cleaned_spine then
	       formatted_mesostic_table[table_counter] = formatted_mesostic_table[table_counter] .. close_letter_space .. "\\\\\n" .. blank_lines[table_counter] .. letter_space
	    else
	       formatted_mesostic_table[table_counter] = formatted_mesostic_table[table_counter] .. close_letter_space .. "\n\\end{mesostic}\n"	    
	    end
	    table_counter = table_counter + 1
	 end
      end
   else 
      formatted_mesostic_table[1] = "\n\\begin{adjustwidth}{-2cm}{2cm}\n\\begin{flushright}\n" .. font_size .. " " .. formatted_mesostic_table[1]
      formatted_mesostic_table[#formatted_mesostic_table] = formatted_mesostic_table[#formatted_mesostic_table] ..  "\n\\end{flushright}\n\n\\end{adjustwidth}"
   end
   
   local tmp_text = table.concat(formatted_mesostic_table)
   
   local latex_title = latex_title(dedication, title, instrument, poet, composer, arranger, algorithm, meter, piece, temperament_name, pretty_system)

   local score = preamble .. "\n\\usepackage{environ,xparse}\n\\usepackage{anyfontsize}\n\\usepackage{changepage}\n\\input{dir_mesostic/mesostic.sty}\n" .. latex_title
   
   if style == "merce" then
      score = string.gsub(score, "\\usepackage{microtype}", "\\usepackage[letterspace=-150]{microtype}")
   end
  
   local score = score .. "\\\\\\\\\\\\\n\\setlength{\\lineskiplimit}{-100pt}\n" .. tmp_text 

   local footer = latex_footer()
   
   local score = score .. footer .. "\n\\end{document}"

   local latex_name = mesostic_name
   if command.generate_latex_file == "yes" then
      create_latex(score, latex_name)
   end
   
   return
end
