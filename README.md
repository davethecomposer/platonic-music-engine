Hello and welcome to the Platonic Music Engine (official website: [www.platonicmusicengine.com](http://www.platonicmusicengine.com)).

The Platonic Music Engine (PME) takes an initial input from the user and converts it into music and other forms of art. You can select various musical properties (like scale, tuning, durations, dynamics, etc) as well as various musical styles based on existing works (Bach, etc) or generic musical ideas (serialism, etc) or even ideas in writing, painting, and other media. And then print out the results (eg, standard sheet music, graphical scores, other visual representations) in addition to your audio files. 

Please read this README first. After that check out the Tutorial located in the docs directory (*tutorial.pdf*). It will show you how to use and manipulate the *simple_melody.lua* file in the "style_algorithm" directory. Or just run that file and start playing around with the settings in that program -- it's pretty well-commented with examples.

Here are the requirements for running the Platonic Music Engine. Note, these are approximately in the order of necessity.

1. Linux though probably MacOS and Windows; see note 1
1. Lua 5.3.5 (either install it yourself or use zbstudio from [www.studio.zerobrane.com](http://www.studio.zerobrane.com) which has Lua 5.3 built in). Anything in the 5.3.x series should work just fine.
1. Csound 6.0 or higher -- required for generating audio files ([csound.github.io](http://csound.github.io))

----

* Lilypond -- required for generating standard sheet music. I always use the latest development version (2.19.x) but the latest stable version (2.18.x) should work as well [www.lilypond.org](http://www.lilypond.org).
* pdfLaTeX -- required for generating graphical notation scores. ([www.tug.org/texlive](https://www.tug.org/texlive/))

Technically everything after the line is optional. The software will produce a Csound file which can be listened to/processed into an audio file by Csound.

If you don't want the sheet music then you don't need Lilypond or pdfLaTeX.

I am using the latest development version of Lilypond (2.19.x). I don't know if the latest stable version (2.18.x) will work with the PME at this point, though I think it is likely.

I am using the most up-to-date version of pdfLaTeX however I imagine that any version released in the last five years would work just as well.

The software produces a Csound file which it will then process into an audio file (.wav, .flac, or .ogg) and will tag it (.ogg only).

Note 1: This should work on any Linux distribution that meets the remaining requirements. I do not have access to either MacOS or Windows, though I would assume it works on them just fine especially if you use the Zerobrane editor for Lua. Please report any success or failure you have in getting the PME to work on these platforms.

# Need help? 

The Platonic Music Engine can be confusing. In the docs directory are two manuals and a tutorial. The tutorial is especially for people who do not know how to program and is more hand-holdy. The other two manuals contain detailed descriptions of some of what goes on in the PME.

The tutorial is a good place to start.

# Copyright and license 

Platonic Music Engine -- manipulate music in interesting ways. Copyright (C) 2014 David Bellows davebellows@gmail.com

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see [www.gnu.org/licenses/](http://www.gnu.org/licenses/).
